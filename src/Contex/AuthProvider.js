// import { Children, createContext, useState } from "react";
// const AuthContext = createContext({});

// export const AuthProvider = ({ children }) => {
//   const [auth, setAuth] = useState({});
//   return (
//     <AuthContext.Provider value={{ auth, setAuth }}>
//       {children}
//     </AuthContext.Provider>
//   );
// };
// export default AuthContext;

// ok code

// import { createContext, useContext, useEffect, useState } from "react";

// const AuthContext = createContext(undefined);

// const setLocalStorage = (key, value) => {
//   try {
//     localStorage.setItem(key, JSON.stringify(value));
//   } catch (e) {
//     console.error({ e });
//   }
// };

// const getLocalStorage = (key, initialValue) => {
//   try {
//     const value = localStorage.getItem(key);
//     return value ? JSON.parse(value) : initialValue;
//   } catch (e) {
//     return initialValue;
//   }
// };

// const AuthProvider = ({ children }) => {
//   const [user, setUser] = useState(() =>
//     getLocalStorage("user", { loggedIn: false })
//   );
//   useEffect(() => {
//     setLocalStorage("user", user);
//   }, [user]);

//   const toggleAuth = () => {
//     setUser((prev) => ({
//       ...prev,
//       loggedIn: !prev.loggedIn,
//     }));
//   };

//   const value = { toggleAuth: toggleAuth, user };

//   return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
// };

// const useAuth = () => {
//   const context = useContext(AuthContext);

//   if (context === undefined)
//     throw new Error("useAuth must be within AuthProvider!");

//   return context;
// };

// export { AuthProvider, useAuth };
//ok code
// import { createContext, useContext, useEffect, useState } from "react";

// import { createContext, useContext, useEffect, useState } from "react";
// import General from "../utils/General";

// const AuthContext = createContext(undefined);

// const setLocalStorage = (key, value) => {
//   try {
//     localStorage.setItem(key, JSON.stringify(value));
//   } catch (e) {
//     console.error({ e });
//   }
// };

// const getLocalStorage = (key, initialValue) => {
//   try {
//     const value = localStorage.getItem(key);
//     return value ? JSON.parse(value) : initialValue;
//   } catch (e) {
//     return initialValue;
//   }
// };

// const AuthProvider = ({ children }) => {
//   // setLocalStorage("user", { loggedIn: false });

//   const [user, setUser] = useState(() =>
//     getLocalStorage("user", { loggedIn: false })
//   );
//   useEffect(() => {
//     setLocalStorage("user", user);
//   }, [user]);

//   // const toggleAuth = () => {
//   //   setUser((prev) => ({
//   //     ...prev,
//   //     loggedIn: !prev.loggedIn,
//   //   }));
//   // };
//   const initAuth = (loggedIn, token = "") => {
//     setUser({ loggedIn: loggedIn, token: token });
//     General.authorizationToken =
//       token !== "" ? { Authorization: "Bearer " + token } : "";
//   };

//   // const value = { toggleAuth: toggleAuth, user };
//   const value = { initAuth: initAuth, user };

//   return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
// };

// const useAuth = () => {
//   const context = useContext(AuthContext);

//   if (context === undefined)
//     throw new Error("useAuth must be within AuthProvider!");
import { createContext, useContext, useEffect, useState } from "react";
import General from "../utils/General";

//   return context;
// };

// export { AuthProvider, useAuth };

const AuthContext = createContext(undefined);

const setLocalStorage = (key, value) => {
  try {
    localStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    console.error({ e });
  }
};

const getLocalStorage = (key, initialValue) => {
  try {
    const value = localStorage.getItem(key);
    return value ? JSON.parse(value) : initialValue;
  } catch (e) {
    return initialValue;
  }
};

const AuthProvider = ({ children }) => {
  // setLocalStorage("user", { loggedIn: false });

  const [user, setUser] = useState(() =>
    getLocalStorage("user", { loggedIn: false })
  );
  useEffect(() => {
    setLocalStorage("user", user);
  }, [user]);

  // const toggleAuth = () => {
  //   setUser((prev) => ({
  //     ...prev,
  //     loggedIn: !prev.loggedIn,
  //   }));
  // };
  const initAuth = (loggedIn, token = "", nameFamily = "", phone = "", image = "") => {
    setUser({ loggedIn: loggedIn, token: token, nameFamily: nameFamily, phone: phone, image: image });
    General.authorizationToken =
      token !== "" ? { Authorization: "Bearer " + token } : "";
  };

  // const value = { toggleAuth: toggleAuth, user };
  const value = { initAuth: initAuth, user };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

const useAuth = () => {
  const context = useContext(AuthContext);
  const [user, setUser] = useState(() =>
    getLocalStorage("user", { loggedIn: false, token: "", nameFamily: "", phone: "", image: "" })
  );
  useEffect(() => {
    setLocalStorage("user", user);
  }, [user]);

  General.authorizationToken = { Authorization: "Bearer " + user.token };

  // const toggleAuth = () => {
  //   setUser((prev) => ({
  //     ...prev,
  //     loggedIn: !prev.loggedIn,
  //   }));
  // };
  const initAuth = (loggedIn, token = "", nameFamily = "", phone = "", image = "") => {
    setUser({ loggedIn: loggedIn, token: token, nameFamily: nameFamily, phone: phone, image: image });
    General.authorizationToken =
      token !== "" ? { Authorization: "Bearer " + token } : "";
  };
  if (context === undefined)
    throw new Error("useAuth must be within AuthProvider!");

  return context;
};

export { AuthProvider, useAuth };
