import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "../src/App.css";
import "../src/panel.css";

// components
import Footer from "./components/Footer/Footer";
import VerticalSlider from "./components/VerticalSlider/VerticalSlider";
// import Formonline from "./components/FormOnline/Formonline";
import FormContact from "./components/Form/Form";
import CardWrapper from "./components/CardsWrapper/CardWrapper";
import Navbar from "./components/Navbar/Navbar";
import Sidebar from "./components/Slider/Slider";
import VideoWrapper from "./components/VideoWrapper/VideoWrapper";
import GroupSlider from "./components/GroupSlider/GroupSlider";
import PackageSlider from "./components/PackageSlider/PackageSlider";

// pages
import Gallery from "./pages/Gallery/Gallery";
import HomePage from "../src/pages/HomePage/HomePage";
import Rules from "./pages/Rules/Rules";

import ContactUs from "./pages/ContactUs/ContactUs";
import AboutUs from "./pages/AboutUs/AboutUs";
import Seminars from "./pages/Seminars/Seminars";
import Faq from "./pages/Faq/Faq";
import Advisors from "./pages/Advisors/Advisors";

import Roshateams from "./pages/Roshateam/Roshateams";
import CoWorkers from "./pages/Co-Workers/Co-Workers";
import Reserve from "./pages/reserveExam/ReserveExam";
import News from "./pages/News/News";
import Service from "./pages/Services/Service";
import Branches from "./pages/Branches/Branches";
import Arzyabi from "./pages/Arzyabi/Arzyabi";
import Dashboard from "./pages/Dashboard/Dashboard";
import Exam from "./pages/Exam/Exam";
import ExamView from "./pages/Exam/ExamView";
import Talent from "./pages/Talent/Talent";
import Purchase from "./pages/Purchase/Purchase";
import Installment from "./pages/Installment/Installment";
import Login from "./pages/Login/Login";
import Logout from "./pages/Logout/Logout";
import Acceleration from "./pages/Acceleration/Acceleration";
import MyVideos from "./pages/MyVideos/MyVideos";
import MyFavorites from "./pages/MyFavorites/MyFavorites";
import { useAuth } from "./Contex/AuthProvider";
import Register from "./pages/Register/Register";
import Edit from "./pages/Edit/Edit";
import ResultExam from "./pages/Exam/ResultExam";
import MyPackages from "./pages/MyPackages/MyPackages";
import ExamPackage from "./pages/ExamPackage/ExamPackage";
import MyServices from "./pages/MyServices/MyServices";
import ExamExecution from "./pages/Exam/ExamExe";
import FormAcceleration1 from "./components/Form/Form";

import FormAcceleration2 from "./components/Form/Form2";
import FormAcceleration3 from "./components/Form/Form3";
import FormAcceleration4 from "./components/Form/Form4";
import FormAcceleration5 from "./components/Form/Form5";
import Payment from "./pages/Payment/Payment";
import Exams from "./pages/Exam/Exams";
import EmptyPage from "./components/EmptyPage/Empty";
import NotFound from "./pages/NotFound/NotFound";

//
function App() {
  const { initAuth, user } = useAuth();

  //
  return (
    <>
      <Router>
        {/*{window.location.pathname !== "/login" ? <Navbar /> : null}*/}
        <Navbar />
        <Switch>
          <Route path={"/"} exact component={HomePage} />
          <Route path={"/faq"} component={Faq} />
          <Route path={"/contact"} component={ContactUs} />
          <Route path={"/about-us"} component={AboutUs} />
          <Route path={"/gallery"} component={Gallery} />
          <Route path={"/seminars"} component={Seminars} />
          <Route path={"/rules"} component={Rules} />
          <Route path={"/advisors"} component={Advisors} />
          <Route path={"/roshateam"} component={Roshateams} />
          <Route path={"/co-workers"} component={CoWorkers} />
          <Route path={"/reserve"} component={Reserve} />
          <Route path={"/news"} component={News} />
          <Route path={"/branches"} component={Branches} />
          <Route path={"/arzyabi"} component={Arzyabi} />
          <Route path={"/accelerationindex"} component={Acceleration} />
          <Route path={"/service/:id"} component={Service} />
          {/* <Route path={"/payment"} component={Payment} /> */}
          <Route path={"/exams"} component={Exams} />
          <Route path={"/acceleration-submit/:title"} component={FormAcceleration1} />
          <Route path={"/acceleration-submit/preacceleration"} component={FormAcceleration2} />
          <Route path={"/acceleration-submit/acceleration"} component={FormAcceleration3} />
          <Route path={"/acceleration-submit/nationalevent"} component={FormAcceleration4} />
          <Route path={"/acceleration-submit/mentalproperty"} component={FormAcceleration5} />

          {/* // panel */}
          <Route path={"/login"} component={Login} />
          <Route path={"/logout"} component={Logout} />
          <Route path={"/register"} component={Register} />
          <Route
            path={"/exam/exe/:id"}
            component={
              user.loggedIn === true
                ? ExamExecution
                : () => <Redirect to="/login" />
            }
          />
                    {/* <Route
            path={"/reserve"}
            component={
              user.loggedIn === true ? Reserve : () => <Redirect to="/login" />
            }
          /> */}
          <Route
            path={"/payment/:id"}
            component={
              user.loggedIn === true ? Payment : () => <Redirect to="/login" />
            }
          />

          <Route
            path={"/exam/exe/:id"}
            component={
              user.loggedIn === true
                ? ExamExecution
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/exam/result/:id"}
            // path={"/exam/result"}
            component={
              user.loggedIn === true
                ? ResultExam
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/exam"}
            component={
              user.loggedIn === true ? Exam : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/talent"}
            component={
              user.loggedIn === true ? Talent : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/purchase"}
            component={
              user.loggedIn === true ? Purchase : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/installment"}
            component={
              user.loggedIn === true
                ? Installment
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/myfavorites"}
            component={
              user.loggedIn === true
                ? MyFavorites
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/mypackages/exam/:id/:pid"}
            component={
              user.loggedIn === true
                ? ExamPackage
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/mypackages"}
            component={
              user.loggedIn === true
                ? MyPackages
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/myvideos"}
            component={
              user.loggedIn === true ? MyVideos : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/myservices"}
            component={
              user.loggedIn === true
                ? MyServices
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/dashboard"}
            component={
              user.loggedIn === true
                ? Dashboard
                : () => <Redirect to="/login" />
            }
          />
          <Route
            path={"/edit"}
            component={
              user.loggedIn === true ? Edit : () => <Redirect to="/login" />
            }
          />
          <Route component={NotFound} />
        </Switch>
      </Router>
      {/* <IranMap /> */}
      {/* <Rolls /> */}
      {/* <Seminars /> */}
      {/* <ContactUs /> */}
      {/* <AboutUs />
            // // <RegisterMail /> */}
      {/* // <FormContact /> */}
      {/* <Formonline /> */}
      {/* <Galleryy /> */}
      {/*{window.location.pathname !== "/login" ? <Footer /> : null}*/}
      <Footer />
    </>
  );
}
export default App;
