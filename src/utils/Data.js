import {
  faHome,
  faSeedling,
  faBookOpen,
  faBars,
} from "@fortawesome/free-solid-svg-icons";

import talentIcon from "../assets/images/panel-menu/talent.svg";
import purchaseIcon from "../assets/images/panel-menu/purchase.svg";
import dashboardIcon from "../assets/images/panel-menu/dashboard.svg";
import editIcon from "../assets/images/panel-menu/edit.svg";
import examIcon from "../assets/images/panel-menu/exam.svg";
import favouriteIcon from "../assets/images/panel-menu/favourite.svg";
import installmentIcon from "../assets/images/panel-menu/installment.svg";
import logoutIcon from "../assets/images/panel-menu/logout.svg";
import packageIcon from "../assets/images/panel-menu/package.svg";
import serviceIcon from "../assets/images/panel-menu/service.svg";
import videoIcon from "../assets/images/panel-menu/video.svg";

/* *** */

export const navitems = [
  {
    id: 4,
    title: "روشا",
    path: "/",
    cName: "fa nav-item",
    icon: "fa fa-bars",
  },
  {
    id: 3,
    title: "مجله و اخبار",
    path: "/",
    cName: "nav-item",
    icon: "fa fa-book",
  },
  {
    id: 2,
    title: "سرویس ها",
    path: "/",
    cName: "nav-item",
    icon: "fa fa-seedling",
  },
  {
    id: 1,
    title: "خانه",
    path: "/",
    cName: "nav-item",
    icon: "fa fa-home",
  },
];
export const serviceDropdown = [
  {
    id: 1,
    title: "خانه",
    path: "/home",
    cName: "nav-item",
  },
  {
    id: 2,
    title: "خانه",
    path: "/home",
    cName: "nav-item",
  },
  {
    id: 3,
    title: "خانه",
    path: "/home",
    cName: "nav-item",
  },
  {
    id: 4,
    title: "خانه",
    path: "/home",
    cName: "nav-item",
  },
  {
    id: 5,
    title: "خانه",
    path: "/home",
    cName: "nav-item",
  },
];
export const SliderData = [
  {
    image: "/public",
  },
  {
    image:
      "https://tse1.mm.bing.net/th?id=OIP.OF59vsDmwxPP1tw7b_8clQHaE8&pid=Api&P=0&w=268&h=180",
  },
  {
    image:
      "https://images.search.yahoo.com/images/view;_ylt=AwrJ4NY2_W9hHlUA5h02nIlQ;_ylu=c2VjA3NyBHNsawNpbWcEb2lkAzdmMzdjNmY0NmY4ZGRkMThhZjZkZmQ2YjdkNjgzNmUyBGdwb3MDOQRpdANiaW5n?back=https%3A%2F%2Fimages.search.yahoo.com%2Fyhs%2Fsearch%3Fp%3Dgoogle%26ei%3DUTF-8%26type%3Dq3000_A18FS_set_bcrq%26fr%3Dyhs-ima-st_mig%26hsimp%3Dyhs-st_mig%26hspart%3Dima%26tab%3Dorganic%26ri%3D9&w=2200&h=1502&imgurl=s1.ibtimes.com%2Fsites%2Fwww.ibtimes.com%2Ffiles%2F2014%2F04%2F16%2Fgoogle.jpg&rurl=http%3A%2F%2Fibtimes.com%2Fgoogle-inc-googl-q1-2014-earnings-report-misses-estimates-earnings-rise-profit-margins-1572791&size=766.6KB&p=google&oid=7f37c6f46f8ddd18af6dfd6b7d6836e2&fr2=&fr=yhs-ima-st_mig&tt=Google+Inc.+%28GOOGL%29+Q1+2014+Earnings+Report+Misses+...&b=0&ni=264&no=9&ts=&tab=organic&sigr=B7e9wxRmCA_u&sigb=3FQvzngPktSA&sigi=VKVkUg64EbSu&sigt=6jFNNiSJ9MtD&.crumb=5k5sbbODmih&fr=yhs-ima-st_mig&hsimp=yhs-st_mig&hspart=ima&type=q3000_A18FS_set_bcrq",
  },
];

/* *** */

export const menuItems = [
  {
    id: 1,
    title: "خانه",
    path: "/",
    icon: faHome,
  },
  {
    id: 2,
    title: "سرویس ها",
    path: "/service",
    icon: faSeedling,
    subMenuItems: [
      {
        id: 1,
        title: " روشایار",
        path: "/1",
      },
      {
        id: 3,
        title: " آکادمی استعداد روشا",
        path: "/3",
      },
      {
        id: 4,
        title: " روشاگردی",
        path: "/4",
      },
      {
        id: 5,
        title: " روشا تالک",
        path: "/5",
      },
      {
        id: 11,
        title: " روشا خانواده",
        path: "/11",
      },
      {
        id: 12,
        title: " روشا بازی",
        path: "/12",
      },
    ],
  },
  {
    id: 3,
    title: "مجله و اخبار",
    path: "/home",
    icon: faBookOpen,
    subMenuItems: [
      {
        id: 31,
        title: "مجله 1",
        path: "/home",
      },
      {
        id: 32,
        title: "مجله 2",
        path: "/home",
      },
      {
        id: 33,
        title: "مجله 3",
        path: "/home",
      },
    ],
  },
  {
    id: 4,
    title: "روشا",
    path: "/home",
    icon: faBars,
    subMenuItems: [
      {
        id: 1,
        title: "تماس یا ما ",
        path: "/contact",
      },
      {
        id: 1,
        title: "سوالات متداول",
        path: "/faq",
      },
      {
        id: 1,
        title: " قوانین و مقررات",
        path: "/rules",
      },
      {
        id: 1,
        title: " مشاوران و اساتید",
        path: "/roshateam",
      },
      {
        id: 1,
        title: " همکاران ما  ",
        path: "/co-workers",
      },
      {
        id: 1,
        title: "  رزرو آزمون  ",
        path: "/reserve",
      },
      {
        id: 1,
        title: " نمایندگی های من   ",
        path: "/branches",
      },
      {
        id: 1,
        title: "   ارزیابی و غربال گری   ",
        path: "/arzyabi",
      },
      {
        id: 1,
        title: "  شتابدهی  ",
        path: "/accelerationindex",
      },
      {
        id: 5,
        title: " درباره ما",
        path: "/about-us",
      },
      {
        id: 2,
        title: " سمینار ها و وبینارها",
        path: "/seminars",
      },
    ],
  },
];

export const panelMenuItems = [
  {
    title: "داشبورد",
    path: "/dashboard",
    icon: dashboardIcon,
  },
  {
    title: "ویرایش اطلاعات",
    path: "/edit",
    icon: editIcon,
  },
  {
    title: "ویدئوهای من",
    path: "/myvideos",
    icon: videoIcon,
  },
  {
    title: "آزمون های من",
    path: "/exam",
    icon: examIcon,
  },
  {
    title: "پکیج های من",
    path: "/mypackages",
    icon: packageIcon,
  },
  {
    title: "مشاوره های من",
    path: "/talent",
    icon: talentIcon,
  },
  {
    title: "سرویس های من",
    path: "/myservices",
    icon: serviceIcon,
  },
  {
    title: "سوابق خرید",
    path: "/purchase",
    icon: purchaseIcon,
  },
  {
    title: "اقساط",
    path: "/installment",
    icon: installmentIcon,
  },
  {
    title: "علاقمندی ها",
    path: "/myfavorites",
    icon: favouriteIcon,
  },
  {
    title: "خروج",
    path: "/logout",
    icon: logoutIcon,
    full: true,
  },
];
