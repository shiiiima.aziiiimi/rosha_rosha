class General {
  // siteUrl = "https://api-test.roshatalent.ir/api";
  siteUrl = "http://127.0.0.1:8000/api";

  // token
  authorizationToken = "";

  //
  cleanHtml(str) {
    return str != null ? str.replace(/<[^>]*>?/gm, "") : "";
  }
}

export default new General();

