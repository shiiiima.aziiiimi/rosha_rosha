import React, { useState, useEffect } from "react"
import Header from "../../components/Header/Header"
import Container from "@mui/material/Container"
import Grid from "@mui/material/Grid"
import { Link } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCaretDown, faClock } from "@fortawesome/free-solid-svg-icons"
import PanelMenu from "../../components/PanelMenu/PanelMenu"
import ServiceApi from "../../services/ServiceApi"
import General from "../../utils/General"
import { Button } from "@mui/material"
import Loading from "../../components/Loading/Loading"
import EmptyPage from "../../components/EmptyPage/Empty"
import headerImage from "../../assets/images/header/header-exam.png"
import "./MyServices.css"

//
function MyServices() {
  //
  const [data, setData] = useState([])
  const [dataLoading, setDataLoading] = useState(true)
  const [isNextPage, setIsNextPage] = useState(false)
  const [empty, setEmpty] = useState(false)
  let pageNumber = 1

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data
    setDataLoading(true) // show loading
    setIsNextPage(false) // set more false
    if (pageNumber === 1) dataTmp = [] // set data null

    // get data
    ServiceApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.services !== null &&
          response.services.length > 0
        ) {
          setData([...dataTmp, ...response.services])
          if (response.services.next_page_url != null) setIsNextPage(true)
        }
        if (response.services.length == 0) {
          setEmpty(true)
        }

        setDataLoading(false) // hide loading
      })
      .catch(function (error) {
        setDataLoading(false) // hide loading
      })
  }

  useEffect(() => {
    loadData(1)
  }, [])

  //
  const btnMore = () => {
    pageNumber += 1
    loadData(pageNumber)
  }

  //
  return (
    <>
      <Header title=" سرویس های من" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12} sm={12} md={6} key={item.id}>
                          <div className="my-service-box">
                            <img className="image" src={item.img} />
                            <div className="row-bottom">
                              <div className="title">{item.title}</div>
                              <div className="des">
                                {General.cleanHtml(item.info)}
                              </div>
                              <div className="date">
                                تاریخ برگزاری: {item.date}
                              </div>
                              <div className="price">
                                قیمت: {item.price} ريال
                              </div>

                              <div className="btn-box">
                                <div className="audience">
                                  رده سنی: {item.age_range}
                                </div>
                                <Link
                                  className="link"
                                  to={"service/" + item.id}
                                >
                                  مشاهده این سرویس
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      )
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز سرویسی ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}
export default MyServices
