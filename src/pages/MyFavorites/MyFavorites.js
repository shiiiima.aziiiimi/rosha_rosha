import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faHeart } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import FavoriteApi from "../../services/FavoriteApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import EmptyPage from "../../components/EmptyPage/Empty";
import headerImage from "../../assets/images/header/header-installment.png";
import "./MyFavorites.css";

//
function MYFavorite() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    FavoriteApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.favourites !== null &&
          response.favourites.length > 0
        ) {
          setData([...dataTmp, ...response.favourites]);
          if (response.favourites.next_page_url != null) setIsNextPage(true);
        }
        if (response.favourites.length == 0) {
          setEmpty(true);
        }
        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      <Header title="علاقمندی ها" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container>
                <Grid item xs={12}>
                  <div className="title-bar">علاقمندی ها</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12} key={item.id}>
                          <div className="favorite-box">
                            <div className="row-image-data">
                              <div className="icon-box">
                                <FontAwesomeIcon
                                  icon={faHeart}
                                  className="icon"
                                />
                              </div>
                              <div className="col-data">
                                <div className="title">{item.exam_title}</div>
                                <div className="des">
                                  {General.cleanHtml(item.exam_info)}
                                </div>

                                <div className="date-price-status-box">
                                  <div className="dps">
                                    <span>قیمت: {item.exam_price} ريال</span>
                                    <span>تعداد سوال: {item.no}</span>
                                    <span>
                                      رده سنی: {item.exam_age_range_string}
                                    </span>
                                  </div>

                                  <Link
                                    className="link"
                                    to={"exam/view/" + item.exam_id}
                                  >
                                    نمایش آزمون
                                  </Link>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز علاقه مندی ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default MYFavorite;
