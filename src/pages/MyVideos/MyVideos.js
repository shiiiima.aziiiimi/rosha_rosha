import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import VideoApi from "../../services/VideoApi";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";

import headerImage from "../../assets/images/header/header-exam.png";
import "./MyVideos.css";
import General from "../../utils/General";
import EmptyPage from "../../components/EmptyPage/Empty";
//
function MyVideos() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    VideoApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.videos !== null &&
          response.videos.length > 0
        ) {
          setData([...dataTmp, ...response.videos]);
          if (response.videos.next_page_url != null) setIsNextPage(true);
        }
        if (response.videos.length == 0) {
          setEmpty(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      <Header title="ویدیو های من" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container>
                <Grid item xs={12}>
                  <div className="title-bar">ویدیوهای من</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12}>
                          <div className="video-box">
                            <img className="image" src={item.img} />
                            <div className="col-left">
                              <div className="title">{item.title}</div>
                              <div className="des">
                                {General.cleanHtml(item.info)}
                              </div>
                              <div className="link-box">
                                <a href={item.video} className="link">
                                  مشاهده ویدئو
                                </a>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز ویدیویی ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default MyVideos;
