import React, { useEffect } from "react"
import headerimg from "../../assets/images/Rectangle 60.png"
import axios from "axios"
import brandimg from "../../assets/images/Untitled-1 1.png"
import { Container, Grid, useMediaQuery, useTheme } from "@mui/material"
import FormContact from "../../components/FormContact/FormContact"
import Button from "@mui/material/Button"
import img from "../../assets/images/Untitled-2 1.png"
import "./Co-Workers.css"
import Header from "../../components/Header/Header"
import { Link } from "react-router-dom/cjs/react-router-dom.min"

function CoWorkers() {
  const theme = useTheme()
  const isSmall = useMediaQuery(theme.breakpoints.down("sm"))

  const data = [
    { img: brandimg },
    { img: brandimg },
    { img: brandimg },
    { img: brandimg },
    { img: brandimg },
    { img: brandimg },
    { img: brandimg },
    { img: brandimg }
  ]
  const Getinfo = () => {
    axios.get("http://localhost:8000/api/co-workers").then((res) => {
      console.log(res)
    })
  }
  useEffect(() => {
    Getinfo()
  }, [])
  return (
    <>
      <Header banner={headerimg} title="همکاران  استراتزیک" />
      <div class="context text-about-us">
        <p>
          مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در
          حوزه استعدادیابی و استعدادپروری کودک و نوجوان شروع به فعالیت های علمی
          و پژوهشی و ایجاد زیرساخت های لازم و انجام طرح های سراسری کرد، با توجه
          به توسعه فعالیت های این مجموعه از فروردین سال 1400 با رویکردی جدید و
          استقرار در مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران،
          با حضور معاونت علمی و فناوری ریاست جمهوری جناب آقای دکتر ستاری، این
          فعالیت ها در حوزه های تخصصی ارزیابی و غربالگری، کشف استعداد، پرورش
          استعداد و شتابدهی و حمایت از استعدادهای کودکان و نوجوانان متناظر با
          اهداف مرکز نوآوری آغاز به کار نمود. مرکز استعدادیابی روشا با حمایت
          وزارت تعاون، کار و رفاه اجتماعی و بنیاد ملی توسعه فناوری های فرهنگی و
          با بکارگیری جدید ترین و معتبرترین روش ها و ابزارهای سنجش استعداد و
          همچنین با بهره گیری از دانش و تجارب اعضای محترم هیئت علمی دانشگاه ها،
          در جهت مهارت آموزی، مشاوره، استعدادیابی، استعدادپروری و توانمندسازی
          کودک و نوجوان، در تلاش است با رویکرد بومی خدماتی نوینی را به صورت
          حضوری و غیرحضوری به جامعه مخاطبین اعم از دانشجویان، خانواده ها و دیگر
          اقشار جامعه ارائه نموده و زمینه سازی رشد و بالندگی نسل های آینده کشور
          را فراهم آورد. همچنین از دیگر خدمات شتابدهنده روشا، شناسایی، شتابدهی،
          تجاری سازی ایده های حوزه روانشناسی و علوم تربیتی و کودک و نوجوان،
          مشاوره کسب و کار و همچنین حمایت از کسب و کارهای نوپای این حوزه است. در
          همین زمینه، این مجموعه با بهره گیری از تیمی حرفه ای و همچنین مشاوره
          اساتید و صاحب نظران حوزه کسب و کار فرصتی بی نظیر در جهت کسب تجربه و
          نگرش عمیق، توسعه و موفقیت نوآوران را فراهم آورده است..
        </p>
      </div>

      <div class="area">
        <ul class="circles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>

      {/* <Grid container className="panel-content-box">
        <Grid item xs={12}>
          <h4> مرکز رویشا</h4>

          <div className="line"></div>
        </Grid>
      </Grid> */}
      {/* <div className="panel-content-box">
        <Grid container spacing={4}>
          <Grid item xs={12} sm={12} md={12} className="content-data"></Grid>
        </Grid>
      </div> */}
      <Grid container className="panel-content-box">
        <Grid item xs={12}>
          <Container maxWidth="xl" className="title-brand-holder">
            <div className="title-bar">مرکز رویشا</div>
          </Container>
        </Grid>

        {data.map((item, index) => {
          return (
            <Grid item xs={6} md={3} className="">
              <Container maxWidth="xl">
                <div className="img-brand-holder">
                  <Link to="/">
                    {" "}
                    <img src={brandimg} />
                  </Link>
                </div>
              </Container>
            </Grid>
          )
        })}
      </Grid>

      {/* <Grid container spacing={1}>
        <Grid item xs={12} md={4} className="right-side-rosha-services">
          <div className="img-holder-services">
            <img src={img} />
          </div>
        </Grid>

        <Grid item xs={12} md={8} className="left-side-rosha-services">
          <div className="info-holder1">
            <h4>مشاوران روشا</h4>
            <div className="line"></div>
            <p>
              {" "}
              مرکز رویش و شکوفایی استعداد روشا مفتخر به همکاری با اساتید و
              مشاوران برترین دانشگاههای کشور می باشد.
            </p>
          </div>
          <Button variant="contained" className="btn-send" type="submit">
            کلیک
          </Button>
        </Grid>
      </Grid>
      <div className="full-line"></div> */}
      <div className="section-advertise-coworkers-page">
        <div className="panel-content-box">
          <Grid container spacing={isSmall ? 0 : 4}>
            <Grid item xs={12} sm={6} md={7} className="content-data">
              <Container maxWidth="md">
                <div className="title-bar">تک آزمونهای و آنلاین</div>
                <p className="des">
                  {" "}
                  استعدادیابی غیرحضوری انتخاب خیلی خوبی برای افرادی است که در
                  تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش،
                  رغبت شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید.
                  نتایج آزمون پس از تحلیل توسط متخصصین مجموعه در پروفایل شما
                  قرار خواهد گرفت.
                </p>
                <Button variant="contained" className="btn-send" type="submit">
                  کلیک
                </Button>
              </Container>
            </Grid>
            <Grid item xs={12} sm={6} md={5} className=" left-rosha-coworkers">
              <Container md>
                <div className="img-holder-coworkers">
                  <Link>
                    {" "}
                    <img src={img} />
                  </Link>
                </div>
              </Container>
            </Grid>
          </Grid>
        </div>
      </div>

      <Container maxWidth="xl">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="panel-content-box">
              <FormContact
                title="فرم ارتباط با ما"
                des="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد."
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}
export default CoWorkers
