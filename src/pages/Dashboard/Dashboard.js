import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import PackageApi from "../../services/PackageApi";
import UserApi from "../../services/UserApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import ReactApexChart from "react-apexcharts";

import headerImage from "../../assets/images/header/header-exam.png";
import "./Dashboard.css";

//
function Dashboard() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  let pageNumber = 1;

  const chartCircle = {
    series: [76, 67, 61, 90],
    options: {
      chart: {
        height: 390,
        type: "radialBar",
      },
      plotOptions: {
        radialBar: {
          offsetY: 0,
          startAngle: -90,
          endAngle: 90,
          hollow: {
            margin: 5,
            size: "30%",
            background: "transparent",
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,
            },
            value: {
              show: false,
            },
          },
        },
      },
      colors: ["#b50033", "#2eb5c2", "#ff672f", "#0077B5"],
      labels: ["آزمون 1", "آزمون 2", "آزمون 3", "آزمون 4"],
      legend: {
        show: true,
        floating: true,
        fontSize: "14px",
        position: "top",
        offsetX: 0,
        offsetY: 0,
        labels: {
          useSeriesColors: true,
        },
        markers: {
          size: 0,
        },
        formatter: function (seriesName, opts) {
          return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex];
        },
        itemMargin: {
          vertical: 0,
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              show: false,
            },
          },
        },
      ],
    },
  };

  const chartBar = {
    series: [{ name: "", data: [44, 55, 57, 56, 61, 58, 63, 60, 66] }],
    options: {
      chart: {
        type: "bar",
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded",
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      xaxis: {
        categories: [
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
        ],
      },
      yaxis: {
        title: {
          text: "",
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return val;
          },
        },
      },
    },
  };

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    PackageApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.packages !== null &&
          response.packages.length > 0
        ) {
          setData([...dataTmp, ...response.packages]);
          if (response.packages.next_page_url != null) setIsNextPage(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });

    // get data
    UserApi.userIndex()
      .then(function (response) {
        // if (
        //     response !== null &&
        //     response.packages !== null &&
        //     response.packages.length > 0
        // ) {
        //     setData([...dataTmp, ...response.packages]);
        //     if (response.packages.next_page_url != null) setIsNextPage(true);
        // }
        // setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        // setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      <Header title="داشبورد" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container>
                <Grid item xs={12}>
                  <div className="title-bar">داشبورد</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                  <div className="dashboard-box">
                    <img className="image" src={headerImage} />
                    <div className="col-left">
                      <div className="title">قوانین و مقررات</div>
                      <div className="no">تعداد: 23 عدد</div>
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                  <div className="dashboard-box">
                    <img className="image" src={headerImage} />
                    <div className="col-left">
                      <div className="title">قوانین و مقررات</div>
                      <div className="no">تعداد: 23 عدد</div>
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={12} md={6} lg={4} xl={4}>
                  <div className="dashboard-box">
                    <img className="image" src={headerImage} />
                    <div className="col-left">
                      <div className="title">قوانین و مقررات</div>
                      <div className="no">تعداد: 23 عدد</div>
                    </div>
                  </div>
                </Grid>
              </Grid>

              <Grid container className="container-title-bar">
                <Grid item xs={12}>
                  <div className="title-bar">خودشناسی</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <div className="chart-bar">
                    <ReactApexChart
                      options={chartBar.options}
                      series={chartBar.series}
                      type="bar"
                      height={250}
                    />
                  </div>
                </Grid>

                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <div className="chart-circle">
                    <ReactApexChart
                      options={chartCircle.options}
                      series={chartCircle.series}
                      type="radialBar"
                      height={400}
                    />
                  </div>
                </Grid>
              </Grid>

              <Grid container className="container-title-bar">
                <Grid item xs={12}>
                  <div className="title-bar">دوره های جدید</div>
                </Grid>
              </Grid>

              {/*<Grid container spacing={2}>*/}
              {/*    <Grid item xs={12}>*/}
              {/*        <div className="course-box">*/}
              {/*            <img className="image" src={headerImage} />*/}
              {/*            <div className="col-left">*/}
              {/*                <div className="title">قوانین و مقررات</div>*/}
              {/*                <div className="des">صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و کارها و همچنین استعدادیابی و استعدادپروری کودکان و نوجوانان، با تکیه بر دانش روز و تجارب اساتید و کســــــــــب و کار و رویش و شکوفایی استعداد، با نگاه بومی و رویکردی جامع در تلاش است خدماتی موثر و نقش آفرین در حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر با اهداف مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران در دو حوزه تخصصی مشـاوره</div>*/}
              {/*                <div className="date-link">*/}
              {/*                    <div className="date">تاریخ برگزاری: 1400/10/23</div>*/}
              {/*                    <a href="" className="link">کلیک کنید</a>*/}
              {/*                </div>*/}
              {/*            </div>*/}
              {/*        </div>*/}
              {/*    </Grid>*/}
              {/*</Grid>*/}
              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12}>
                          <div className="course-box">
                            <img className="image" src={item.img} />
                            <div className="col-left">
                              <div className="title">{item.title}</div>
                              <div className="des">{item.des}</div>
                              <div className="date-link">
                                <div className="date">
                                  تاریخ برگزاری: {item.date}
                                </div>
                                <a href="" className="link">
                                  کلیک کنید
                                </a>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default Dashboard;
