import { useAuth } from "../../Contex/AuthProvider";
import React, { useEffect, useState } from "react";
import { useHistory, Redirect } from "react-router-dom";
import Grid from "@mui/material/Grid";
import logoImage from "../../assets/images/4.png";
import Loading from "../../components/Loading/Loading";
import "./Payment.css";
import { Box, Button, Link, TextField, Typography } from "@mui/material";
import UserApi from "../../services/UserApi";
import InputLabel from "@mui/material/InputLabel/InputLabel";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { CartIconSvg } from "../../components/Icon/Icon";
import axios from "axios";
import General from "../../utils/General";
import ReserveApi from "../../services/ReserveApi";
import { Code, CodeSharp, ConstructionOutlined } from "@mui/icons-material";





const Payment = (props) => {
  
    const payment_id =( props.match.params.id );
console.log("payment_id:",payment_id)
    // const [email, setEmail] = useState("");
    // const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const [formSaving, setFormSaving] = useState(false);
    const [data,setData]=useState([])
    // const onEmailInputChange = (event) => setEmail(event.target.value);
    // const onPasswordInputChange = (event) => setPassword(event.target.value);
    // const { initAuth, user } = useAuth();
    // const history = useHistory();
    const [code, setCode] = useState("");
    const [obj_name, setName] = useState("");
    const [age, setAge] = useState("")
    const [termcheck, setTermcheck] = useState(false);
    const handleCode = (event) => {
            setCode(event.target.value)
    }
    const handleName = (event) => {
        setName(event.target.value)
    }
    const handleAge = (event) => {
        setAge(event.target.value)
    }
    const handleTermChcek = (event) => {
    setTermcheck(!termcheck)
}
    useEffect(() => {
   
         
            ReserveApi.findId(payment_id)
       
          .then(function (response) {
            setData(response.payment)
       
          })
          .catch(function (error) {
         console.log(error)
          });
    

      }, []);

      const handleClick = () => {
        
   

        //
      

            ReserveApi.send({ code: code, obj_name: obj_name , age:age ,termcheck:termcheck }).then
            (
                function (response)
                {
               console.log("ok :",response)
                }
            ).catch(function (error)
            {
              console.log("err", error)
            });
       
    }

    //
    return (
        <div className="page-login payment-page">
            <div className="form-login-box">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <img className="logo" src={logoImage} />
                        <div className="title">شما در یک قدمی پرداخت هستید !</div>
                    </Grid>

                    <Grid item xs={7} sx={{ pr: 2 }}>
                        <Box sx={{ mb: 2 }}>
                            <Box className="name-exam">
                                <Typography component="span" sx={{ color: '#036e83' }}>نام :</Typography>
                                <Typography component="span"> آزمون mbti</Typography>
                            </Box>
                            <Box className="name-exam">
                                <Typography component="span" sx={{ color: '#036e83' }}>قیمت :</Typography>
                                <Typography component="span"> {data.price} ریال</Typography>
                            </Box>
                        </Box>
                        <form onSubmit="" autoComplete="off">
                            <Grid container>
                                <Grid item xs={12}>
                                <input
                                        onChange={handleCode}
                                        className="field-input"
                value={code}
                type="text"
                placeholder="کد تخفیف یا کد معرف"
                autoComplete="false"
              />
                                    {/* <TextField className="field-input" label="کد تخفیف یا کد معرف" variant="outlined" onChange="" value="code" /> */}
                                </Grid>
                                <Grid item xs={12} sx={{ mb: 1, mt: 2 }}>
                                    <div className="title">اطلاعات فرزندتان را وارد کنید</div>
                                </Grid>
                                <Grid item xs={12}>
                                <input
                                        onChange={handleName}
                                        className="field-input"
                value={obj_name}
                type="text"
                placeholder="نام و نام خانوادگی"
                autoComplete="false"
              />
                                    {/* <TextField className="field-input" label="سال تولد" variant="outlined" onChange="" value="age" /> */}
                                </Grid>
                                <Grid item xs={12}>
                                    {/* <TextField className="field-input" label="نام و نام خانوادگی" variant="outlined" onChange="" value="obj_name" /> */}
                                    <input
                                        onChange={handleAge}
                                        className="field-input"
                value={age}
                type="text"
                placeholder=" سال تولد  "
                autoComplete="false"
              />

                                </Grid>

                                <Grid item xs={12}>
                                    {/* <FormControlLabel sx={{ mr: 0, color: '#948A8A', display: 'block' }}  control={} label="شرایط پرداخت را می پذبرم" /> */}
                                    <input
                                        type="radio"
                                        onClick={handleTermChcek}
                                        className="field-input"
                value={"termcheck"}
checked={termcheck}
                placeholder="نام و نام خانوادگی"
                autoComplete="false"
              />
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography component="span">برای مشاهده شرایط، به صفحه <Link>قوانین و مقررات</Link> مراجعه کنید.</Typography>
                                </Grid>
                                <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'left' }}>
                                    {formSaving ? <Loading /> : <Button onClick={handleClick} variant="contained"  sx={{ display: 'flex', gap: '10px' }}> <CartIconSvg />   پرداخت</Button>}
                                </Grid>
                            </Grid>
                        </form>
                    </Grid>


                </Grid>
            </div>
        </div>
    );
};

export default Payment;
