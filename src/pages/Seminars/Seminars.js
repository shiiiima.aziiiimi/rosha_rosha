import { Grid, Container } from "@mui/material";
import React, { useEffect, useState } from "react";

import img1 from "../../assets/images/unsplash_s9CC2SKySJM.png";
import headerimg from "../../assets/images/unsplash_F8t2VGnI47I.png";
import img2 from "../../assets/images/unsplash_YRMWVcdyhmI (1).png";
import SeminarCard from "./SeminarCard";
import imgportrate from "../../assets/images/portrait-successful-man-having-stubble-posing-with-broad-smile-keeping-arms-folded 1.png";
import General from "../../utils/General";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faDesktop,
  faLaptop,
  faStopwatch,
} from "@fortawesome/free-solid-svg-icons";
import "./Seminar.css";

import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import Header from "../../components/Header/Header";
import SeminarApi from "../../services/SeminarApi";
function Seminars() {
  // const [council_count, setcouncil_count] = useState(0);
  // const [items_count, setitems_count] = useState(0);
  // const [talent_scout_count, settalent_scout_count] = useState(0);
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  let pageNumber = 1;
  // const [info, setInfo] = useState();
  const [minutes, setMinutes] = useState(0);
  const [teachers, setTeachers] = useState(0);
  const [vebinars, setVebinars] = useState(0);
  // const info = [
  //   {
  //     title: "وبینار از بازی تا اسباب بازی، راه تفکر کودکان",
  //     text: "آیا شما به عنوان یک پدر و مادر آگاه، راه ورود به دنیای تفکر فرزندتان را می دانید؟\
  //         از دنیای شناختیِ او و راه های رشد آن بوسیله اسباب بازی های مختلف را می \
  //         هنگام خرید اسباب بازی، به مولفه های استاندارد بودن آن ها توجه می کنید؟\
  //         در این سمینار به طور اختصاصی و کاملا علمی از زبان دکتر رضا پورحسین عضو هیات علمی دانشگاه تهران و متخصص روانشناسی کودک، پاسخ این پرسش ها را می آموزید.\
  //         ",
  //     date: "1400/8/19",
  //     goal: "ویژه والدین",
  //     price: "2,980,000",
  //     img: img1,
  //   },
  //   {
  //     title: "وبینار استعدادیابی هنری ",
  //     text: "در این وبینار با لزوم اهمیت استعدادیابی هنری کودک و نوجوان و همچنین چرایی و چگونگی مدیریت استعدادهای هنری آشنا میشوید.",
  //     date: "1400/3/9",
  //     goal: "کلیه والدین",
  //     price: "1,000,000",
  //     img: img1,
  //   },
  //   {
  //     title: "وبینار استعدادیابی کارآفرینی",
  //     text: "در این وبینار با لزوم اهمیت استعدادیابی کودک و نوجوان و همچنین چرایی و چگونگی ایجاد تفکر کارآفرینی برای کودکان و نوجوانان آشنا میشوید.",
  //     date: "1400/3/3",
  //     goal: "کلیه والدین",
  //     price: "1,000,000",
  //     img: img1,
  //   },
  // ];
  const Cards = [
    {
      number: vebinars,
      title: "وبینار و دوره آنلاین",
      icon: faDesktop,
    },
    {
      number: teachers,
      title: "مدرس ",
      icon: faLaptop,
    },
    {
      number: minutes,
      title: "دقیقه  ",
      icon: faStopwatch,
    },
  ];
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    SeminarApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.items !== null &&
          response.items.length > 0
        ) {
          setMinutes(response.minutes);
          setTeachers(response.council_count);
          setVebinars(response.items_count);
          setData([...dataTmp, ...response.items]);
          if (response.items.next_page_url != null) setIsNextPage(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  // function getInfo() {
  //   axios.get("http://localhost:8000/api/service-page/10").then((res) => {
  //     setInfo(res.data.items);

  //     setMinutes(res.data.minutes);
  //     setTeachers(res.data.items_count);
  //     setVebinars(res.data.video_count);
  //   });

  // useEffect(() => {
  //   getInfo();

  // }, []);

  return (
    <div className="page-seminars">
      <Header banner={headerimg} title="وبیناروسمینار" />

      {/* <div className="img-holder">
        <img src={headerimg} className="img-roll" />
        <h3 className="head-roll"> وبینار ها و سمینار ها</h3>
      </div> */}
      {/* {console.log("info :", info)} */}
      {/* <Grid container className="cards-seminar">
        {info.map((item, index) => {
          return <Seminar item={item} id={index} />;
        })}
      </Grid> */}

      {/* <Grid container spacing={2}>
        {data != null && data.length > 0
          ? data.map((item) => {
              return (
                <Grid item xs={12}>
                  <div className="course-box">
                    <img className="image" src={item.img} />
                    <div className="col-left">
                      <div className="title">{item.title}</div>
                      <div className="des">{item.des}</div>
                      <div className="date-link">
                        <div className="date">تاریخ برگزاری: {item.date}</div>
                        <a href="" className="link">
                          کلیک کنید
                        </a>
                      </div>
                    </div>
                  </div>
                </Grid>
              );
            })
          : null}
      </Grid>
      {isNextPage ? (
        <Grid container spacing={2}>
          <Button onClick={btnMore} className="btn-more">
            مشاهده بیشتر
            <FontAwesomeIcon icon={faCaretDown} className="icon" />
          </Button>
        </Grid>
      ) : null}
      {dataLoading ? <Loading /> : null} */}
      <Container maxWidth="xl" className="page-section-2">
        <Grid container>
          {data != null && data.length > 0
            ? data.map((item) => {
                return (
                  <Grid item xs={12}>
                    <div className="course-box">
                      <img className="image" src={item.img} />
                      <div className="col-left">
                        <div className="title">{item.title}</div>
                        <div className="des">
                          {General.cleanHtml(item.text)}
                        </div>
                        <div className="date-link">
                          <div className="date-age-rage">
                            <span>تاریخ برگزاری: {item.date}</span>&nbsp;
                            <span>جامعه هدف: {item.goal}</span>
                            <span>قیمت : {item.price}</span>
                          </div>
                          <Link
                            to={"/service-item/" + item.id}
                            className="link"
                          >
                            کلیک کنید
                          </Link>
                        </div>
                      </div>
                    </div>
                  </Grid>
                );
              })
            : null}

          {isNextPage ? (
            <Grid item xs={12}>
              <Button onClick={btnMore} className="btn-more">
                مشاهده بیشتر
                <FontAwesomeIcon icon={faCaretDown} className="icon" />
              </Button>
            </Grid>
          ) : null}
          {dataLoading ? <Loading /> : null}
        </Grid>
      </Container>

      <Grid container spacing={2} className="cards-seminars-wrapper">
        {Cards != null && Cards.length > 0
          ? Cards.map((card, index) => {
              return (
                <Grid item xs={12} md={3} className="card-wrapper">
                  <SeminarCard card={card} />
                </Grid>
              );
            })
          : null}
      </Grid>
      {/* <Grid container>
        <Grid item xs={12} md={4}>
          <SeminarCard item={minutes} title={} />
          <SeminarCard item={teachers} />
          <SeminarCard item={vebinars} />
        </Grid>
      </Grid> */}

      <Grid container className="seminar-about-holder">
        <Grid item xs={12} md={6} className="text-seminar-about">
          <p>
            مرکز رویش و شکوفایی استعداد روشا در جهت آموزش و توانمندسازی خانواده
            ها و نوجوانان و کودکان،با برگزاری  سلسله وبینار و سمینار های ویژه، و
            با حضور اساتید برجسته و بنام حوزه های مختلف، سعی بر این دارد تا بخشی
            از دغدغه ی خانواده های عزیز را برطرف سازد.
          </p>
        </Grid>
        <Grid item xs={12} md={6} className="img-portrate-seminar-about">
          <img src={imgportrate} />
        </Grid>
      </Grid>
    </div>
  );
}
export default Seminars;
