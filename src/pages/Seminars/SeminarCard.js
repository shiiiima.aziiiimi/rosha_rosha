import React from "react";
import "./Seminar.css";
import { Grid } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
function SeminarCard({ card }) {
  const { number, title, icon } = card;

  return (
    <>
      <div className="card">
        <FontAwesomeIcon icon={icon} className="icon-slider-card-seminars" />
        <h5>{number}</h5>
        <h5>{title}</h5>
      </div>
      <div className="border-card"></div>
    </>
  );
}
export default SeminarCard;
