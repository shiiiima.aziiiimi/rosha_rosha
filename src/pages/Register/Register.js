import { useAuth } from "../../Contex/AuthProvider";
import React, { useEffect, useState } from "react";
import { useHistory, Redirect } from "react-router-dom";
import Grid from "@mui/material/Grid";
import logoImage from "../../assets/images/4.png";
import Loading from "../../components/Loading/Loading";
import "./Register.css";
import { Button, TextField } from "@mui/material";
import UserApi from "../../services/UserApi";
import InputLabel from "@mui/material/InputLabel/InputLabel";

//
export const ERROR_MESSAGES =
    {
        "Cannot find user": "کاربری با این ایمیل یافت نشد!",
        "Incorrect password": "کلمه عبور اشتباه می‌باشد!",
        "Empty Data": "اطلاعات را وارد نمایید!",
        "already logged in": "کاربر با این مشخصات وارد شده است.",
        "bad credentials": "اطلاعات اشتباه می باشد.",
        "Clinic admin logged in": "کاربر با این مشخصات وارد شده است.",
        "User logged in": "کاربر با این مشخصات وارد شده است.",
        Problem: "مشکلی در ورود کاربر پیش آمده است.",
    };

//
const Register = () =>
{
    const [name, setName] = useState("");
    const [family, setFamily] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [province, setProvince] = useState("");
    const [city, setCity] = useState("");
    const [password, setPassword] = useState("");
    const [passwordRep, setPasswordRep] = useState("");
    const [error, setError] = useState("");
    const [success, setSuccess] = useState(false);
    const [formSaving, setFormSaving] = useState(false);

    const onNameInputChange = (event) => setName(event.target.value);
    const onFamilyInputChange = (event) => setFamily(event.target.value);
    const onEmailInputChange = (event) => setEmail(event.target.value);
    const onPhoneInputChange = (event) => setPhone(event.target.value);
    const onProvinceInputChange = (event) => setProvince(event.target.value);
    const onCityInputChange = (event) => setCity(event.target.value);
    const onPasswordInputChange = (event) => setPassword(event.target.value);
    const onPasswordRepInputChange = (event) => setPasswordRep(event.target.value);
    const { initAuth, user } = useAuth();
    const history = useHistory();

    //
    useEffect(() =>
    {
        if (user.loggedIn)
        {
            history.push("/dashboard");
        }
    }, [user]);

    const handleRegister = (event) =>
    {
        setError("");
        setSuccess(false);

        //
        if (name !== "" && family !== "" && email !== "" && /*phone !== "" && province !== "" && city !== "" &&*/ (password !== "" && passwordRep !== "" && password === passwordRep))
        {
            setFormSaving(true); // show loading

            UserApi.register({ full_name: name + " " + family, email: email, phone: phone, province: province, city: city, password: password, password_confirmation: passwordRep }).then
            (
                function (response)
                {
                    if (response !== null)
                    {
                        if (response.message === "registered")
                        {
                            setSuccess(true);

                            setName("");
                            setFamily("");
                            setEmail("");
                            setPhone("");
                            setProvince("");
                            setCity("");
                            setPassword("");
                            setPasswordRep("");
                        }
                        else if (response.email !== null && response.email.length > 0)
                        {
                            setError(response.email[0]);
                        }
                        else if (response.message !== "")
                        {
                            setError(response.message);
                        }
                        else
                        {
                            setError("Problem");
                        }
                    }
                    else
                    {
                        setError("Problem");
                    }

                    //
                    setFormSaving(false); // hide loading
                }
            ).catch(function (error)
            {
                setFormSaving(false); // hide loading
                setError("Problem");
            });
        }
        else
        {
            setError("Empty Data");
        }
    };

    const handleGoToSignIn = () =>
    {
        history.push("/login");
    };

    //
    return (
        <div className="page-register">
            <div className="form-register-box">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <img className="logo" src={logoImage} />
                        <div className="title">ثبت نام در روشا</div>
                    </Grid>

                    <Grid item xs={7}>
                        <form onSubmit={handleRegister} autoComplete="off">
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <TextField className="field-input" label="نام" variant="outlined" onChange={onNameInputChange} value={name} />
                                </Grid>

                                <Grid item xs={6}>
                                    <TextField className="field-input" label="نام خانوادگی" variant="outlined" onChange={onFamilyInputChange} value={family} />
                                </Grid>

                                <Grid item xs={12}>
                                    <TextField className="field-input" label="ایمیل" variant="outlined" onChange={onEmailInputChange} value={email} />
                                </Grid>

                                {/*<Grid item xs={12}>*/}
                                {/*    <TextField className="field-input" label="شماره تماس" variant="outlined" onChange={onPhoneInputChange} value={phone} />*/}
                                {/*</Grid>*/}

                                <Grid item xs={6}>
                                    <TextField className="field-input" label="رمز عبور" variant="outlined" type="password" autoComplete="current-password" onChange={onPasswordInputChange} value={password} />
                                </Grid>

                                <Grid item xs={6}>
                                    <TextField className="field-input" label="تکرار رمز عبور" variant="outlined" type="password" autoComplete="current-password" onChange={onPasswordRepInputChange} value={passwordRep} />
                                </Grid>

                                {/*<Grid item xs={6}>*/}
                                {/*    <TextField className="field-input" label="استان محل سکونت" variant="outlined" onChange={onProvinceInputChange} value={province} />*/}
                                {/*</Grid>*/}

                                {/*<Grid item xs={6}>*/}
                                {/*    <TextField className="field-input" label="شهر محل سکونت" variant="outlined" onChange={onCityInputChange} value={city} />*/}
                                {/*</Grid>*/}

                                {
                                    formSaving ?
                                        <Loading />
                                        :
                                        <>
                                            <Grid item xs={6}>
                                                <Button onClick={handleRegister} variant="contained">ثبت نام</Button>
                                            </Grid>

                                            <Grid item xs={6}>
                                                <Button className="btn-default" onClick={handleGoToSignIn} variant="contained">قبلا ثبت نام کرده اید!</Button>
                                            </Grid>
                                        </>
                                }
                            </Grid>
                        </form>
                    </Grid>

                    <Grid item xs={5}>
                        {error && (<div className="alert">{ERROR_MESSAGES[error] ?? error}</div>)}
                        {success && (<div className="alert success">ثبت نام با موفقیت انجام شد.</div>)}
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default Register;
