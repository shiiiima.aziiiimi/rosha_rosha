import React, { useState, useEffect } from "react"
import img from "../../assets/images/unsplash_F8t2VGnI47I (1).png"
import axios from "axios"
import { Container, Grid, useMediaQuery, useTheme } from "@mui/material"
import CountUp from "react-countup"
import "./Service.css"
import image from "../../assets/images/s-o-c-i-a-l-c-u-t-1RT4txDDAbM-unsplash.jpg"
import Button from "@mui/material/Button"
import { useHistory, useParams, useLocation } from "react-router-dom"
import { faCaretDown } from "@fortawesome/free-solid-svg-icons"
import Header from "../../components/Header/Header"
import Loading from "../../components/Loading/Loading"
import ServiceApi from "../../services/ServiceApi"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFile } from "@fortawesome/free-solid-svg-icons"
import headerImage from "../../assets/images/header/header-service.png"
import General from "../../utils/General"
import { Link } from "react-router-dom"

//
function Service(props) {
  const theme = useTheme()
  const isSmall = useMediaQuery(theme.breakpoints.down("sm"))

  const [data, setData] = useState([])
  const [dataText, setDataText] = useState({})
  const [dataLoading, setDataLoading] = useState(true)
  const [isNextPage, setIsNextPage] = useState(false)

  let pageNumber = 1

  const serviceId = parseInt(props.match.params.id, 0)
  const pathname = window.location.pathname

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data
    let dataTextTmp = dataText
    setDataLoading(true) // show loading
    setIsNextPage(false) // set more false
    if (pageNumber === 1) dataTmp = [] // set data null

    // get data
    ServiceApi.findById({ service_id: serviceId }, serviceId)
      .then(function (response) {
        setDataText(response.service)
        if (
          response !== null &&
          response.items !== null &&
          response.items.length > 0
        ) {
          setData([...dataTmp, ...response.items])

          if (response.items.next_page_url != null) setIsNextPage(true)
        }

        setDataLoading(false) // hide loading
      })
      .catch(function (error) {
        setDataLoading(false) // hide loading
      })
    // ServiceApi.findById({ service_id: serviceId }, serviceId)
    //   .then(function (response) {
    //     // if (
    //     //   response !== null &&
    //     //   response.service !== null &&
    //     //   response.service.length > 0
    //     // )
    //     // {

    //     console.log("response.service:", dataText);
    //     // }
    //   })
    //   .catch(function (error) {
    //     // setDataLoading(false); // hide loading
    //   });
  }

  // usePathname();
  useEffect(() => {
    loadData(1)
  }, [pathname])

  //
  const btnMore = () => {
    pageNumber += 1
    loadData(pageNumber)
  }

  //
  return (
    <div className="page-service">
      <Header title="خدمات ما" banner={headerImage} />

      <Container maxWidth="xl" className="page-section-1">
        <Grid container>
          <Grid item xs={12} sm={12}>
            <div className="title-bar"> {dataText.title} </div>
          </Grid>

          <Grid item xs={12} className="panel-page-content-grid">
            <p className="des">
              {General.cleanHtml(dataText.info)}
              {/* مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های
                    لازم در حوزه استعدادیابی و استعدادپروری کودک و نوجوان شروع
                    به فعالیت های علمی و پژوهشی و ایجاد زیرساخت های لازم و انجام
                    طرح های سراسری کرد، با توجه به توسعه فعالیت های این مجموعه
                    از فروردین سال 1400 با رویکردی جدید و استقرار در مرکز نوآوری
                    دانشکده روانشناسی و علوم تربیتی دانشگاه تهران، با حضور
                    معاونت علمی و فناوری ریاست جمهوری جناب آقای دکتر ستاری، این
                    فعالیت ها در حوزه های تخصصی ارزیابی و غربالگری، کشف استعداد،
                    پرورش استعداد و شتابدهی و حمایت از استعدادهای کودکان و
                    نوجوانان متناظر با اهداف مرکز نوآوری آغاز به کار نمود. مرکز
                    استعدادیابی روشا با حمایت وزارت تعاون، کار و رفاه اجتماعی و
                    بنیاد ملی توسعه فناوری های فرهنگی و با بکارگیری جدید ترین و
                    معتبرترین روش ها و ابزارهای سنجش استعداد و همچنین با بهره
                    گیری از دانش و تجارب اعضای محترم هیئت علمی دانشگاه ها، در
                    جهت مهارت آموزی، مشاوره، استعدادیابی، استعدادپروری و
                    توانمندسازی کودک و نوجوان، در تلاش است با رویکرد بومی خدماتی
                    نوینی را به صورت حضوری و غیرحضوری به جامعه مخاطبین اعم از
                    دانشجویان، خانواده ها و دیگر اقشار جامعه ارائه نموده و زمینه
                    سازی رشد و بالندگی نسل های آینده کشور را فراهم آورد. همچنین
                    از دیگر خدمات شتابدهنده روشا، شناسایی، شتابدهی، تجاری سازی
                    ایده های حوزه روانشناسی و علوم تربیتی و کودک و نوجوان،
                    مشاوره کسب و کار و همچنین حمایت از کسب و کارهای نوپای این
                    حوزه است. در همین زمینه، این مجموعه با بهره گیری از تیمی
                    حرفه ای و همچنین مشاوره اساتید و صاحب نظران حوزه کسب و کار
                    فرصتی بی نظیر در جهت کسب تجربه و نگرش عمیق، توسعه و موفقیت
                    نوآوران را فراهم آورده است.. */}
            </p>
          </Grid>
        </Grid>
      </Container>

      <Container maxWidth="xl" className="page-section-2">
        <Grid container spacing={isSmall ? 0 : 0} className="course-box-holder">
          {data != null && data.length > 0
            ? data.map((item) => {
                console.log("cards:", item)
                return (
                  <Grid item xs={12} className="course-box-holder">
                    <div className="course-box">
                      <img className="image" src={item.img} />
                      <div className="col-left">
                        <div className="title">{item.title}</div>
                        <div className="des">
                          {General.cleanHtml(item.short_info)}
                        </div>
                        <div className="date-link">
                          <div className="date-age-rage">
                            <span>تاریخ برگزاری: {item.date}</span>&nbsp;
                            <span>جامعه هدف: {item.age_range}</span>
                          </div>
                          <Link
                            to={"/service-item/" + item.id}
                            className="link"
                          >
                            کلیک کنید
                          </Link>
                        </div>
                      </div>
                    </div>
                  </Grid>
                )
              })
            : null}

          {isNextPage ? (
            <Grid item xs={12}>
              <Button onClick={btnMore} className="btn-more">
                مشاهده بیشتر
                <FontAwesomeIcon icon={faCaretDown} className="icon" />
              </Button>
            </Grid>
          ) : null}
          {dataLoading ? <Loading /> : null}
        </Grid>
      </Container>

      <div className="page-section-3">
        <Container maxWidth="xl">
          <Grid container>
            <Grid item xs={6} sm={3} className="item">
              <CountUp end={40} duration={2} className="number" />
              <div className="title">آزمون اجرا شده</div>
            </Grid>

            <Grid item xs={6} sm={3} className="item">
              <CountUp end={30} duration={5} className="number" />
              <div className="title">ساعت مشاوره تخصصی</div>
            </Grid>

            <Grid item xs={6} sm={3} className="item">
              <CountUp end={10} duration={5} className="number" />
              <div className="title"> عضو آکادمی استعداد</div>
            </Grid>

            <Grid item xs={6} sm={3} className="item">
              <CountUp end={600} duration={10} className="number" />
              <div className="title">
                {" "}
                نوع خدمت در زمینه استعدادپروری و شتابدهی استعداد
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>

      <div className="page-section-4">
        <Container maxWidth="xl">
          <Grid container spacing={isSmall ? 0 : 4}>
            <Grid item xs={12} sm={6}>
              <img src={image} className="image" />
            </Grid>

            <Grid item xs={12} sm={6}>
              <div className="title-bar"> دوره ها و وبینارها</div>

              <p className="des">
                مجموعه رویش و شکوفایی استعداد روشا در راستای خدمات توانمندسازی و
                استعدادپروری خود، مجموعه ای هدفمند از دوره ها، وبینارها و
                سمینارها را زیر نظر متخصصین و مشاورین مجموعه برگزار می کند. این
                دوره ها بر اساس نتایج مستخرج از جلسات مشاوره و توانمندسازی
                مراجعین طراحی شده است.
              </p>

              <Link to={""} className="link">
                کلیک کنید
              </Link>
            </Grid>
          </Grid>
        </Container>
      </div>

      <div className="page-section-5">
        <Container maxWidth="xl">
          <Grid container spacing={isSmall ? 0 : 4}>
            <Grid item md={6} sm={12}>
              <div className="title-bar"> لورم ایپسوم</div>
              <p className="des">
                ۱. با دوره های استعدادپروری، روی استعدادت سرمایه گذاری میکنی...
              </p>
            </Grid>

            <Grid item md={6} sm={12}>
              <Grid container spacing={isSmall ? 0 : 4}>
                <Grid item sm={6} xs={12}>
                  <div className="title-box">
                    <FontAwesomeIcon icon={faFile} className="icon" />
                    <h4 className="title">لورم ایپسوم</h4>
                  </div>
                  <p className="des">
                    ۲. میتونی در بیش از ۲۰‌ نوع دوره و کارگاه آموزشی متناسب با
                    استعدادت شرکت کنی...
                  </p>
                </Grid>

                <Grid item sm={6} xs={12}>
                  <div className="title-box">
                    <FontAwesomeIcon icon={faFile} className="icon" />
                    <h4 className="title">لورم ایپسوم</h4>
                  </div>
                  <p className="des">
                    ۳. اساتید متخصص و مجرب در هر بخش، یادگیری رو براتون آسونتر
                    میکنه...
                  </p>
                </Grid>

                <Grid item sm={6} xs={12}>
                  <div className="title-box">
                    <FontAwesomeIcon icon={faFile} className="icon" />
                    <h4 className="title">لورم ایپسوم</h4>
                  </div>
                  <p className="des">
                    ۴. پیش آزمون و پس آزمون برای دوره ها کمک میکنه میزان پیشرفتت
                    رو متوجه بشی...
                  </p>
                </Grid>

                <Grid item sm={6} xs={12}>
                  <div className="title-box">
                    <FontAwesomeIcon icon={faFile} className="icon" />
                    <h4 className="title">لورم ایپسوم</h4>
                  </div>
                  <p className="des">
                    نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                    چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
                    لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
                    متنوع با هدف بهبود
                  </p>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  )
}
export default Service
