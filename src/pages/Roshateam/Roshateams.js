import axios from "axios";
import React, { useEffect, useState } from "react";
import Roshateam from "./Roshateam";
import RoshaSubTeam from "./RoshaSubTeam";
import img1 from "../../assets/images/000 1.png";
import headerimg from "../../assets/images/Untitled-1 16.png";
import { Container, Grid } from "@mui/material";
import imgfooter from "../../assets/images/unsplash_YRMWVcdyhmI (2).png";
import imgfooter2 from "../../assets/images/closeup-senior-lecturer-with-arms-crossed 1.png";
import PackageApi from "../../services/PackageApi";
import Header from "../../components/Header/Header";
function Roshateams() {
  const [roshateam, setRoshateam] = useState([]);
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  let pageNumber = 1;
  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    PackageApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.packages !== null &&
          response.packages.length > 0
        ) {
          setData([...dataTmp, ...response.packages]);
          if (response.packages.next_page_url != null) setIsNextPage(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  // function getInfo() {
  //   axios
  //     .get("http://localhost:8000/api/rosha-team")

  //     .then((res) => {
  //       setRoshateam(res.data.members.data);
  //     });
  // }
  // useEffect(() => {
  //   getInfo();
  // }, []);

  const data1 = [
    {
      id: 1,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 2,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 3,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 4,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 5,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 6,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
    {
      id: 7,
      name: "علی",
      info: "مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در",
      img: img1,
    },
  ];
  console.log(roshateam);
  return (
    <>
      <Header banner={headerimg} title="مشاوران و اساتید" />
      <Container maxWidth="xl">
        <div class="context text-about-us">
          <p>
            مرکز رویش و شکوفایی استعداد روشا مفتخر به همکاری با اساتید و مشاوران
            برتر دانشگاههای برتر کشور می باشد.
          </p>
        </div>

        <div class="area">
          <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>

        <div className="title-roshasubteam">
          <h3> مرکز رویشا</h3>
          <div className="line"></div>
        </div>
        {data1.map((item, index) => {
          return <Roshateam item={item} index={item.id} />;
        })}
        <div className="title-roshasubteam">
          <h3> رمرکز رویا</h3>
          <div className="line"></div>
        </div>
        <Grid container className="roshasubteam-holder">
          {data1.map((item, index) => {
            return (
              <>
                <Grid item xs={12} md={3} className="roshasubteam-item-holder">
                  <RoshaSubTeam item={item} index={item.id} />;
                </Grid>
              </>
            );
          })}
        </Grid>
      </Container>
      <div className="seminar-about">
        <Grid container className="seminar-about-holder">
          <Grid item xs={12} md={6} className="text-seminar-about">
            <p>
              مرکز رویش و شکوفایی استعداد روشا در جهت آموزش و توانمندسازی
              خانواده ها و نوجوانان و کودکان،با برگزاری  سلسله وبینار و سمینار
              های ویژه، و با حضور اساتید برجسته و بنام حوزه های مختلف، سعی بر
              این دارد تا بخشی از دغدغه ی خانواده های عزیز را برطرف سازد.
            </p>
          </Grid>
          <Grid item xs={12} md={6} className="img-portrate-seminar-about">
            <img src={imgfooter2} />
          </Grid>
        </Grid>
      </div>
    </>
  );
}
export default Roshateams;
