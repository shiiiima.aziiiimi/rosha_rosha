import React, { useEffect } from "react";
import { useAuth } from "../../Contex/AuthProvider";
import { useHistory } from "react-router-dom";

//
function Logout() {
  const { initAuth } = useAuth();
  const history = useHistory();

  useEffect(() => {
    initAuth(false);
    history.push("/login");
  }, []);

  return <></>;
}
export default Logout;
