import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import ExamApi from "../../services/ExamApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import ReactApexChart from "react-apexcharts";

import headerImage from "../../assets/images/header/header-exam.png";
import "./ExamPackage.css";

//
function ExamPackage(props)
{
    // get exam id
    const packageId = parseInt(props.match.params.id, 0);
    const userPackageId = parseInt(props.match.params.pid, 0);

    //
    const [data, setData] = useState([]);
    const [dataLoading, setDataLoading] = useState(true);
    const [isNextPage, setIsNextPage] = useState(false);
    let pageNumber = 1;

    // load data
    const loadData = (pageNumber = 1) => {
        let dataTmp = data;
        setDataLoading(true); // show loading
        setIsNextPage(false); // set more false
        if (pageNumber === 1) dataTmp = []; // set data null

        // get data
        ExamApi.packageExams({ packageId: packageId, userPackageId: userPackageId })
            .then(function (response) {
                if (
                    response !== null &&
                    response.packages !== null &&
                    response.packages.length > 0
                ) {
                    setData([...dataTmp, ...response.packages]);
                    if (response.packages.next_page_url != null) setIsNextPage(true);
                }

                setDataLoading(false); // hide loading
            })
            .catch(function (error) {
                setDataLoading(false); // hide loading
            });
    };

    useEffect(() => {
        loadData(1);
    }, []);

    //
    const btnMore = () => {
        pageNumber += 1;
        loadData(pageNumber);
    };

    //
    return (
        <>
            <Header title=" آزمون های پکیج" banner={headerImage} />

            <Container maxWidth="lg">
                <Grid container>
                    <Grid item xs={12} className="panel-page-content-grid">
                        <PanelMenu />
                        <main className="panel-content-box">
                            {/* <Grid container className="container-title-bar">
                <Grid item xs={12}>
                  <div className="title-bar">دوره های جدید</div>
                </Grid>
              </Grid> */}

                            {/*<Grid container spacing={2}>*/}
                            {/*    <Grid item xs={12}>*/}
                            {/*        <div className="course-box">*/}
                            {/*            <img className="image" src={headerImage} />*/}
                            {/*            <div className="col-left">*/}
                            {/*                <div className="title">قوانین و مقررات</div>*/}
                            {/*                <div className="des">صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و کارها و همچنین استعدادیابی و استعدادپروری کودکان و نوجوانان، با تکیه بر دانش روز و تجارب اساتید و کســــــــــب و کار و رویش و شکوفایی استعداد، با نگاه بومی و رویکردی جامع در تلاش است خدماتی موثر و نقش آفرین در حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر با اهداف مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران در دو حوزه تخصصی مشـاوره</div>*/}
                            {/*                <div className="date-link">*/}
                            {/*                    <div className="date">تاریخ برگزاری: 1400/10/23</div>*/}
                            {/*                    <a href="" className="link">کلیک کنید</a>*/}
                            {/*                </div>*/}
                            {/*            </div>*/}
                            {/*        </div>*/}
                            {/*    </Grid>*/}
                            {/*</Grid>*/}
                            <Grid container spacing={2}>
                                {data != null && data.length > 0
                                    ? data.map((item) => {
                                        return (
                                            <Grid item xs={12}>
                                                <div className="exam-package-box">
                                                    <img className="image" src={item.img} />
                                                    <div className="col-left">
                                                        <div className="title">{item.title}</div>
                                                        <div className="des">{item.des}</div>
                                                        <div className="date-link">
                                                            <a href="" className="link">
                                                                اجرا آزمون
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Grid>
                                        );
                                    })
                                    : null}
                            </Grid>
                            {isNextPage ? (
                                <Grid container spacing={2}>
                                    <Button onClick={btnMore} className="btn-more">
                                        مشاهده بیشتر
                                        <FontAwesomeIcon icon={faCaretDown} className="icon" />
                                    </Button>
                                </Grid>
                            ) : null}
                            {dataLoading ? <Loading /> : null}
                        </main>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
}
export default ExamPackage;
