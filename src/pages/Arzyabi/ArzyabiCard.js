import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid } from "@mui/material";
import React from "react";

import "./Arzyabi.css";
function ArzyabiCard({ card }) {
  const { text, title, icon } = card;

  return (
    <>
      <div className="card">
        <FontAwesomeIcon icon={icon} className="icon-slider-card-arzyabi" />
        <h5>{text}</h5>
        <h5>{title}</h5>
      </div>
      <div className="border-card"></div>
    </>
  );
}
export default ArzyabiCard;
