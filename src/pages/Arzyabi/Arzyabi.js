import { Button, Grid, Container } from "@mui/material"
import React from "react"
import headerimg from "../../assets/images/unsplash_oXV3bzR7jxI.png"
import img1 from "../../assets/images/unsplash_Wh9ZC4727e4 (1).png"
import bgimg from "../../assets/images/Rectangle 84 (1).png"
import { Link } from "react-router-dom"
import "./Arzyabi.css"
import ArzyabiCard from "./ArzyabiCard"
import image1 from "../../assets/images/unsplash_g1Kr4Ozfoac.png"
import Header from "../../components/Header/Header"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faFileLines,
  faChildren,
  faPeopleArrows,
  faChild,
  faFile
} from "@fortawesome/free-solid-svg-icons"
function Arzyabi() {
  var style = {
    backgroundImage: "url(" + { bgimg } + ")"
  }
  const Cards = [
    {
      text: "مشاوره و استعدادیابی",
      title: "حضوری",
      icon: faFile
    },
    {
      text: "پکیج های استعداد یابی",
      title: "غیر حضوری ",
      icon: faFileLines
    },
    {
      text: "ارزیابی کودکان کمتر از ۵ سال",
      title: "ارزیابی و غربالگری",
      icon: faChild
    }
  ]
  return (
    <div className="page-talent-scout">
      <Header banner={headerimg} title="ارزیابی استعدادیابی" />

      <Container maxWidth="xl">
        <div class="context text-talent-scout">
          <p>
            مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در
            حوزه استعدادیابی و استعدادپروری کودک و نوجوان شروع به فعالیت های
            علمی و پژوهشی و ایجاد زیرساخت های لازم و انجام طرح های سراسری کرد،
            با توجه به توسعه فعالیت های این مجموعه از فروردین سال 1400 با
            رویکردی جدید و استقرار در مرکز نوآوری دانشکده روانشناسی و علوم
            تربیتی دانشگاه تهران، با حضور معاونت علمی و فناوری ریاست جمهوری جناب
            آقای دکتر ستاری، این فعالیت ها در حوزه های تخصصی ارزیابی و غربالگری،
            کشف استعداد، پرورش استعداد و شتابدهی و حمایت از استعدادهای کودکان و
            نوجوانان متناظر با اهداف مرکز نوآوری آغاز به کار نمود. مرکز
            استعدادیابی روشا با حمایت وزارت تعاون، کار و رفاه اجتماعی و بنیاد
            ملی توسعه فناوری های فرهنگی و با بکارگیری جدید ترین و معتبرترین روش
            ها و ابزارهای سنجش استعداد و همچنین با بهره گیری از دانش و تجارب
            اعضای محترم هیئت علمی دانشگاه ها، در جهت مهارت آموزی، مشاوره،
            استعدادیابی، استعدادپروری و توانمندسازی کودک و نوجوان، در تلاش است
            با رویکرد بومی خدماتی نوینی را به صورت حضوری و غیرحضوری به جامعه
            مخاطبین اعم از دانشجویان، خانواده ها و دیگر اقشار جامعه ارائه نموده
            و زمینه سازی رشد و بالندگی نسل های آینده کشور را فراهم آورد. همچنین
            از دیگر خدمات شتابدهنده روشا، شناسایی، شتابدهی، تجاری سازی ایده های
            حوزه روانشناسی و علوم تربیتی و کودک و نوجوان، مشاوره کسب و کار و
            همچنین حمایت از کسب و کارهای نوپای این حوزه است. در همین زمینه، این
            مجموعه با بهره گیری از تیمی حرفه ای و همچنین مشاوره اساتید و صاحب
            نظران حوزه کسب و کار فرصتی بی نظیر در جهت کسب تجربه و نگرش عمیق،
            توسعه و موفقیت نوآوران را فراهم آورده است..
          </p>
        </div>

        <div class="area">
          <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
      </Container>

      <Grid container spacing={2} className="cards-arzyabi-wrapper">
        {Cards != null && Cards.length > 0
          ? Cards.map((card, index) => {
              return (
                <Grid item xs={12} md={3} className="card-wrapper">
                  <ArzyabiCard card={card} />
                </Grid>
              )
            })
          : null}
      </Grid>

      <div className="panel-content-box">
        <Grid container>
          <Grid item xs={12} sm={12} md={12} className="content-data">
            <Container maxWidth="xl">
              <div className="title-bar">تک آزمونهای و آنلاین</div>
              <p className="des">
                {" "}
                استعدادیابی غیرحضوری انتخاب خیلی خوبی برای افرادی است که در
                تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش،
                رغبت شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید. نتایج
                آزمون پس از تحلیل توسط متخصصین مجموعه در پروفایل شما قرار خواهد
                در تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش،
                رغبت شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید. نتایج
                آزمون پس از تحلیل توسط متخصصین مجموعه در پروفایل شما قرار خواهد
                در تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش،
                رغبت شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید. نتایج
                آزمون پس از تحلیل توسط متخصصین مجموعه در پروفایل شما قرار
                خواهدگرفت.
              </p>
            </Container>
          </Grid>
        </Grid>
      </div>

      {/* hello start */}
      {/* <Grid container className="rosha-talent-wrapper">
        <Grid item xs={12} md={4} className="right-side-rosha-talent">
          <div className="img-holder-talent">
            <img src={img1} />
          </div>
        </Grid>
        <Grid item xs={12} md={8} className="left-side-rosha-talent">
          <div className="info-holder1">
            <h4> تک آزمونهای و آنلاین</h4>
            <div className="line"></div>
            <p>
              {" "}
              استعدادیابی غیرحضوری انتخاب خیلی خوبی برای افرادی است که در
              تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش، رغبت
              شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید. نتایج آزمون
              پس از تحلیل توسط متخصصین مجموعه در پروفایل شما قرار خواهد گرفت.
            </p>
          </div>
          <Button variant="contained" className="btn-send" type="submit">
            کلیک
          </Button>
        </Grid>
        {/* <div className="animate-line-wrapper">
          <div className="animate-line"></div>
        </div> */}
      {/* </Grid> */}
      {/* hello end */}

      <div className="talent-scout-section-advertise">
        <Container maxWidth="xl">
          <Grid container alignItems="center">
            <Grid item xs={12} md={6} className="image-holder">
              <img src={image1} className="image" />
            </Grid>
            <Grid item xs={12} md={6}>
              <div className="title-bar"> دوره ها و وبینارها</div>

              <p className="des">
                مرکز رویش و شکوفایی استعداد روشا در جهت آموزش و توانمندسازی
                خانواده ها و نوجوانان و کودکان،با برگزاری دوره ها و وبینارهای
                گوناگون، و با حضور اساتید برجسته و بنام حوزه های مختلف، سعی بر
                این دارد تا بخشی از دغدغه ی خانواده های عزیز را برطرف سازد.
              </p>
              <Link to={""} className="link">
                کلیک کنید
              </Link>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  )
}
export default Arzyabi
