import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import {Button} from "@mui/material";
import Loading from "../../components/Loading/Loading";
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import Alert from '@mui/material/Alert';
import headerImage from "../../assets/images/header/header-edit.png";
import UserApi from "../../services/UserApi";
import "./Edit.css";

//
function Edit()
{
    //
    const [dataLoading, setDataLoading] = useState(true);
    const [formSaving, setFormSaving] = useState(false);

    const [flagShowPasswordOld, setFlagShowPasswordOld] = useState(false);
    const [flagShowPassword, setFlagShowPassword] = useState(false);
    const [flagShowPasswordRep, setFlagShowPasswordRep] = useState(false);

    const [alertMessage, setAlertMessage] = useState("");

    const [nameFamily, setNameFamily] = useState("");
    const [email, setEmail] = useState("");
    const [mobile, setMobile] = useState("");
    const [passwordOld, setPasswordOld] = useState("");
    const [password, setPassword] = useState("");
    const [passwordRep, setPasswordRep] = useState("");

    const onNameFamilyInputChange = (event) => setNameFamily(event.target.value);
    const onEmailInputChange = (event) => setEmail(event.target.value);
    const onMobileInputChange = (event) => setMobile(event.target.value);
    const onPasswordOldInputChange = (event) => setPasswordOld(event.target.value);
    const onPasswordInputChange = (event) => setPassword(event.target.value);
    const onPasswordRepInputChange = (event) => setPasswordRep(event.target.value);

    // load data
    const loadData = () =>
    {
        setDataLoading(true); // show loading

        // get data
        UserApi.userInfo().then
        (
            function(response)
            {
                if(response !== null && response.user !== null)
                {
                    setNameFamily(response.user.full_name);
                    setEmail(response.user.email);
                    setMobile(response.user.phone);
                }

                setDataLoading(false); // hide loading
            }
        ).catch
        (
            function(error)
            {
                setDataLoading(false); // hide loading
            }
        );
    };

    useEffect(() => { loadData(); }, []);

    //
    const btnSave = async () =>
    {
        let message = [];

        setAlertMessage(message);
        setFormSaving(false);

        //
        if(nameFamily !== "" && email !== "" && mobile !== "" && ((password === "" && passwordRep === "") || (passwordOld.length >= 5 && password.length >= 5 && passwordRep.length >= 5 && password === passwordRep)))
        {
            setFormSaving(true); // show loading

            // save info
            await UserApi.update({"full_name": nameFamily, "email": email, "phone": mobile}).then
            (
                function(response)
                {
                    if(response !== null)
                    {
                        if(response.message === "account information got successfully updated")
                        {
                            message[message.length] = {type: "success", text: "اطلاعات با موفقیت ذخیره شد."};
                        }
                        else
                        {
                            if(response.errors != null && response.errors.email != null && response.errors.email[0] === "The email field is required.") message[message.length] = {type: "error", text: "ایمیل را وارد نمایید."};
                            if(response.errors != null && response.errors.email != null && response.errors.email[0] === "The email must have an email formay") message[message.length] = {type: "error", text: "ایمیل را بطور کامل وارد نمایید.."};
                            if(response.errors != null && response.errors.email != null && response.errors.email[0] === "the new email should not already exist in database") message[message.length] = {type: "error", text: "ایمیل تکراری می باشد."};
                            if(response.errors != null && response.errors.full_name != null && response.errors.full_name[0] === "The full_name field is required.") message[message.length] = {type: "error", text: "نام و نام خانوادگی را وارد نمایید."};
                            if(response.errors != null && response.errors.phone != null && response.errors.phone[0] === "The phone field is required.") message[message.length] = {type: "error", text: "شماره موبایل را وارد نمایید."};
                        }
                    }
                    else
                    {
                        message[message.length] = {type: "error", text: "مشکلی در ثبت اطلاعات پیش آمده است."};
                    }
                }
            ).catch
            (
                function(error)
                {
                    message[message.length] = {type: "error", text: "مشکلی در ثبت اطلاعات پیش آمده است."};
                }
            );

            // save password
            if(passwordOld.length >= 5 && password.length >= 5 && passwordRep.length >= 5 && password === passwordRep)
            {
                await UserApi.updatePassword({"old_password": passwordOld, "password": password, "password_confirmation": passwordRep}).then
                (
                    function(response)
                    {
                        if(response !== null)
                        {
                            if(response.message === "updated successfully")
                            {
                                message[message.length] = {type: "success", text: "رمز عبور با موفقیت ذخیره شد."};
                            }
                            else
                            {
                                if(response.errors != null && response.errors.password != null && response.errors.password[0] === "The password field is required.") message[message.length] = {type: "error", text: "رمز عبور را وارد نمایید."};
                                if(response.errors != null && response.errors.password != null && response.errors.password[0] === "The password must be atleast 5  charecters.") message[message.length] = {type: "error", text: "رمز عبور باید حداقل 5 حرف باشد."};
                                if(response.errors != null && response.errors.password != null && response.errors.password[0] === "password and password_confirmation must be the same") message[message.length] = {type: "error", text: "رمز عبور و تکرار رمز عبور باید یکسان باشند."};
                                if(response.errors != null && response.errors.password != null && response.errors.password_confirmation[0] === "The password_confirmation field is required.") message[message.length] = {type: "error", text: "تکرار رمز عبور را وارد نمایید."};
                                if(response.errors != null && response.errors.password != null && response.errors.password_confirmation[0] === "The password_confirmation must be atleast 5  charecters.") message[message.length] = {type: "error", text: "تکرار رمز عبور باید حداقل 5 حرف باشد."};
                                if(response.errors != null && response.errors.password != null && response.errors.password_confirmation[0] === "password and password_confirmation must be the same") message[message.length] = {type: "error", text: "رمز عبور و تکرار رمز عبور باید یکسان باشند."};
                                if(response.errors != null && response.errors.password != null && response.errors.old_password[0] === "The old_password field is required.") message[message.length] = {type: "error", text: "رمز عبور قدیمی را وارد نمایید."};
                                if(response.errors != null && response.errors.password != null && response.errors.old_password[0] === "old password must match with our database") message[message.length] = {type: "error", text: "رمز عبور قدیمی مطابقت ندارد."};
                            }
                        }
                        else
                        {
                            message[message.length] = {type: "error", text: "مشکلی در ثبت اطلاعات پیش آمده است."};
                        }
                    }
                ).catch
                (
                    function(error)
                    {
                        message[message.length] = {type: "error", text: "مشکلی در ثبت اطلاعات پیش آمده است."};
                    }
                );
            }

            setAlertMessage(message);
            setFormSaving(false);
        }
        else
        {
            if(!(nameFamily !== "")) message[message.length] = {type: "error", text: "نام و نام خانوادگی را کامل وارد نمایید."};
            if(!(email !== "")) message[message.length] = {type: "error", text: "ایمیل را کامل وارد نمایید."};
            if(!(mobile !== "")) message[message.length] = {type: "error", text: "موبایل را کامل وارد نمایید."};
            if(!((password === "" && passwordRep === "") || (password.length >= 5 && passwordRep.length >= 5 && password === passwordRep))) message[message.length] = {type: "error", text: "رمز عبور و تکرار رمز عبور(حداقل 5 حرف) را کامل وارد نمایید."};
            if(!(passwordOld.length >= 5) && password.length >= 5 && passwordRep.length >= 5) message[message.length] = {type: "error", text: "رمز عبور قدیمی را کامل وارد نمایید."};

            setAlertMessage(message);
        }
    };

    //
    return(
        <>
            <Header title="ویرایش اطلاعات" banner={headerImage} />

            <Container maxWidth="lg">
                <Grid container>
                    <Grid item xs={12} className="panel-page-content-grid">
                        <PanelMenu />
                        <main className="panel-content-box">
                            <Grid container>
                                <Grid item xs={12}>
                                    <div className="title-bar">ویرایش اطلاعات</div>
                                </Grid>
                            </Grid>
                            {
                                alertMessage != null && alertMessage.length > 0 ?
                                    alertMessage.map
                                    (
                                        am => { return (<Alert severity={am.type}>{am.text}</Alert>); }
                                    )
                                    :
                                    null
                            }
                            {
                                dataLoading ?
                                    <Loading />
                                    :
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} className="form-edit-box">
                                            <Grid item xs={12}>
                                                <InputLabel>نام و نام خانوادگی</InputLabel>
                                                <TextField onChange={onNameFamilyInputChange} value={nameFamily} />
                                            </Grid>

                                            <Grid item xs={12}>
                                                <InputLabel>ایمیل</InputLabel>
                                                <TextField type="email" onChange={onEmailInputChange} value={email} />
                                            </Grid>

                                            <Grid item xs={12}>
                                                <InputLabel>شماره موبایل</InputLabel>
                                                <TextField onChange={onMobileInputChange} value={mobile} />
                                            </Grid>

                                            <Grid item xs={12}>
                                                <InputLabel>رمز عبور قدیمی</InputLabel>
                                                <TextField type={flagShowPasswordOld ? "text" : "password"} onChange={onPasswordOldInputChange} autoComplete="current-password" />
                                            </Grid>

                                            <Grid item xs={12}>
                                                <InputLabel>رمز عبور</InputLabel>
                                                <TextField type={flagShowPassword ? "text" : "password"} onChange={onPasswordInputChange} autoComplete="current-password" />
                                            </Grid>

                                            <Grid item xs={12}>
                                                <InputLabel>تکرار رمز عبور</InputLabel>
                                                <TextField type={flagShowPasswordRep ? "text" : "password"} onChange={onPasswordRepInputChange} autoComplete="current-password" />
                                            </Grid>

                                            <Grid item xs={12}>
                                                {
                                                    formSaving ?
                                                        <Loading />
                                                        :
                                                        <Button variant="text" onClick={btnSave}>ذخیره</Button>
                                                }
                                            </Grid>
                                        </Grid>
                                    </Grid>
                            }
                        </main>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
}
export default Edit;
