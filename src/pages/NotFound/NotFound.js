import { Button, Grid } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import "./NotFound.css";
function NotFound() {
  return (
    <div className="not-found-page">
      <Grid container>
        <Grid item xs={12} className="des-notfound">
          <p>صفحه مورد نظر یافت نشد</p>
          <Link to="/">
            <Button variant="container" className="btn-send">
              برو به صفحه اصلی
            </Button>
          </Link>
        </Grid>
      </Grid>
      <div className="circle">
        <div className="moon"></div>
        <div className="moon2"></div>
        <div className="crater1"></div>
        <div className="crater2"></div>
        <div className="crater3"></div>
        <div className="land1"></div>
        <div className="land2"></div>
        <div className="land3"></div>
        <div className="land4"></div>
        <div className="shadow"></div>
        <div className="rabbit"></div>
        <div className="ear1"></div>
        <div className="ear2"></div>
        <div className="tail"></div>
        <div className="cloud1"></div>
        <div className="cloud2"></div>
        <div className="cloud3"></div>
        <div className="cloud4"></div>
      </div>
    </div>
  );
}
export default NotFound;
