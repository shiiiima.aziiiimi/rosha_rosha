import React from "react";
import "./Acceleration.css";
import { Grid } from "@mui/material";
import AccelerationItem from "./AccelerationItem";
import Header from "../../components/Header/Header";
import { Link } from "react-router-dom";

function Acceleration() {
  return (
    <>
      <Header
        banner={"./assets/images/shetabdehi-header.png"}
        title="شتابدهی"
      />

      {/* <div className="img-holder">
        <img src="./assets/images/shetabdehi-header.png" className="img-roll" />
        <h3 className="head-roll">شتابدهی</h3>
      </div> */}
      <Grid container className="main-page">
        <Grid item xs={10} sx={{ margin: "auto", py: 2 }}>
          <AccelerationItem key={1} id="1"  url="/acceleration-submit/prepevent" />
          <AccelerationItem
            key={2}
            id="2"

            url="/acceleration-submit/preacceleration"
          />
          <AccelerationItem key={3} id="3" url="/acceleration-submit/acceleration" />
          <AccelerationItem
            key={4}
            id="4"
     
            url="/acceleration-submit/nationalevent"
          />
          <AccelerationItem
            key={5}
            id="5"
      
            url="/acceleration-submit/mentalproperty"
          />
          {/* <AccelerationItem
            key={6}
            id="6"
            head=""
            des=""
            url="/accelerationResult"
          /> */}
        </Grid>
      </Grid>
    </>
  );
}
export default Acceleration;
