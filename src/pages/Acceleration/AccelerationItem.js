import React from "react";
import { Grid, Box, Button } from "@mui/material";
import { Link } from "react-router-dom";



function AccelerationItem(props) {
  const url = props.url
  const head = props.head
  const des = props.des

  //   const  head = props.head;
//   const  des=props.des
//   const PassProps = () => {
 
//   const  head = props.head;
//   const  des=props.des

// }

  return (
    <Box className="accel-items-container">
      <Box className="accel-item-number">{props.id}</Box>
      <Grid container className="accel-item">
        <Grid md={8} className="accel-items-info">
          <Box className="accel-item-title">لورم ایپسوم متن ساختگی</Box>
          <Box
            sx={{
              mt: 2,
              fontWeight: "400",
              fontSize: "16px",
              lineHeight: "30px",
            }}
            className="accel-item-desc"
          >
            از صنعت چاپ، و با استفاده از طراحان گرافیک لورم ایپسوم متن ساختگی با
            تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک
          </Box>
          <Box sx={{ textAlign: "left", mt: 3 }}>
            <Link to={props.url} >
              <Button className="button" sx={{ px: 4, py: 1 }}>
                {" "}
                مشاهده فرم
              </Button>
            </Link>
          </Box>
        </Grid>
        <Grid
          md={4}
          sx={{ display: "flex", alignItems: "center" }}
          className="accel-item-img"
        >
          <img src="./assets/images/accel-images.png" />
        </Grid>
      </Grid>
    </Box>
  );
}

export default AccelerationItem;
