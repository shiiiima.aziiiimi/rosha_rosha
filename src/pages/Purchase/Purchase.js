import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import PurchaseApi from "../../services/PurchaseApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import EmptyPage from "../../components/EmptyPage/Empty";
import headerImage from "../../assets/images/header/header-purchase.png";
import "./Purchase.css";

//
function Purchase() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    PurchaseApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.payments !== null &&
          response.payments.length > 0
        ) {
          setData([...dataTmp, ...response.payments]);
          if (response.payments.next_page_url != null) setIsNextPage(true);
        }
        if (response.payments.length == 0) {
          setEmpty(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      <Header title="سوابق خرید" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container>
                <Grid item xs={12}>
                  <div className="title-bar">سوابق خرید</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12} key={item.id}>
                          <div className="purchase-box">
                            <div className="row-image-data">
                              <img className="image" src={item.img} />
                              <div className="col-data">
                                <div className="title">{item.title}</div>
                                <div className="des">
                                  {General.cleanHtml(item.info)}
                                </div>

                                <div className="date-price-status-box">
                                  <span>تاریخ: {item.date}</span>
                                  <span>مبلغ کل: {item.price}</span>
                                  <span>وضعیت: {item.status}</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز خریدی  ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default Purchase;
