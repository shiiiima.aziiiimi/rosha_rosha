import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCheck } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import InstallmentApi from "../../services/InstallmentApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import EmptyPage from "../../components/EmptyPage/Empty";
import headerImage from "../../assets/images/header/header-installment.png";
import "./Installment.css";

//
function Installment() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    InstallmentApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.installments !== null &&
          response.installments.length > 0
        ) {
          setData([...dataTmp, ...response.installments]);
          if (response.installments.next_page_url != null) setIsNextPage(true);
        }
        if (response.installments.length == 0) {
          setEmpty(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      <Header title="اقساط" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container>
                <Grid item xs={12}>
                  <div className="title-bar">اقساط</div>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid item xs={12} key={item.id}>
                          <div className="installment-box">
                            <div className="row-image-data">
                              <img className="image" src={item.img} />
                              <div className="col-data">
                                <div className="title">{item.title}</div>
                                <div className="des">
                                  {General.cleanHtml(item.short_info)}
                                </div>

                                <div className="date-price-status-box">
                                  <span>تاریخ: {item.date}</span>
                                  <span>مبلغ کل: {item.total_price}</span>
                                  <span>
                                    وضعیت:{" "}
                                    {item.compeleted === 1
                                      ? "کامل"
                                      : "کامل نشده"}
                                  </span>
                                </div>
                              </div>
                            </div>
                            {data.payments != null && data.payments.length > 0
                              ? data.payments.map((paymentItem) => {
                                  return (
                                    <div className="row-installment-payment">
                                      <Grid container spacing={2}>
                                        <Grid
                                          item
                                          xs={12}
                                          sm={6}
                                          md={4}
                                          lg={3}
                                          xl={3}
                                          key={paymentItem.id}
                                          className={
                                            paymentItem.completed === 1
                                              ? "complete"
                                              : ""
                                          }
                                        >
                                          <div className="title">
                                            {paymentItem.title}
                                          </div>
                                          <div className="price">
                                            {paymentItem.price} ريال
                                          </div>
                                          {paymentItem.completed === 1 ? (
                                            <div className="icon">
                                              <FontAwesomeIcon icon={faCheck} />
                                            </div>
                                          ) : null}
                                        </Grid>
                                      </Grid>
                                    </div>
                                  );
                                })
                              : null}
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? <EmptyPage description="شما هنوز قسطی ندارید" /> : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default Installment;
