import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faClock } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import PackageApi from "../../services/PackageApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import EmptyPage from "../../components/EmptyPage/Empty";
import headerImage from "../../assets/images/header/header-exam.png";
import "./MyPackages.css";

//
function MyPackages() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    PackageApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.packages !== null &&
          response.packages.length > 0
        ) {
          setData([...dataTmp, ...response.packages]);
          if (response.packages.next_page_url != null) setIsNextPage(true);
        }
        if (response.packages.length == 0) {
          setEmpty(true);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      {/*<Header title="آزمون ها" subTitle="نام شخص" banner={headerImage} image={image} />*/}
      <Header title=" پکیح های من" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          md={6}
                          lg={4}
                          xl={4}
                          key={item.id}
                        >
                          <div className="my-package-box">
                            <img className="image" src={item.img} />
                            <div className="row-bottom">
                              <div className="title">{item.title}</div>
                              <div className="audience">
                                مخاطب: {item.age_range}
                              </div>
                              <div className="des">
                                {General.cleanHtml(item.info)}
                              </div>

                              <div className="btn-box">
                                <span className="package-time">
                                  <FontAwesomeIcon
                                    icon={faClock}
                                    className="icon"
                                  />
                                  زمان تقریبی:۹۰ دقیقه
                                </span>
                                <Link
                                  className="link"
                                  to={
                                    "mypackages/exam/" +
                                    item.id +
                                    "/" +
                                    item.user_package_id
                                  }
                                >
                                  مشاهده آزمون
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? <EmptyPage description="شما هنوز پکیجی ندارید" /> : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default MyPackages;
