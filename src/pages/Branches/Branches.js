import React from "react";
import image from "../../assets/images/unsplash_F8t2VGnI47I (1).png";
import { Grid, Container } from "@mui/material";
import image1 from "../../assets/images/unsplash_g1Kr4Ozfoac.png";
import "./Branches.css";
import FormContact from "../../components/FormContact/FormContact";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import Header from "../../components/Header/Header";
function Branches() {
  return (
    <div className="page-branches">
      <Header banner={image} title="نمایندگی ما" />
      <Container maxWidth="xl">
        <div class="context text-branches">
          <p>
            مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در
            حوزه استعدادیابی و استعدادپروری کودک و نوجوان شروع به فعالیت های
            علمی و پژوهشی و ایجاد زیرساخت های لازم و انجام طرح های سراسری کرد،
            با توجه به توسعه فعالیت های این مجموعه از فروردین سال 1400 با
            رویکردی جدید و استقرار در مرکز نوآوری دانشکده روانشناسی و علوم
            تربیتی دانشگاه تهران، با حضور معاونت علمی و فناوری ریاست جمهوری جناب
            آقای دکتر ستاری، این فعالیت ها در حوزه های تخصصی ارزیابی و غربالگری،
            کشف استعداد، پرورش استعداد و شتابدهی و حمایت از استعدادهای کودکان و
            نوجوانان متناظر با اهداف مرکز نوآوری آغاز به کار نمود. مرکز
            استعدادیابی روشا با حمایت وزارت تعاون، کار و رفاه اجتماعی و بنیاد
            ملی توسعه فناوری های فرهنگی و با بکارگیری جدید ترین و معتبرترین روش
            ها و ابزارهای سنجش استعداد و همچنین با بهره گیری از دانش و تجارب
            اعضای محترم هیئت علمی دانشگاه ها، در جهت مهارت آموزی، مشاوره،
            استعدادیابی، استعدادپروری و توانمندسازی کودک و نوجوان، در تلاش است
            با رویکرد بومی خدماتی نوینی را به صورت حضوری و غیرحضوری به جامعه
            مخاطبین اعم از دانشجویان، خانواده ها و دیگر اقشار جامعه ارائه نموده
            و زمینه سازی رشد و بالندگی نسل های آینده کشور را فراهم آورد. همچنین
            از دیگر خدمات شتابدهنده روشا، شناسایی، شتابدهی، تجاری سازی ایده های
            حوزه روانشناسی و علوم تربیتی و کودک و نوجوان، مشاوره کسب و کار و
            همچنین حمایت از کسب و کارهای نوپای این حوزه است. در همین زمینه، این
            مجموعه با بهره گیری از تیمی حرفه ای و همچنین مشاوره اساتید و صاحب
            نظران حوزه کسب و کار فرصتی بی نظیر در جهت کسب تجربه و نگرش عمیق،
            توسعه و موفقیت نوآوران را فراهم آورده است..
          </p>
        </div>

        <div class="area">
          <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
      </Container>
      {/* <div className="section-advertise-branches-page">
        <div className="panel-content-box">
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6} md={7} className="content-data">
              <Container maxWidth="md">
                <div className="title-bar">تک آزمونهای و آنلاین</div>
                <p className="des">
                  {" "}
                  استعدادیابی غیرحضوری انتخاب خیلی خوبی برای افرادی است که در
                  تهراندر این قسمت شما می¬توانید آزمون¬های مختلف در زمینه هوش،
                  رغبت شغلی ، شخصیت و هوش هیجانی را بصورت آنلاین انجام دهید.
                  نتایج آزمون پس از تحلیل توسط متخصصین مجموعه در پروفایل شما
                  قرار خواهد گرفت.
                </p>
                <Button variant="contained" className="btn-send" type="submit">
                  کلیک
                </Button>
              </Container>
            </Grid>
            <Grid item xs={12} sm={6} md={5} className=" left-rosha-branches">
              <Container md>
                <div className="img-holder-branches">
                  {" "}
                  <img src={img1} />
                </div>
              </Container>
            </Grid>
          </Grid>
        </div>
      </div> */}
      <div className="page-section-advertise">
        <Container maxWidth="xl">
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6} className="image-holder">
              <img src={image1} className="image" />
            </Grid>

            <Grid item xs={12} sm={6}>
              <div className="title-bar"> دوره ها و وبینارها</div>

              <p className="des">
                مرکز رویش و شکوفایی استعداد روشا در جهت آموزش و توانمندسازی
                خانواده ها و نوجوانان و کودکان،با برگزاری دوره ها و وبینارهای
                گوناگون، و با حضور اساتید برجسته و بنام حوزه های مختلف، سعی بر
                این دارد تا بخشی از دغدغه ی خانواده های عزیز را برطرف سازد.
              </p>

              <Link to={""} className="link">
                کلیک کنید
              </Link>
            </Grid>
          </Grid>
        </Container>
      </div>

      {/* <Grid container className="form-holder">
        <Grid item>
          <div className="info-holder">
            <h3>ارتباط با ما</h3>
            <div className="line"></div>
            <p> برای این که با شما تماس بگیریم لطفا فرم زیر را پر کنید</p>
          </div>
          <FormContact />
        </Grid>
      </Grid> */}
      <Container maxWidth="xl">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="panel-content-box">
              <FormContact
                title="فرم ارتباط با ما"
                des="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد."
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
export default Branches;
