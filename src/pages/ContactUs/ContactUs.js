import React, { useState } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import headerImage from "../../assets/images/header/header-contact-us.png";
import FormContact from "../../components/FormContact/FormContact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobileAlt,
  faMapMarkerAlt,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";

import "./ContactUs.css";

//
function ContactUs(props) {
  //
  return (
    <div className="page-contact">
      <Header title="ارتباط با ما" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="panel-content-box">
              <div className="des-box">
                <div className="title-bar">بخش ارتباط با ما:</div>
                <p className="des">
                  مرکز رویش و شکوفایی استعداد روشا در راستای توسعه و بهبود
                  فرایندهای خود و ایجاد تجربه های بهتر برای مراجعین محترم،
                  پذیرای ایده ها و پیشنهادات همه متخصصین، پژوهشگران، خانواده¬ها
                  و تمامی دغدغه¬مندان حوزه استعداد می¬باشد. همچنین ممکن است شما
                  از گوشه ای از این شهر، کشور یا جهان، ایده¬ای داشته باشید که
                  بتواند جریانی نو یا جرقه¬ای تازه برای این رسالت بزرگ،
                  "استعدادیابی کودکان و نوجوانان این سرزمین" ایجاد کند. پس این
                  فرصت را از خود و جهان دریغ نکنید و دوشادوش روشا گامی برای تحقق
                  بهتر این هدف بردارید.
                </p>
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className="section-map">
        <div className="panel-content-box">
          <Container maxWidth="lg">
            <Grid container spacing={4}>
              <Grid item xs={12} sm={12}>
                <div className="title-bar">قوانین و مقررات</div>
                <p className="des">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                  استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                  مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                  تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                  کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                  آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم
                  افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                  طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                </p>
              </Grid>

              <Grid item xs={12} sm={6} md={6} className="contact-data">
                <iframe
                  className="google-map"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3892.033638913015!2d51.37288165543435!3d35.72756374238894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92a24f4f6bd4bdd4!2sFaculty%20of%20Psychology%20and%20Educational%20Sciences!5e0!3m2!1sen!2s!4v1643807925128!5m2!1sen!2s"
                  width="100%"
                  height="450"
                  allowFullScreen=""
                  loading="lazy"
                ></iframe>

                <div className="title">اطلاعات تماس دفتر مرکزی</div>
                <div className="item-box">
                  <FontAwesomeIcon icon={faMobileAlt} className="icon" />
                  <span className="title">تلفن:</span>
                  <span className="des">02112345678</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faMapMarkerAlt} className="icon" />
                  <span className="title">آدرس:</span>
                  <span className="des">گیشا - دانشگاه تهران - مرکز رشد</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faEnvelope} className="icon" />
                  <span className="title">پست الکترونیکی:</span>
                  <span className="des">sabkeZendegiNovin@gmail.com</span>
                </div>
              </Grid>

              <Grid item xs={12} sm={6} md={6} className="contact-data">
                <iframe
                  className="google-map"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3892.033638913015!2d51.37288165543435!3d35.72756374238894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92a24f4f6bd4bdd4!2sFaculty%20of%20Psychology%20and%20Educational%20Sciences!5e0!3m2!1sen!2s!4v1643807925128!5m2!1sen!2s"
                  width="100%"
                  height="450"
                  allowFullScreen=""
                  loading="lazy"
                ></iframe>

                <div className="title">اطلاعات تماس شعبه یک</div>
                <div className="item-box">
                  <FontAwesomeIcon icon={faMobileAlt} className="icon" />
                  <span className="title">تلفن:</span>
                  <span className="des">02112345678</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faMapMarkerAlt} className="icon" />
                  <span className="title">آدرس:</span>
                  <span className="des">گیشا - دانشگاه تهران - مرکز رشد</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faEnvelope} className="icon" />
                  <span className="title">پست الکترونیکی:</span>
                  <span className="des">sabkeZendegiNovin@gmail.com</span>
                </div>
              </Grid>
            </Grid>
          </Container>
        </div>
      </div>

      <Container maxWidth="xl">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="panel-content-box">
              <FormContact
                title="فرم ارتباط با ما"
                des="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد."
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
export default ContactUs;
