import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { Box, Button, Typography, TextField } from "@mui/material";
import ReactApexChart from "react-apexcharts";
import { HandIconSvg, BookIconSvg } from "../../components/Icon/Icon";
import { Swiper, SwiperSlide } from "swiper/react";
import Comment from "../../components/Comment/Comment";
import NewsLetter from "../../components/NewsLetter/NewsLetter";

import axios from "axios";
import General from "../../utils/General";
import ResultQarbalgary from "../../components/Result/ResultQarbalgary";
import ResultGardner from "../../components/Result/ResultGardner";
import ResultHaland from "../../components/Result/ResultHaland";
import ResultMBTI from "../../components/Result/ResultMBTI";
import ResultHosh from "../../components/Result/ResultHosh";
import ResultShoartz from "../../components/Result/ResultShoartz";
import ResultSoanson from "../../components/Result/ResultSoanson";

function ResultExam(props) {
  const [result_id, setResultId] = useState(0)
  const userExamId = parseInt(props.match.params.id, 0);
  
  const getData = () => {
    axios({
      method: "get",
      url: General.siteUrl+"/exam-resultpage/" + userExamId,

      headers: {
        ...General.authorizationToken,
        "content-type": "application/json",
      },


    }).then((res) => {
      setResultId(res.data.exam.id)
    
  
     
    }).catch((err) => {
      console.log("err :" ,err)
    })

  }
  useEffect(() => {
    getData()
  }, [])

  const ShowSuitableResult = (result_id) => {
   console.log(result_id)
    switch (result_id) {
      
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
        case 19:
        return <ResultQarbalgary/>
        break;
    
        case 29:
        return <ResultGardner/>
        break;
        case 30:
          return <ResultHaland/>
          break;
          case 31:
            return <ResultMBTI/>
            break;
            case 32:
              return <ResultHosh/>
              break;
              case 33:
                return <ResultShoartz/>
                break;
                case 35:
                  return <ResultSoanson/>
                  break;
              
      default:
        break;
    }
   
 }
  return (
    <>
      {
       ShowSuitableResult(result_id)
      }
    </>
  );
}
export default ResultExam;
