import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import ExamApi from "../../services/ExamApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import headerImage from "../../assets/images/header/header-exam.png";
import "./Exam.css";
import { CodeSharp, ConstructionOutlined } from "@mui/icons-material";
import EmptyPage from "../../components/EmptyPage/Empty";
//
function Exam() {
  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    ExamApi.find({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.exams !== null &&
          response.exams.length > 0
        ) {
          setData([...dataTmp, ...response.exams]);
      
          if (response.exams.next_page_url != null) setIsNextPage(true);
        }
        if (response.exams.length == 0) {
          setEmpty(true);
        }
  
        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };

  //
  return (
    <>
      {/*<Header title="آزمون ها" subTitle="نام شخص" banner={headerImage} image={image} />*/}
      <Header title="آزمون ها" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <PanelMenu />
            <main className="panel-content-box">
              <Grid container spacing={2}>
                {    console.log(" data :" ,data)}
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid
                          item
                          xs={12}
                          sm={6}
                          md={6}
                          lg={4}
                          xl={4}
                          key={item.id}
                        >
                          <div className="exam-box">
                            <img className="image" src={item.img} />
                            <div className="row-bottom">
                              <div className="title">{item.title}</div>
                              <div className="audience">
                                {General.cleanHtml(item.short_info)}
                              </div>
                              <div className="des">
                                {General.cleanHtml(item.info.substring(0, 200))}
                                ...
                              </div>

                              <div className="btn-box">
                                <Link
                                  className="link"
                                  to={{ pathname: "exam/exe/" +item.user_exam_id , state:  item.user_exam_id }}
                            
                                
                                >
                            
                                  شروع آزمون
                                </Link>
                                <Link
                                  className="link"
                                  to={{ pathname: "exam/result/" +item.user_exam_id , state:  item.user_exam_id }}
                                  // to={"exam/result/" + item.user_exam_id}
                                >
                                  مشاهده نتیجه
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز آزمونی ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default Exam;
