import { Box, Button } from "@mui/material";
import React ,{useState} from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../../Contex/AuthProvider";
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import General from "../../utils/General";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import axios from "axios";
//
function ExamsSidebar({setData}) {
    const { initAuth, user } = useAuth();
    const [value, setValue] = useState([]);

  // get data

const handleclick = () => {
    axios({
        method: "get",
        url: General.siteUrl + "/exams-all/filter/" + value,
    
        headers: {
            "cache-control": 'no-cache, private', "content-type": 'application/json'
        },
      })
        .then((response) => {
          // check if received an OK response, throw error if not
          setData(response.data.exams)
        })
       
        .catch((errors) => {
          // If error was thrown then unpack here and handle in component/app state
          console.log("error", errors);
    
        });
}
const handleChange = (event) => {
    setValue(event.target.value);
};

    //
    return (
        <aside className="exams-side-bar">
            <Box className="accel-item-title" sx={{ mb: 2, px: 3 }}>
                لورم ایپسوم متن ساختگی
            </Box>
            <FormControl component="fieldset">
                <RadioGroup aria-label="position" className="checkGroup"    value={value}
        onChange={handleChange}>

                    <FormControlLabel
                        name="filter"
                        value="exam?filter=1%2C15"
                        control={<Radio />}
                        label="4 تا 15 ماهگی"
                        labelPlacement="end"
                    />
                    <FormControlLabel
                               name="filter"
                               value="exam?filter=16%2C30"
                        control={<Radio />}
                        label="16 تا 30 ماگی"
                        labelPlacement="end"
                    />
                    <FormControlLabel
                               name="filter"
                               value="?filter=31%2C45"
                        control={<Radio />}
                        label="31 تا 45 ماگی"
                        labelPlacement="end"
                    />
                    <FormControlLabel
                               name="filter"
                               value="exam?filter=46%2C60"
                        control={<Radio />}
                        label="46 تا 60 ماگی"
                        labelPlacement="end"
                    />
                    <FormControlLabel
                               name="filter"
                               value="exam?filter=1%2C60"
                        control={<Radio />}
                        label="4 تا 60 ماگی"
                        labelPlacement="end"
                    />
                     <FormControlLabel
                               name="filter"
                               value="talent_scout_exam?filter=سایر+آزمون+ها"
                        control={<Radio />}
                        label="سایر آزمون ها"
                        labelPlacement="end"
                    />
                </RadioGroup>
                <Button type="submit" onClick={handleclick}>فیلتر</Button>
            </FormControl>

            <Box className="blue-border-title">
                آموزش ها و پکیح های پر کاربرد
            </Box>
            <ul className="packages-list">
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
                <li>لورم ایپسوم متن ساختگی </li>
            </ul>
        </aside>
    );
}
export default ExamsSidebar;
