import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import ExamApi from "../../services/ExamApi";
import General from "../../utils/General";
import { Box, Button, Typography } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import headerImage from "../../assets/images/header/header-exam.png";
import "./Exams.css";
import { ConstructionOutlined } from "@mui/icons-material";
import ExamsSidebar from "./ExamsSidebar";


import { faCaretDown } from "@fortawesome/free-solid-svg-icons";

import "./Exam.css";

import EmptyPage from "../../components/EmptyPage/Empty";

//
function Exams() {
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [isNextPage, setIsNextPage] = useState(false);
  const [empty, setEmpty] = useState(false);
  let pageNumber = 1;

  // load data
  const loadData = (pageNumber = 1) => {
    let dataTmp = data;
    setDataLoading(true); // show loading
    setIsNextPage(false); // set more false
    if (pageNumber === 1) dataTmp = []; // set data null

    // get data
    ExamApi.findAll({ page: pageNumber })
      .then(function (response) {
        if (
          response !== null &&
          response.exams !== null &&
          response.exams.length > 0
        ) {
          setData([...dataTmp, ...response.exams]);
          if (response.exams.next_page_url != null) setIsNextPage(true);
        }
        if (response.exams.length == 0) {
          setEmpty(true);
          console.log(response.exams)
        }
        console.log("empty:", empty);
        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData(1);
  }, []);

  //
  const btnMore = () => {
    pageNumber += 1;
    loadData(pageNumber);
  };
  console.log(data)
  return (
    <>
      {/*<Header title="آزمون ها" subTitle="نام شخص" banner={headerImage} image={image} />*/}
      <Header title="آزمون ها" banner={'/assets/images/exams.png'} />

      <Container maxWidth="lg">
        <Typography component='p' sx={{ mb: 4 }}>
          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
        </Typography>
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <ExamsSidebar setData={setData}/>
            <main className="panel-content-box">
              {/* <Grid container spacing={2}>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  md={6}
                  lg={4}
                  xl={4}

                >
                  <div className="exam-box">
                    <img className="image" src='' />
                    <div className="row-bottom">
                      <div className="title">لورم ایپسوم متن ساختگی با تولید </div>
                      <div className="audience">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و
                      </div>
                      <div className="des">
                        مخاطب 12 سال
                      </div>

                      <div className="btn-box">
                        <Box
                          sx={{ backgroundColor: 'transparent', display: 'inline-block' }}
                        >
                          2,000,000 تومان
                        </Box>
                        <Link
                          className="link"
                          to=""
                        >
                          نمایش آزمون
                        </Link>

                      </div>
                    </div>
                  </div>

                </Grid>





              </Grid> */}
                <Grid container spacing={2}>
                {data != null && data.length > 0
                  ? data.map((item) => {
                      return (
                        <Grid
                        item
                        xs={12}
                        sm={6}
                        md={6}
                        lg={4}
                        xl={4}
      
                      >
                        <div className="exam-box">
                          <img className="image" src={item.img} />
                          <div className="row-bottom">
                            <div className="title"> {item.title} </div>
                            <div className="audience">
                                {General.cleanHtml(item.info.substring(0, 300))}
                         
                            </div>
                              <div className="des">
                              {General.cleanHtml(item.short_info.substring(0, 100))}
                           {}
                            </div>
      
                            <div className="btn-box">
                              <Box
                                sx={{ backgroundColor: 'transparent', display: 'inline-block' }}
                                >
                                  قیمت:
       {item.price}
                              </Box>
                              <Link
                                className="link"
                                to=""
                              >
                                نمایش آزمون
                              </Link>
      
                            </div>
                          </div>
                        </div>
      
                      </Grid>
      
                      );
                    })
                  : null}
              </Grid>
              {isNextPage ? (
                <Grid container spacing={2}>
                  <Button onClick={btnMore} className="btn-more">
                    مشاهده بیشتر
                    <FontAwesomeIcon icon={faCaretDown} className="icon" />
                  </Button>
                </Grid>
              ) : null}
              {dataLoading ? <Loading /> : null}
              {empty ? (
                <EmptyPage description="شما هنوز آزمونی ندارید" />
              ) : null}
            </main>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default Exams;
