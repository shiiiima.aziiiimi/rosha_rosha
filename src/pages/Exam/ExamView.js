import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import ExamApi from "../../services/ExamApi";
import General from "../../utils/General";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import {
  faPlus,
  faMinus,
  faPlay,
  faHeart,
  faChartColumn,
  faClock,
  faAlignLeft,
  faBookOpen,
} from "@fortawesome/free-solid-svg-icons";
import headerImage from "../../assets/images/header/header-exam.png";
import video11 from "../../assets/videos/video11.mp4";
import videoImage from "../../assets/images/unsplash_Yi9-QIObQ1o (1).png";
import { panelMenuItems } from "../../utils/Data";
import logoImage from "../../assets/images/4.png";
import { useAuth } from "../../Contex/AuthProvider";
import FavoriteApi from "../../services/FavoriteApi";
// import CommentApi from "../../services/CommentApi";
import "./ExamView.css";

//
export const ERROR_MESSAGES = {
  // "Cannot find user": "کاربری با این ایمیل یافت نشد!",
  // "Incorrect password": "کلمه عبور اشتباه می‌باشد!",
  // "Empty Data": "اطلاعات را وارد نمایید!",
  // "already logged in": "کاربر با این مشخصات وارد شده است.",
  // "bad credentials": "اطلاعات اشتباه می باشد.",
  // "Clinic admin logged in": "کاربر با این مشخصات وارد شده است.",
  // "User logged in": "کاربر با این مشخصات وارد شده است.",
  // "Problem": "مشکلی در ورود کاربر پیش آمده است.",
};

//
function ExamView(props) {
  // get exam id
  const examId = parseInt(props.match.params.id, 0);

  //
  const [data, setData] = useState([]);
  const [dataLoading, setDataLoading] = useState(true);
  const [formLoading, setFormLoading] = useState(false);
  const [isPlay, setIsPlay] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const onEmailInputChange = (event) => setEmail(event.target.value);
  const onPasswordInputChange = (event) => setPassword(event.target.value);
  const { initAuth, user } = useAuth();
  const [commentName, setCommentName] = useState("");
  const [commentEmail, setCommentEmail] = useState("");
  const [commentMessage, setCommentMessage] = useState("");
  const [commentError, setCommentError] = useState("");
  const onCommentNameInputChange = (event) =>
    setCommentName(event.target.value);
  const onCommentEmailInputChange = (event) =>
    setCommentEmail(event.target.value);
  const onCommentMessageInputChange = (event) =>
    setCommentMessage(event.target.value);

  const [faqData, setFaqData] = useState([
    {
      active: false,
      id: "1",
      question:
        "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است؟",
      answer:
        "کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.",
    },
    {
      active: false,
      id: "2",
      question:
        "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است؟",
      answer:
        "کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.",
    },
    {
      active: false,
      id: "3",
      question:
        "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است؟",
      answer:
        "کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.",
    },
    {
      active: false,
      id: "4",
      question:
        "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است؟",
      answer:
        "کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.",
    },
    {
      active: false,
      id: "5",
      question:
        "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است؟",
      answer:
        "کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.",
    },
  ]);

  // load data
  const loadData = () => {
    setDataLoading(true); // show loading

    // get data
    ExamApi.findById(examId)
      .then(function (response) {
        if (response !== null) {
          // setData([...dataTmp, ...response.exams]);
        }

        setDataLoading(false); // hide loading
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  // comment
  const handleComment = (event) => {};

  // favorite
  const handleFavorite = (event) => {
    setIsFavorite(!isFavorite);

    //
    FavoriteApi.save(examId)
      .then(function (response) {
        if (
          response !== null &&
          response.message === "favorite was created successfully"
        ) {
          setIsFavorite(!isFavorite);
        } else {
          setIsFavorite(isFavorite);
        }
      })
      .catch(function (error) {
        setIsFavorite(isFavorite);
      });
  };

  // faq
  const handleFaq = (index) => () => {
    const faqDataTemp = faqData;
    faqDataTemp[index].active = !faqDataTemp[index].active;
    setFaqData([...faqDataTemp, ...[]]);
  };

  // video play
  const handleVideoPlay = () => {
    const video = document.getElementById("video");
    setIsPlay(true);

    video.controls = true;
    video.play();
  };

  //
  return (
    <div className="page-exam-view">
      <Header title="آزمون هالند" banner={headerImage} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            {!dataLoading ? (
              <>
                <main className="panel-content-box panel-content-box-exam">
                  <p className="exam-des">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                    با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه
                    و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                    تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                    کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                    آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم
                    افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                    طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                  </p>
                  <div className="video-box">
                    <video id="video" poster={videoImage}>
                      <source src={video11} type="video/mp4" />
                    </video>
                    {!isPlay ? (
                      <FontAwesomeIcon
                        icon={faPlay}
                        className="icon"
                        onClick={handleVideoPlay}
                      />
                    ) : null}
                  </div>
                  <div className="title-bar">لورم ایپسوم متن ساختگی</div>
                  <p className="exam-des">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                    با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه
                    و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                    تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                    کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                    آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم
                    افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                    طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                  </p>

                  {faqData != null && faqData.length > 0 ? (
                    <div className="faq-box">
                      {faqData.map((item, index) => {
                        return (
                          <div className="question-answer">
                            <div
                              className="question"
                              onClick={handleFaq(index)}
                            >
                              <p>{item.question}</p>
                              {!item.active ? (
                                <FontAwesomeIcon
                                  icon={faPlus}
                                  className="icon"
                                />
                              ) : (
                                <FontAwesomeIcon
                                  icon={faMinus}
                                  className="icon"
                                />
                              )}
                            </div>
                            {item.active ? (
                              <div className="answer">{item.answer}</div>
                            ) : null}
                          </div>
                        );
                      })}
                    </div>
                  ) : null}

                  <div className="title-bar comment-title-bar">
                    دیدگاهتان را بنویسید
                  </div>
                  <div className="form-comment-box">
                    <form onSubmit={handleComment} autoComplete="off">
                      <input
                        onChange={onCommentNameInputChange}
                        value={commentName}
                        type="text"
                        placeholder="نام"
                        autoComplete="false"
                      />
                      <input
                        onChange={onCommentEmailInputChange}
                        value={commentEmail}
                        type="email"
                        placeholder="ایمیل"
                        autoComplete="false"
                      />
                      <textarea
                        onChange={onCommentMessageInputChange}
                        placeholder="پیام"
                      >
                        {commentMessage}
                      </textarea>
                      {dataLoading ? (
                        <Loading />
                      ) : (
                        <button
                          onClick={handleComment}
                          type="button"
                          className="btn btn-primary"
                        >
                          ارسال
                        </button>
                      )}
                    </form>
                  </div>
                  {commentError && (
                    <div className="form-comment-alert">
                      {ERROR_MESSAGES[commentError] ?? commentError}
                    </div>
                  )}
                </main>

                <aside className="panel-exam-side-bar">
                  <a href="" className="btn-exam-buy">
                    خرید آزمون
                  </a>
                  <div className="exam-price">
                    هزینه: <span className="price">15.000 توماان</span>
                  </div>

                  <button
                    onClick={handleFavorite}
                    type="button"
                    className={
                      isFavorite
                        ? "btn-exam-favorite active"
                        : "btn-exam-favorite"
                    }
                  >
                    <FontAwesomeIcon icon={faHeart} className="icon" /> افزودن
                    به علاقه مندی ها
                  </button>

                  <div className="title-bar">لورم ایپسوم متن ساختگی</div>
                  <p className="exam-des">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                    با استفاده از طراحان گرافیک است.
                  </p>

                  <div className="exam-data-box">
                    <div className="exam-data">
                      <FontAwesomeIcon icon={faClock} className="icon" />
                      <div className="title">زمان</div>
                      <div className="value">30 دقیقه</div>
                    </div>

                    <div className="exam-data">
                      <FontAwesomeIcon
                        icon={faAlignLeft}
                        className="icon icon2"
                      />
                      <div className="title">تعدااد سوال</div>
                      <div className="value">25 سوال</div>
                    </div>

                    <div className="exam-data">
                      <FontAwesomeIcon
                        icon={faChartColumn}
                        className="icon icon2"
                      />
                      <div className="title">گروه سنی</div>
                      <div className="value">25-30 سال</div>
                    </div>
                  </div>

                  <div className="book-introduction">
                    <div className="title">
                      <FontAwesomeIcon icon={faBookOpen} className="icon" />
                      معرفی کتاب
                    </div>
                    <img className="image" src={videoImage} />
                    <div className="book-title">کتاب آدم حسابی ها</div>
                    <div className="book-des">
                      با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
                      الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                    </div>
                    <a href="" className="btn-book-buy">
                      خرید کتاب
                    </a>
                  </div>
                </aside>
              </>
            ) : (
              <Loading />
            )}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
export default ExamView;
