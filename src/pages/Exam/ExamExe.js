// import React, { useState, useEffect } from "react";
// import Header from "../../components/Header/Header";
// import Container from "@mui/material/Container";
// import Grid from "@mui/material/Grid";
// import { Link } from "react-router-dom";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
// import PanelMenu from "../../components/PanelMenu/PanelMenu";
// import { Button } from "@mui/material";
// import Loading from "../../components/Loading/Loading";
// import axios from "axios";
// import StepWizard from "react-step-wizard";
// import { useAuth } from "../../Contex/AuthProvider";
// import General from "../../utils/General";
// import ExamExeApi from "../../services/ExamExeApi";
// import { Box } from "@mui/system";
// import Radio from "@mui/material/Radio";
// import RadioGroup from "@mui/material/RadioGroup";
// import FormControlLabel from "@mui/material/FormControlLabel";
// import FormControl from "@mui/material/FormControl";
// import FormLabel from "@mui/material/FormLabel";

// const Step1 = (props) => {
//   const [askSelected, setAskSelected] = useState("1");
//   const handelChangeAsk = (event) => {
//     setAskSelected(event.target.id);
//   };
//   return (
//     <Box>
//       <Grid container sx={{ gap: "20px" }}>
//         <Grid item className="question-num">
//           1
//         </Grid>
//         <Grid item className="flex-1" sx={{ pt: 1 }}>
//           لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//         </Grid>
//       </Grid>
//       <Box className="questions-container" sx={{ mt: 3 }}>
//         <FormControl>
//           <RadioGroup
//             aria-labelledby="demo-radio-buttons-group-label"
//             defaultValue="female"
//             name="radio-buttons-group"
//             className="question-items"
//           >
//             <FormControlLabel
//               value="female"
//               control={<Radio onChange={handelChangeAsk} id="1" />}
//               label="Female"
//               className={askSelected == "1" ? `ask-selected` : ""}
//             />
//             <FormControlLabel
//               value="male"
//               control={<Radio onChange={handelChangeAsk} id="2" />}
//               label="Male"
//               className={askSelected == "2" ? `ask-selected` : ""}
//             />
//             <FormControlLabel
//               value="other"
//               control={<Radio onChange={handelChangeAsk} id="3" />}
//               label="Other"
//               className={askSelected == "3" ? `ask-selected` : ""}
//             />
//           </RadioGroup>
//         </FormControl>
//       </Box>
//       <Grid container className="footer-question" sx={{ mt: 4 }}>
//         <Grid item>
//           <img src="/assets/images/green.png" />
//         </Grid>
//         <Grid item sx={{ display: "flex", gap: "20px" }}>
//           <Button
//             onClick={props.previousStep}
//             className="btn-signup"
//             sx={{ px: 3, pt: 0 }}
//           >
//             سوال قبلی
//           </Button>
//           <Button
//             onClick={props.nextStep}
//             className="btn-signup"
//             sx={{ px: 3, pt: 0 }}
//           >
//             سوال بعدی
//           </Button>
//         </Grid>
//       </Grid>
//     </Box>
//   );
// };
// const Step2 = (props) => {
//   return (
//     <div>
//       step
//       <button onClick={props.previousStep}>Back</button>
//     </div>
//   );
// };
// const Back = (props) => {
//   return <button onClick={props.previousStep}>Back</button>;
// };

// function ExamExecution(props) {
//   const [questions, setQuestions] = useState([]);

//   const { initAuth, user } = useAuth();
//   useEffect(() => {
//     ExamExeApi.find({ userId: "2819" }).then(function (response) {
//       console.log(response);
//     });
//   }, []);

//   return (
//     <>
//       <Grid container className="exam-exe-header" sx={{ px: 3 }}>
//         <Grid
//           item
//           md={11}
//           sx={{
//             m: "auto",
//             display: "flex",
//             flexWrap: "wrap",
//             gap: "20px",
//             color: "#fff",
//           }}
//         >
//           <Box>
//             <Box className="exam-exe-img">
//               <img src="/assets/images/children.png" />
//             </Box>
//           </Box>
//           <Box
//             sx={{
//               display: "flex",
//               justifyContent: "end",
//               flexDirection: "column",
//             }}
//           >
//             <Box sx={{ fontWeight: "700", fontSize: "22px" }}>
//               لورم ایپسوم متن
//             </Box>
//             <Box sx={{ fontWeight: "400", fontSize: "16px", mt: 1 }}>
//               لورم ایپسوم متن
//             </Box>
//           </Box>
//         </Grid>
//       </Grid>
//       <Grid container className="exam-exe-container" sx={{ py: 4 }}>
//         <Grid item md={11} sx={{ m: "auto" }}>
//           <StepWizard initialStep={2}>
//             <Step1 />
//             <Step2 />
//           </StepWizard>
//         </Grid>
//       </Grid>
//     </>
//   );
// }
// export default ExamExecution;

import React, { useState, useEffect } from "react";
// import Header from "../../components/Header/Header";
// import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
// import { Link } from "react-router-dom";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
// import PanelMenu from "../../components/PanelMenu/PanelMenu";
import { Button } from "@mui/material";
import Loading from "../../components/Loading/Loading";
import axios from "axios";
import StepWizard from "react-step-wizard";
// import { useAuth } from "../../Contex/AuthProvider";
// import General from "../../utils/General";
import ExamExeApi from "../../services/ExamExeApi";
import { Box } from "@mui/system";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
// import FormLabel from '@mui/material/FormLabel';
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import imglogo from "../../assets/images/4.png";
import imgheader from "../../assets/images/کودکان 1.png";
import "./ExamExe.css";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#fff",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const QuestionStep = (props) => {
  const [askSelected, setAskSelected] = useState("0");
  const [isSelected, setIsSelected] = useState(false);
  const handelChangeAsk = (id, quesId, radioValue, answer) => {
    setAskSelected(id);
    setIsSelected(true);

    const tempAnswers = [...props.allAnswers];
    if (tempAnswers.length) {
      let flag = false;
      tempAnswers.map((item) => {
        if (item.quesId == quesId) {
          item.id = id;
          item.quesId = quesId;
          item.value = radioValue;
          item.answer = answer;

          flag = true;
        }
      });

      if (!flag) {
        tempAnswers.push({
          id: id,
          quesId: quesId,
          value: radioValue,
          answer: answer,
        });
      }
    } else {
      tempAnswers.push({
        id: id,
        quesId: quesId,
        value: radioValue,
        answer: answer,
      });
    }

    props.setAllAnswers(tempAnswers);
    console.log("allAnswers2", tempAnswers);
  };

  return (
    <Box>
      <Grid container sx={{ gap: "20px" }}>
        <Grid item className="question-num">
          {props.id}
        </Grid>
        <Grid item className="flex-1" sx={{ pt: 1 }}>
          {props.question}
        </Grid>
      </Grid>
      <Box className="questions-container" sx={{ mt: 3 }}>
        <FormControl>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue="female"
            name="radio-buttons-group"
            className="question-items"
          >
            {props.answers
              ? props.answers.map((item) => {
                  return (
                    <FormControlLabel
                      value={item.value}
                      control={
                        <Radio
                          onChange={() =>
                            handelChangeAsk(
                              item.id,
                              item.question_id,
                              item.value,
                              item.answer
                            )
                          }
                          value={item.value}
                        />
                      }
                      label={item.answer}
                      className={askSelected == item.id ? `ask-selected` : ""}
                      key={item.id}
                    />
                  );
                })
              : null}
          </RadioGroup>
        </FormControl>
      </Box>
      <Grid container className="footer-question" sx={{ mt: 4 }}>
        <Grid item className="footer-img-holder">
          <img src={imglogo} />
        </Grid>
        <Grid item sx={{ display: "flex", gap: "20px" }}>
          {props.numOfQuestions == props.id ? (
            <Button
              onClick={() => props.handleOpenModal(true)}
              className="btn-signup"
              disabled={isSelected ? false : true}
              sx={{ px: 3, pt: 0 }}
            >
              پایان آزمون
            </Button>
          ) : (
            <Button
              onClick={props.nextStep}
              className="btn-signup"
              disabled={isSelected ? false : true}
              sx={{ px: 3, pt: 0 }}
            >
              سوال بعدی
            </Button>
          )}

          <Button
            onClick={props.previousStep}
            className="btn-signup"
            disabled={props.id == 1 ? true : false}
            sx={{ px: 3, pt: 0 }}
          >
            سوال قبلی
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

function ExamExecution(props) {
  // get user exam id
  const userExamId = parseInt(props.match.params.id, 0);


  const [examQuestions, setExamQuestions] = useState([]);
  const [examInfo, setExamInfo] = useState({});
  const [dataLoading, setDataLoading] = useState(true);
  const [examAnswers, setExamAnswers] = useState([]);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);



  useEffect(() => {
    ExamExeApi.find(userExamId)
      .then(function (response) {
        console.log(response)
        if (response !== null) {
          if (response.questions !== null && response.questions.length > 0) {
            setExamQuestions([...response.questions]);
          }
          if (response.exam !== null) {
            setExamInfo({
              ...response.exam,
            });
            //  console.log('exam', response.exam)
          }
        }

        setDataLoading(false);
      })
      .catch(function (error) {
        setDataLoading(false); // hide loading
      });
  }, []);

  //console.log(examQuestions)
  //console.log(examInfo)
  return (
    <>
      <Grid container className="exam-exe-header" sx={{ px: 3 }}>
        <Grid
          item
          md={11}
          sx={{
            m: "auto",
            display: "flex",
            flexWrap: "wrap",
            gap: "20px",
            color: "#fff",
          }}
        >
          <Box>
            <Box className="exam-exe-img">
              <img src={imgheader} />
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "end",
              flexDirection: "column",
            }}
          >
            <Box sx={{ fontWeight: "700", fontSize: "22px" }}>
              {examInfo && examInfo.title ? examInfo.title : ""}
            </Box>
            <Box sx={{ fontWeight: "400", fontSize: "16px", mt: 1 }}>
              {examInfo && examInfo.short_info ? (
                <p dangerouslySetInnerHTML={{ __html: examInfo.short_info }} />
              ) : (
                ""
              )}
            </Box>
          </Box>
        </Grid>
      </Grid>
      <Grid container className="exam-exe-container" sx={{ py: 4 }}>
        <Grid item md={11} sx={{ m: "auto" }}>
          <StepWizard initialStep={1}>
            {!dataLoading ? (
              examQuestions && examQuestions.length !== 0 ? (
                examQuestions.map((item) => {
                  return (
                    <QuestionStep
                      key={item.id}
                      id={parseInt(item.id)}
                      question={item.question}
                      answers={item.answerDatas}
                      setAllAnswers={setExamAnswers}
                      allAnswers={[...examAnswers]}
                      numOfQuestions={examQuestions.length}
                      handleOpenModal={handleOpen}
                    />
                  );
                })
              ) : null
            ) : (
              <Loading />
            )}
          </StepWizard>
        </Grid>

        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ textAlign: "center" }}
            >
              آزمون شما با موفقیت پایان یافت
            </Typography>
          </Box>
        </Modal>
      </Grid>
    </>
  );
}
export default ExamExecution;
