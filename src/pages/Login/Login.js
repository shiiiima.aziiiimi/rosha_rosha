import { useAuth } from "../../Contex/AuthProvider";
import React, { useEffect, useState } from "react";
import { useHistory, Redirect } from "react-router-dom";
import Grid from "@mui/material/Grid";
import logoImage from "../../assets/images/4.png";
import Loading from "../../components/Loading/Loading";
import "./Login.css";
import { Button, TextField } from "@mui/material";
import UserApi from "../../services/UserApi";
import InputLabel from "@mui/material/InputLabel/InputLabel";

//
export const ERROR_MESSAGES =
    {
        "Cannot find user": "کاربری با این ایمیل یافت نشد!",
        "Incorrect password": "کلمه عبور اشتباه می‌باشد!",
        "Empty Data": "اطلاعات را وارد نمایید!",
        "already logged in": "کاربر با این مشخصات وارد شده است.",
        "bad credentials": "اطلاعات اشتباه می باشد.",
        "Clinic admin logged in": "کاربر با این مشخصات وارد شده است.",
        "User logged in": "کاربر با این مشخصات وارد شده است.",
        Problem: "مشکلی در ورود کاربر پیش آمده است.",
    };

//
const Login = () =>
{
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const [formSaving, setFormSaving] = useState(false);

    const onEmailInputChange = (event) => setEmail(event.target.value);
    const onPasswordInputChange = (event) => setPassword(event.target.value);
    const { initAuth, user } = useAuth();
    const history = useHistory();

    //
    useEffect(() =>
    {
        if (user.loggedIn)
        {
            history.push("/dashboard");
        }
    }, [user]);

    const handleLogin = (event) =>
    {
        setError("");

        //
        if (email !== "" && password !== "")
        {
            setFormSaving(true); // show loading

            UserApi.login({ email: email, password: password }).then
            (
                function (response)
                {
                    if (response !== null)
                    {
                        if (response.status === "200")
                        {
                            initAuth(true, response.token, response.user.full_name, response.user.phone, response.user.profile_picture);
                            //window.location = "/dashboard";
                        }
                        else if (response.email !== null && response.email.length > 0)
                        {
                            setError(response.email[0]);
                        }
                        else if (response.message !== "")
                        {
                            setError(response.message);
                        }
                        else
                        {
                            setError("Problem");
                        }
                    }
                    else
                    {
                        setError("Problem");
                    }

                    //
                    setFormSaving(false); // hide loading
                }
            ).catch(function (error)
            {
                setFormSaving(false); // hide loading
                setError("Problem");
            });
        }
        else
        {
            setError("Empty Data");
        }
    };

    //
    return (
        <div className="page-login">
            <div className="form-login-box">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <img className="logo" src={logoImage} />
                        <div className="title">ورود به پنل</div>
                    </Grid>

                    <Grid item xs={7}>
                        <form onSubmit={handleLogin} autoComplete="off">
                            <Grid container>
                                <Grid item xs={12}>
                                    <TextField className="field-input" label="ایمیل" variant="outlined" onChange={onEmailInputChange} value={email} />
                                </Grid>

                                <Grid item xs={12}>
                                    <TextField className="field-input" label="رمز عبور" variant="outlined" type="password" autoComplete="current-password" onChange={onPasswordInputChange} value={password} />
                                </Grid>

                                {formSaving ? <Loading /> : <Button onClick={handleLogin} variant="contained">ورود</Button>}
                            </Grid>
                        </form>
                    </Grid>

                    <Grid item xs={5}>{error && (<div className="alert">{ERROR_MESSAGES[error] ?? error}</div>)}</Grid>
                </Grid>
            </div>
        </div>
    );
};

export default Login;
