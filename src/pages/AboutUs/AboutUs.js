import React, { useEffect, useState } from "react";
import "./AboutUs.css";
import CountUp from "react-countup";
import image from "../../assets/images/unsplash_KdeqA3aTnBY (1).png";
import img1 from "../../assets/images/unsplash_-uHVRvDr7pg.png";
import img2 from "../../assets/images/unsplash_Jk3u514GJes (1).png";
import { Container, Grid } from "@mui/material";
import SliderAboutUs from "./SliderAboutUs";
import axios from "axios";
import SliderWhyRosha from "./SliderWhyRosha";
import Header from "../../components/Header/Header";
import { Link } from "react-router-dom";
function AboutUs() {
  const [talent_scout_count, settalent_scout_count] = useState(0);

  const [council_count, setcouncil_count] = useState(0);
  const [items_count, setitems_count] = useState(0);
  function counts() {
    axios.get("http://localhost:8000/api/aboutus").then((res) => {
      settalent_scout_count(res.data.talent_scout_count);

      setcouncil_count(res.data.council_count);
      setitems_count(res.data.items_count);
      console.log(
        "talent_scout_count: ",
        talent_scout_count,
        "council_count ",
        council_count,
        "items_count",
        items_count
      );
    });
  }
  useEffect(() => {
    counts();
  }, []);
  return (
    <div className="page-about-us">
      <Header banner={image} title="درباره ما" />
      <Container maxWidth="xl">
        <div class="context text-about-us">
          <p>
            مرکز رویش و شکوفایی استعداد روشا از سال 1394 با کسب مجوز های لازم در
            حوزه استعدادیابی و استعدادپروری کودک و نوجوان شروع به فعالیت های
            علمی و پژوهشی و ایجاد زیرساخت های لازم و انجام طرح های سراسری کرد،
            با توجه به توسعه فعالیت های این مجموعه از فروردین سال 1400 با
            رویکردی جدید و استقرار در مرکز نوآوری دانشکده روانشناسی و علوم
            تربیتی دانشگاه تهران، با حضور معاونت علمی و فناوری ریاست جمهوری جناب
            آقای دکتر ستاری، این فعالیت ها در حوزه های تخصصی ارزیابی و غربالگری،
            کشف استعداد، پرورش استعداد و شتابدهی و حمایت از استعدادهای کودکان و
            نوجوانان متناظر با اهداف مرکز نوآوری آغاز به کار نمود. مرکز
            استعدادیابی روشا با حمایت وزارت تعاون، کار و رفاه اجتماعی و بنیاد
            ملی توسعه فناوری های فرهنگی و با بکارگیری جدید ترین و معتبرترین روش
            ها و ابزارهای سنجش استعداد و همچنین با بهره گیری از دانش و تجارب
            اعضای محترم هیئت علمی دانشگاه ها، در جهت مهارت آموزی، مشاوره،
            استعدادیابی، استعدادپروری و توانمندسازی کودک و نوجوان، در تلاش است
            با رویکرد بومی خدماتی نوینی را به صورت حضوری و غیرحضوری به جامعه
            مخاطبین اعم از دانشجویان، خانواده ها و دیگر اقشار جامعه ارائه نموده
            و زمینه سازی رشد و بالندگی نسل های آینده کشور را فراهم آورد. همچنین
            از دیگر خدمات شتابدهنده روشا، شناسایی، شتابدهی، تجاری سازی ایده های
            حوزه روانشناسی و علوم تربیتی و کودک و نوجوان، مشاوره کسب و کار و
            همچنین حمایت از کسب و کارهای نوپای این حوزه است. در همین زمینه، این
            مجموعه با بهره گیری از تیمی حرفه ای و همچنین مشاوره اساتید و صاحب
            نظران حوزه کسب و کار فرصتی بی نظیر در جهت کسب تجربه و نگرش عمیق،
            توسعه و موفقیت نوآوران را فراهم آورده است..
          </p>
        </div>

        <div class="area">
          <ul class="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
      </Container>
      {/* <Grid container className="numbers-about-us">
        <Grid item xs={12} md={3} className="items-about-us">
          <CountUp end={talent_scout_count} duration={2} />
          <h5>آزمون اجرا شده</h5>
        </Grid>
        <Grid item xs={12} md={3} className="items-about-us">
          <CountUp end={council_count} duration={5} />
          <h5>ساعت مشاوره تخصصی</h5>
        </Grid>
        <Grid item xs={12} md={3} className="items-about-us">
          <CountUp end={items_count} duration={5} />
          <h5> عضو آکادمی استعداد</h5>
        </Grid>

      </Grid> */}
      <div className="section-count">
        <Container maxWidth="xl">
          <Grid container>
            <Grid item xs={6} sm={4} className="item">
              <CountUp
                end={talent_scout_count}
                duration={2}
                className="number"
              />
              <div className="title">آزمون اجرا شده</div>
            </Grid>

            <Grid item xs={6} sm={4} className="item">
              <CountUp end={council_count} duration={5} className="number" />
              <div className="title">ساعت مشاوره تخصصی</div>
            </Grid>

            <Grid item xs={6} sm={4} className="item">
              <CountUp
                end={talent_scout_count}
                duration={5}
                className="number"
              />
              <div className="title"> عضو آکادمی استعداد</div>
            </Grid>
          </Grid>
        </Container>
      </div>
      {/* <Grid container className="why-rosha">
        <Grid item xs={12} className="why-rosha-holder">
          <div>
            <h4>چرا روشا؟</h4>
            <div className="line"></div>
          </div>
        </Grid>

        <Grid container spacing={1} className="video-why-rosha-wrapper">
          <Grid item xs={12} md={4} className="video-why-rosha">
            <img src={img1} />
            <div className="video-why-rosha-title">
              <div class="container">
                <a class="border-animation" href="#">
                  <p class="border-animation__inner">
                    ویش و شکوفایی استعداد روشا
                  </p>
                </a>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={4} className="video-why-rosha">
            <img src={img1} />
            <div class="container">
              <a class="border-animation" href="#">
                <p class="border-animation__inner">
                  ویش و شکوفایی استعداد روشا
                </p>
              </a>
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <div className="video-why-rosha">
              <img src={img1} />
              <div class="container">
                <a class="border-animation" href="#">
                  <p class="border-animation__inner">
                    ویش و شکوفایی استعداد روشا
                  </p>
                </a>
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid> */}
      <div className="panel-content-box">
        <Grid container spacing={4} className="mt-10">
          <Grid item xs={12} sm={12} md={12} className="content-data">
            <Container maxWidth="xl">
              <div className="title-bar">تک آزمونهای و آنلاین</div>
              <Grid container spacing={1}>
              <Grid item xs={12} md={4} sm={12} className="video-why-rosha">
              
                  <img src={img1} />
                  <div className="video-why-rosha-title">
                    <div class="container">
                      <Link to="/" class="border-animation">
                        <p class="border-animation__inner">
                          ویش و شکوفایی استعداد روشا
                        </p>
                      </Link>
                    </div>
                  </div>
            
              </Grid>
              <Grid item xs={12} sm={12} md={4} className="video-why-rosha">
       
                  <img src={img1} />
                  <div className="video-why-rosha-title">
                    <div class="container">
                      <Link to="/" class="border-animation">
                        <p class="border-animation__inner">
                          ویش و شکوفایی استعداد روشا
                        </p>
                      </Link>
                    </div>
                  </div>
          
              </Grid>
              <Grid item xs={12} md={4} className="video-why-rosha">
              
                  <img src={img1} />
                  <div className="video-why-rosha-title">
                    <div class="container">
                      <Link to="/" class="border-animation">
                        <p class="border-animation__inner">
                          ویش و شکوفایی استعداد روشا
                        </p>
                      </Link>
                    </div>
                  </div>
             
                </Grid>
                </Grid>
            </Container>
          </Grid>
        </Grid>
      </div>
      {/* <Grid container spacing={1} className="rosha-videos-about-us">
        <Grid item xs={12}>
          <Container maxWidth="xl" className="title-about-us-holder">
            <div className="title-bar">مرکز رویشا</div>
          </Container>
        </Grid>
        <Grid item xs={12} md={4} className="video-why-rosha">
          <Container maxWidth="lg">
            <img src={img1} />
            <div className="video-why-rosha-title">
              <div class="container">
                <Link to="/" class="border-animation">
                  <p class="border-animation__inner">
                    ویش و شکوفایی استعداد روشا
                  </p>
                </Link>
              </div>
            </div>
          </Container>
        </Grid>
        <Grid item xs={12} md={4} className="video-why-rosha">
          <Container maxWidth="lg">
            <img src={img1} />
            <div className="video-why-rosha-title">
              <div class="container">
                <Link to="/" class="border-animation">
                  <p class="border-animation__inner">
                    ویش و شکوفایی استعداد روشا
                  </p>
                </Link>
              </div>
            </div>
          </Container>
        </Grid>
        <Grid item xs={12} md={4} className="video-why-rosha">
          <Container maxWidth="lg">
            <img src={img1} />
            <div className="video-why-rosha-title">
              <div class="container">
                <Link to="/" class="border-animation">
                  <p class="border-animation__inner">
                    ویش و شکوفایی استعداد روشا
                  </p>
                </Link>
              </div>
            </div>
          </Container>
        </Grid>

        {/* <Grid item xs={6} md={3} className="">
              <Container maxWidth="xl">
                <div className="img-brand-holder">
                  <Link to="/">
                    {" "}
                    <img src={brandimg} />
                  </Link>
                </div>
              </Container>
            </Grid> */}
      {/* </Grid> */} 

      <Container maxWidth="xl">
        <Grid container className="about-us-info">
          <Grid item xs={12} md={6} className="info-holder">
            <div>
              <h4>درباره ما</h4>
              <div className="line"></div>
            </div>
            <div className="text-about-us">
              <p>
                {" "}
                مرکز رویش و شکوفایی استعداد روشا نیز در نظر دارد با ﺍﺭﺍﺋﻪ ﻣﻄﻠﻮﺏ
                ﺗﺮﻳﻦ، تخصصی ترین ﻭ به روز ترینﺧﺪﻣﺎﺕ، به ویژه آموزشی، سرگرمی،
                پرورش استعدادها، مشاوره های تحصیلی و شغلی ﻣﺘﻨﺎﺳﺐ ﺑﺎ ﻧﻴﺎﺯﻫﺎﻱ
                کودکان، نوجوانان همچنین بزرگسالان و ﺑﺎ ﻫﺪﻑ ﺍﻓﺰﺍﻳﺶ سطح دسترسي،
                ﺭﺿﺎﻳﺘﻤﻨﺪﻱ و ﺍﻋﺘﻤﺎد براي آحاد جامعه ايراني ﺍﺯ ﻃﺮﻳﻖ طراحي پلتفرمي
                جامع که در بردارنده امن ترین، سالم ترین و متنوع ترین نوع خدمات
                ویژه کودکان و نوجوانان ایران زمین است، به عنوان اولین و برترین
                مجموعه استعدادیابی کودک و نوجوان فعالیت کند.
              </p>
            </div>
            <SliderAboutUs />
          </Grid>
          <Grid item xs={12} md={6} className="img-about-us">
            <img src={img1} />
          </Grid>
        </Grid>
      </Container>

      <Grid container className="chesmandaz-about">
        <Grid item xs={12} md={6} className="">
          <img src={img2} />
        </Grid>
        <Grid item xs={12} md={6} className="info-holder">
          <div>
            <h4> چشم انداز</h4>
            <div className="line"></div>
          </div>
          <div className="text-about-us">
            <p>
              {" "}
              شناسايي، هدايت و حمايت از استعدادهاي برتر يكي از مهمترين مسائل
              مورد تأكيد صاحبنظران، مسئولان عالي نظام و اسناد بالادستي كشور است.
              همچنين، بخشهاي متعددي از «نقشه ي جامع علمي كشور» به لزوم شناسايي و
              هدايت مناسب استعدادهاي برتر اشاره دارد. بندهاي 3-6و3-5 در ذيل
              «اهداف نظام علم، فناوري و نوآوري كشور»، اقدام ملي چهل و دوم در ذيل
              «راهبرد كلان هشتم» و اقدام ملي چهاردهم در ذيل «راهبرد كلان دهم» از
              این موارد است. در صدر اين موارد، نخستين راهبرد ملّي و اولين اقدام
              ملّي در ذيل راهبرد كلان هشت قرار دارد كه به ترتيب بر ارتقا و طراحی
              نظام مشاوره، استعداديابي و هدايت به منظور هدايت دانش آموزان،
              دانشجويان و کارآموزان آموزش های مهارتی به سوي رشته ها و مشاغل،
              متناسب با علایق، ظرفیت ها، توانایی ها و شایستگی های فردی و همچنین
              اولويت هاي كشور تأكيد مي نمايند. طرح روشا درصدد ایجاد سامانة حضوري
              و الکترونیک شناسایی استعدادها و توانمندی‌ها و هدایت صحیح شغلی،
              تحصیلی و ورزشي كودكان و نوجوانان در سه رده سني 5 تا 11 سال تمام،
              12 تا 15 سال تمام و 16 سال به بالا می باشد. چشم انداز روشا ارائه
              خدمات استعدادیابی و استعدادپروری به تمامی نقاط کشور عزیزمان و در
              مرحله بعد در کشورهای همسایه می باشد.
            </p>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
export default AboutUs;
