import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import "./AboutUs.css";
// Import Swiper styles

// import Swiper core and required modules
import SwiperCore, { Pagination } from "swiper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import img from "../../assets/images/Paper.png";
// install Swiper modules
SwiperCore.use([Pagination]);

export default function SliderAboutUs() {
  return (
    <>
      <Swiper
        slidesPerView={3}
        spaceBetween={10}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          300: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          500: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          640: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 50,
          },
        }}
        className="mySwiper"
        id="about-us-slider"
      >
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p> مشاوره توسط اساتید هیئت علمی دانشکده روانشناسی تهران</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p> مشاوره توسط اساتید هیئت علمی دانشکده روانشناسی تهران</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p>
              {" "}
              استفاده از به روز ترین و معتبرترین روش ها و ابزار های سنجش و
              ارزیابی
            </p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p> ارائه خدمات به صورت حضوری و غیر حضوری</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p> پشتیبانی آنلاین</p>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p>ارزیابی کودکان 2ماهگی تا پنج سالگی توسط والدین </p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p> تست استعدادیابی حضوری</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p>تست استعدادیابی غیر حضوری</p>
          </div>{" "}
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p>آموزش های کدنویسی مبتنی بر افزایش مهارت های شناختی</p>
          </div>{" "}
        </SwiperSlide>
        <SwiperSlide>
          <div className="slider-card-aboutus">
            <FontAwesomeIcon
              icon={faFile}
              className="icon-slider-card-aboutus"
            />
            <p>
              شتابدهی و حمایت از طرح ها و ایده های نوآورانه حوزه روانشناسی و
              آموزش
            </p>
          </div>{" "}
        </SwiperSlide>
      </Swiper>
    </>
  );
}
