import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";

import TextField from "@mui/material/TextField";
import axios from "axios";
import headerimg from "../../assets/images/unsplash_744oGeqpxPQ.png";
import "./ReserveExam.css";
import image1 from "../../assets/images/unsplash_g1Kr4Ozfoac.png";
import { Button, Grid, Container } from "@mui/material";
import { Link } from "react-router-dom";
import Header from "../../components/Header/Header";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMobileAlt,
  faMapMarkerAlt,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";





import { Alert } from "@mui/material";
import General from "../../utils/General";
import ReserveApi from "../../services/ReserveApi";
import Select from "react-select";
import Loading from "../../components/Loading/Loading"

import Payment from "../../pages/Payment/Payment";
function Reserve() {


  const [currentAgency, setCurrentAgency] = useState(null);
  const options = [
    {
      label: " مشاوره حضوری - مبلغ سرمایه‌گذاری: 215 هزار تومان",
      value: "talent_scout_presence",
    },
    {
      label: " مشاوره غیر حضوری - مبلغ سرمایه‌گذاری: 227 هزار تومان",
      value: "talent_scout_none_presence",
    },
    {
      label:
        " آزمونهای رایانه ای توجه و تمرکز IVA توسط متخصص (همراه با تفسیر) - مبلغ سرمایه‌گذاری: 347 هزار تومان",
      value: "iva",
    },
    {
      label:
        " آزمونهای رایانه ای توجه و تمرکز ریهاکام توسط متخصص (همراه با تفسیر)-مبلغ سرمایه‌گذاری: 347 هزار تومان",
      value: "ریهاکام",
    },
    {
      label:
        " آزمون حضوری ارزیابی هوش بینه توسط متخصص (همراه با تفسیر)- مبلغ سرمایه‌گذاری: 487 هزار تومان",
      value: "بیمه",
    },
    {
      label:
        "   آزمون حضوری ارزیابی هوش وکسلر توسط متخصص (همراه با تفسیر)- مبلغ سرمایه‌گذاری:487 هزار تومان",
      value: "وکسلر",
    },
    {
      label:
        "  بسته استعدادیابی ۵ تا ۱۱ سال توسط اساتید مجرب روان شناسی - مبلغ سرمایه‌گذاری:847 هزار تومان",
      value: "استعدادیابی 5 تا 11 سال",
    },
    {
      label:
        " بسته استعدادیابی ۱۲ تا ۱۵ سال توسط اساتید مجرب روان شناسی - مبلغ سرمایه‌گذاری: 974 هزار تومان",
      value: "استعدادیابی 12 تا 15 سال",
    },
    {
      label:
        " بسته استعدادیابی ۱۶ سال به بالا توسط اساتید مجرب روان شناسی- مبلغ سرمایه‌گذاری:1 میلیون و 147 هزار تومان",
      value: "استعدادیابی 16 سال به بالا",
    },
  ];


  const [formLoading, setFormLoading] = useState(false);
  const [service, setService] = useState("");
  const [age, setAge] = useState("");
  const [agencies, setAgencies] = useState("");
  const [dataagency, setDataAgency] = useState([]);
  const [comment, setComment] = useState("");
  const [paymentId, setPaymentId, getPaymentId] = useState(0);
  const [commentError, setCommentError] = useState("");
  const [loading,setLoading]=useState(false)
 
  const onServiceInputChange = (event) => {



    setService(event.value);
  }
  const onAgeInputChange = (event) => setAge(event.target.value);
  const onAgencieslInputChange = (event) => {
    // console.log("::--"+JSON.stringify(event));
    
    setAgencies(event.value);
  }
  const onCommentInputChange = (event) => setComment(event.target.value);

  const getData = () => {
    // get data
    ReserveApi.find()
      .then(function (response) {
        setDataAgency(response.agencies);
        // if (
        //   response !== null &&
        //   response.items !== null &&
        //   response.items.length > 0
        // ) {
        //   setMinutes(response.minutes);
        //   setTeachers(response.council_count);
        //   setVebinars(response.items_count);
        //   setData([...dataTmp, ...response.items]);
        //   if (response.items.next_page_url != null) setIsNextPage(true);
        // }

        // setDataLoading(false); // hide loading
       
      })
      .catch(function (error) {
        // setDataLoading(false); // hide loading
        console.log(error);
        setCommentError(error);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  // comment
  const handleComment = (event) => {
    setLoading(true)
    ReserveApi.sendId({  type: service,
      age_range: age,
      agency_id: 1,
      info: comment,}).then
    (
        function (response)
      {
        console.log(response.payment_id)
        setPaymentId(response.payment_id);
        

  console.log("kdml",paymentId)
        }
    ).catch(function (error)
    {
      console.log("err", error)
      
    });
 setLoading(false)
  };
  
  let optionAgencies = dataagency.map((item, index) => {
    return {
      label: item.title + `(${item.city + " " + item.state})`,
      value: item.id,
    };
  });
  // const getInfo = () => {
  //   axios.get("http://localhost:8000/api/talent-scout/presence").then((res) => {
  //     setAgencies(res.data.agencies);
  //   });
  // };
  // useEffect(() => {
  //   getInfo();
  // }, []);

  return (
    <div className="page-reserve">
      <Header banner={headerimg} title="رزرو ارزیابی حضوری" />

      <div class="context text-about-us">
        <p>
          پکیجهای استعدادیابی حضوری روشا در سه بازه سنی 5 تا 11 سال، 11تا 16 سال
          و 16 سال به بالا طراحی شده است. استعدادیابی حضوری در مرکز روشا طی دو
          جلسه انجام می گیرد. جلسه اول آزمونهای مختلف شامل (هوش، شخصیت، رغبت
          شغلی و ...) با توجه به بازه سنی متقاضی انجام می گیرد. بعد از تحلیل
          نتایج آزمون توسط متخصصین مجموعه، در جلسه دوم پس از مشاوره تخصصی با فرد
          بزرگسال یا پدر و مادر کودک، نتیجه گیری کلی آزمون به همراه راهکارهای
          پیشنهادی برای مسیر تحصیلی و شغلی به وی ارائه می گردد.
        </p>
      </div>

      <div class="area">
        <ul class="circles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>

      <div className="page-section-advertise">
        <Container maxWidth="xl">
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6} className="image-holder">
              <img src={image1} className="image" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <div className="title-bar"> دوره ها و وبینارها</div>

              <p className="des">
                مرکز رویش و شکوفایی استعداد روشا در جهت آموزش و توانمندسازی
                خانواده ها و نوجوانان و کودکان،با برگزاری دوره ها و وبینارهای
                گوناگون، و با حضور اساتید برجسته و بنام حوزه های مختلف، سعی بر
                این دارد تا بخشی از دغدغه ی خانواده های عزیز را برطرف سازد.
              </p>

              <Link to={""} className="link">
                کلیک کنید
              </Link>
            </Grid>
          </Grid>
        </Container>
      </div>
      {/* <Grid container> */}
      {/* <Grid item xs={12} md={8} className="right-rosha-reserve">
          <div className="title-rosha-reserve">
            <h4>با مشاوران و اساتید روشا بیشتر آشنا شوید </h4>
            <div className="line"></div>
            <p>
              {" "}
              مرکز رویش و شکوفایی استعداد روشا، در فرایندهای استعدادیابی و
              مشاوره خود مفتخر به همکاری با اساتید و مشاوران برتر دانشگاههای
              برتر کشور می باشد.
            </p>
          </div>
          <Button variant="contained" className="btn-send" type="submit">
            کلیک
          </Button>
        </Grid> */}
      {/* <Grid item xs={12} md={4} className="left-rosha-reserve">
          <div className="img-holder-reserve">
            <img src={img1} />
          </div>
        </Grid> */}
      {/* </Grid> */}
      {/* <div className="full-line"></div> */}

      {/* <Grid container className="form-holder">
        <Grid item xs={12} className="form-wrapper">
          <FormControl fullWidth>
            <InputLabel id="input-reserve-choose-label">
              لطفا خدمت مورد نظر را انتخاب کنید{" "}
            </InputLabel>
            <Select
              labelId="input-reserve-choose-label"
              id="demo-simple-select"
              value={service}
              label="Age"
            >
              <MenuItem value={10}>

                مشاوره حضوری - مبلغ سرمایه‌گذاری: 215 هزار تومان
              </MenuItem>
              <MenuItem value={20}>
                مشاوره غیر حضوری - مبلغ سرمایه‌گذاری: 185 هزار تومان
              </MenuItem>
              <MenuItem value={30}>
                آزمونهای رایانه ای توجه و تمرکز IVA توسط متخصص (همراه با تفسیر)
                - مبلغ سرمایه‌گذاری: 289 هزار تومان
              </MenuItem>
              <MenuItem value={40}>
                آزمونهای رایانه ای توجه و تمرکز ریهاکام توسط متخصص (همراه با
                تفسیر)-مبلغ سرمایه‌گذاری: 289 هزار تومان
              </MenuItem>{" "}
              <MenuItem value={50}>
                آزمون حضوری ارزیابی هوش بینه توسط متخصص (همراه با تفسیر)- مبلغ
                سرمایه‌گذاری: 404 هزار تومان
              </MenuItem>{" "}
              <MenuItem value={60}>
                آزمون حضوری ارزیابی هوش وکسلر توسط متخصص (همراه با تفسیر)- مبلغ
                سرمایه‌گذاری: 404 هزار تومان
              </MenuItem>{" "}
              <MenuItem value={70}>
                بسته استعدادیابی ۵ تا ۱۱ سال توسط اساتید مجرب روان شناسی - مبلغ
                سرمایه‌گذاری: 656 هزار تومان
              </MenuItem>
              <MenuItem value={80}>
                بسته استعدادیابی ۱۲ تا ۱۵ سال توسط اساتید مجرب روان شناسی - مبلغ
                سرمایه‌گذاری: 724 هزار تومان
              </MenuItem>{" "}
              <MenuItem value={90}>
                بسته استعدادیابی ۱۶ سال به بالا توسط اساتید مجرب روان شناسی-
                مبلغ سرمایه‌گذاری: 861 هزار تومان
              </MenuItem>
            </Select>
          </FormControl>
          <TextField
            type="number"
            fullWidth
            id="age-input"
            label="سن نو اندیش را وارد کنید"
            variant="outlined"
          />

          <FormControl fullWidth>
            <InputLabel id="agencies-rosha">تمایندگی ها</InputLabel>
            {currentAgency}
            <Select
              labelId="agencies-rosha"
              id="agencies"
              value={currentAgency}
              label="مایندگی ها"
              onChange={(e) => {
                setCurrentAgency(e.target.value);
              }}
            >
              {agencies.map((item, index) => {
                console.log(item);
                return <MenuItem value={item.id}>{item.title}</MenuItem>;
              })}
            </Select>
          </FormControl>
          <TextField
            fullWidth
            id="note-rosha-reserve"
            label="اگر یادداشتی دارید بنویسید"
            multiline
            variant="outlined"
          />
        </Grid>
      </Grid> */}
      <Container maxWidth="xl">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="panel-content-box">
            <div className="form-contact">
      {/* <div className="title-bar form-title-bar">{title}</div>
      <div className="form-des">{des}</div> */}

      <div className="form-contact-box">
        <form onSubmit={handleComment} autoComplete="off">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Select
                onChange={onServiceInputChange}
                value={options.id}
                className="select-input-form-box"
                options={options}
                placeholder="لطفا خدمت مورد نظر را انتخاب کنید "
              />
            </Grid>

            <Grid item xs={12}>
              <input
                onChange={onAgeInputChange}
                value={age}
                type="text"
                placeholder="لطفا سن نواندیش را وارد کنید "
                autoComplete="false"
              />
            </Grid>

            <Grid item xs={12}>
              <Select
                 onChange={onAgencieslInputChange}
                id={dataagency.id}
                className="select-input-form-box"
                value={optionAgencies.id}
                options={optionAgencies}
                placeholder="نمایندگی ها"
              ></Select>
            </Grid>

            <Grid item xs={12}>
              <textarea
                onChange={onCommentInputChange}
                placeholder="اگر یادداشتی دارید، بنویسید"
              >
                {comment}
              </textarea>
            </Grid>

            <Grid item xs={12}>
              {formLoading ? (
                <Loading />
              ) : (
                  <Link
                    // to={"Payment/:id"}
                    onClick={handleComment}
                  to={{ pathname: "payment/" + paymentId , state:  paymentId }}
                  >
                               <button
              
                  type="button"
                  className="btn btn-primary"
                >
            ورود به صفحه پرداخت
                </button>
                  </Link>
              )}
            </Grid>
          </Grid>
        </form>
      </div>
      {commentError && (
        <div className="form-contact-alert">
        ارسال نشد
        </div>
      )}
    </div>
            </div>
          </Grid>
        </Grid>
      </Container>
      <div className="section-map-reserve-page">
        <div className="panel-content-box">
          <Container maxWidth="xl">
            <Grid container spacing={4}>
              <Grid item xs={12} sm={6} md={6}>
                <div className="title-bar">قوانین و مقررات</div>
                <p className="des">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                  استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                  مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                  تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                  کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته،
                </p>
                <h5 className="title">اطلاعات تماس دفتر مرکزی</h5>
                <div className="item-box">
                  <FontAwesomeIcon icon={faMobileAlt} className="icon" />
                  <span className="title">تلفن:</span>
                  <span className="des">02112345678</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faMapMarkerAlt} className="icon" />
                  <span className="title">آدرس:</span>
                  <span className="des">گیشا - دانشگاه تهران - مرکز رشد</span>
                </div>

                <div className="item-box">
                  <FontAwesomeIcon icon={faEnvelope} className="icon" />
                  <span className="title">پست الکترونیکی:</span>
                  <span className="des">sabkeZendegiNovin@gmail.com</span>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={6} className="contact-data">
                <iframe
                  className="google-map"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3892.033638913015!2d51.37288165543435!3d35.72756374238894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92a24f4f6bd4bdd4!2sFaculty%20of%20Psychology%20and%20Educational%20Sciences!5e0!3m2!1sen!2s!4v1643807925128!5m2!1sen!2s"
                  width="100%"
                  height="450"
                  allowFullScreen=""
                  loading="lazy"
                ></iframe>
              </Grid>
            </Grid>
          </Container>
        </div>
      </div>

      {/* <Grid container className="map-holder">
        <Grid item xs={12} className="map-wrapper">
          <Grid container>
            <Grid item xs={12} md={6} className="map-wrapper">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3892.033638913015!2d51.37288165543435!3d35.72756374238894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92a24f4f6bd4bdd4!2sFaculty%20of%20Psychology%20and%20Educational%20Sciences!5e0!3m2!1sen!2s!4v1643807925128!5m2!1sen!2s"
                width="100%"
                height="400"
                allowfullscreen=""
                loading="lazy"
              ></iframe>
            </Grid>
            <Grid item xs={12} md={6}>
              <div className="info-wrapper">
                <div className="">
                  <h4>مکان دفتر مرکزی روشا</h4>
                  <div className="line"></div>
                </div>
                <div className="info">
                  <i></i>
                  <p> تلفن دفتر مرکزی:02188234037</p>
                </div>
                <div className="info">
                  <i></i>
                  <p>تلفن گویا:02128425104</p>
                </div>
                <div className="info">
                  <i></i>
                  <p>
                    آدرس: تهران,پل گیشا,خیابان دکتر کاردان,مرکز نوآوری روانشناسی
                    و علوم تربیتی دانشگاه تهران
                  </p>
                </div>
                <div className="info">
                  <i></i>
                  <p>
                    {" "}
                    آدرس پست الکترونیکی دفتر مرکزی : sabkezendeginovin@gmail.com
                  </p>
                </div>
                <div className="info">
                  <i></i>
                  <p> آدرس پست الکترونیکی سازمانی :info@roshatalent.ir</p>
                </div>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid> */}
    </div>
  );
}
export default Reserve;
