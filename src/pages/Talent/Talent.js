import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCaretDown} from "@fortawesome/free-solid-svg-icons";
import PanelMenu from "../../components/PanelMenu/PanelMenu";
import TalentApi from "../../services/TalentApi";
import General from "../../utils/General";
import {Button} from "@mui/material";
import Loading from "../../components/Loading/Loading";

import headerImage from "../../assets/images/header/header-talent.png";
import "./Talent.css";

//
function Talent()
{
    //
    const [data, setData] = useState([]);
    const [dataLoading, setDataLoading] = useState(true);
    const [isNextPage, setIsNextPage] = useState(false);
    let pageNumber = 1;

    // load data
    const loadData = (pageNumber = 1) =>
    {
        let dataTmp = data;
        setDataLoading(true); // show loading
        setIsNextPage(false); // set more false
        if(pageNumber === 1) dataTmp = []; // set data null

        // get data
        TalentApi.find({ page: pageNumber }).then
        (
            function(response)
            {
                if(response !== null && response.talentscouts !== null && response.talentscouts.length > 0)
                {
                    setData([...dataTmp, ...response.talentscouts]);
                    if(response.talentscouts.next_page_url != null) setIsNextPage(true);
                }

                setDataLoading(false); // hide loading
            }
        ).catch
        (
            function(error)
            {
                setDataLoading(false); // hide loading
            }
        );
    };

    useEffect(() => { loadData(1); }, []);

    //
    const btnMore = () =>
    {
        pageNumber += 1;
        loadData(pageNumber);
    };

    //
    return(
        <>
            <Header title="مشاوره های حضوری" banner={headerImage} />

            <Container maxWidth="lg">
                <Grid container>
                    <Grid item xs={12} className="panel-page-content-grid">
                        <PanelMenu />
                        <main className="panel-content-box">
                            <Grid container>
                                <Grid item xs={12}>
                                    <div className="title-bar">مشاوره های من</div>
                                </Grid>
                            </Grid>

                            <Grid container spacing={2}>
                                {
                                    data != null && data.length > 0 ?
                                        data.map
                                        (
                                            item =>
                                            {
                                                return (
                                                    <Grid item xs={12} key={item.id}>
                                                        <div className="talent-box">
                                                            <div className="title">{item.title}</div>
                                                            <div className="des">{General.cleanHtml(item.info)}</div>
                                                            <div className="date-time">
                                                                <span>تاریخ: {item.date}</span>
                                                                <span>ساعت: {item.time}</span>
                                                            </div>
                                                        </div>
                                                    </Grid>
                                                );
                                            }
                                        )
                                        :
                                        null
                                }
                            </Grid>
                            {
                                isNextPage ?
                                    <Grid container spacing={2}>
                                        <Button onClick={btnMore} className="btn-more">مشاهده بیشتر<FontAwesomeIcon icon={faCaretDown} className="icon" /></Button>
                                    </Grid>
                                    :
                                    null
                            }
                            {
                                dataLoading ?
                                    <Loading />
                                    :
                                    null
                            }
                        </main>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
}
export default Talent;
