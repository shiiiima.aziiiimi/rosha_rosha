import React, { useState } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loading from "../../components/Loading/Loading";
import {faPlus, faMinus} from "@fortawesome/free-solid-svg-icons";
import headerImage from "../../assets/images/header/header-faq.png";
import {DataFaqs} from "../../utils/DataFaqs";
import FormContact from "../../components/FormContact/FormContact";
import "./Faq.css";

//
function Faq(props)
{
    //
    const [faqData, setFaqData] = useState(DataFaqs);

    // faq
    const handleFaq = (index) => () =>
    {
        const faqDataTemp = faqData;
        faqDataTemp[index].active = !faqDataTemp[index].active;
        setFaqData([...faqDataTemp, ...[]]);
    };

    //
    return (
        <div className="page-faq">
            <Header title="سوالات متداول" banner={headerImage} />

            <Container maxWidth="lg">
                <Grid container>
                    <Grid item xs={12} className="panel-page-content-grid">
                        <main className="panel-content-box">
                            {
                                faqData != null && faqData.length > 0 ?
                                    (
                                        <div className="faq-box">
                                            {
                                                faqData.map
                                                (
                                                    (item, index) =>
                                                    {
                                                        return (
                                                            <div className="question-answer">
                                                                <div className="question" onClick={handleFaq(index)}>
                                                                    <p>{item.question}</p>
                                                                    {!item.active ? (<FontAwesomeIcon icon={faPlus} className="icon"/>) : (<FontAwesomeIcon icon={faMinus} className="icon"/>)}
                                                                </div>
                                                                {item.active ? (<div className="answer">{item.answer}</div>) : null}
                                                            </div>
                                                        );
                                                    }
                                                )
                                            }
                                        </div>
                                    )
                                    :
                                    null
                            }

                            <FormContact title="سوال جدید" des="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد." />
                        </main>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}
export default Faq;
