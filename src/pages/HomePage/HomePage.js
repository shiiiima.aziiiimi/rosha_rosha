import React, { useState, useEffect } from "react"
// import Sidebar from "../../components/Slider/Slider";
import VerticalSlider from "../../components/VerticalSlider/VerticalSlider"
// import CardWrapper from "../../components/CardsWrapper/CardWrapper";
// import VideoWrapper from "../../components/VideoWrapper/VideoWrapper";
import GroupSlider from "../../components/VideoWrapper/VideoWrapper"
import PackageSlider from "../../components/PackageSlider/PackageSlider"
import { Swiper, SwiperSlide } from "swiper/react"
import "swiper/swiper-bundle.min.css"
import "./HomePage.css"

// import SwiperCore, { Autoplay, Pagination } from "swiper";

import serviceLogo1 from "../../assets/images/1.png"
import serviceLogo2 from "../../assets/images/2.png"
import serviceLogo3 from "../../assets/images/3.png"
import serviceLogo6 from "../../assets/images/6.png"
import serviceLogo7 from "../../assets/images/7.png"
import serviceLogo8 from "../../assets/images/8.png"
import serviceLogo9 from "../../assets/images/9.png"

import packageImage1 from "../../assets/images/16.png"
import pachageImage2 from "../../assets/images/12-15.png"
import packageImage3 from "../../assets/images/5-11.png"
import img1 from "../../assets/images/quz.jpg"
import img2 from "../../assets/images/imgslider1.jpg"
import img3 from "../../assets/images/imgSlider2.jpeg"
import { Grid, Stack, useMediaQuery, useTheme } from "@mui/material"
import Container from "@mui/material/Container"
import Drawer from "@mui/material/Drawer/Drawer"
import videoImage from "../../assets/images/unsplash_Yi9-QIObQ1o (1).png"
import video11 from "../../assets/videos/video11.mp4"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlay } from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom"

import PhotoAlbum from "react-photo-album"
import HomeApi from "../../services/HomeApi"

//
function HomePage() {
  const [isPlay, setIsPlay] = useState(false)
  const [slide, setSlide] = useState([])
  // video play
  const handleVideoPlay = () => {
    const video = document.getElementById("video")
    setIsPlay(true)

    video.controls = true
    video.play()
  }

  const photos = [
    { src: videoImage, width: 800, height: 500 },
    { src: img3, width: 1600, height: 1300 },
    { src: videoImage, width: 800, height: 700 },
    { src: img3, width: 1200, height: 800 },
    { src: videoImage, width: 800, height: 800 },
    { src: img3, width: 1600, height: 900 },
    { src: videoImage, width: 800, height: 600 },
    { src: img3, width: 1600, height: 900 },
    { src: videoImage, width: 800, height: 750 },
    { src: img3, width: 1600, height: 900 }
  ]
  // get images slider
  const loadData = () => {
    // get data
    HomeApi.find()
      .then(function (response) {
        if (
          response !== null &&
          response.sliders !== null &&
          response.sliders.length > 0
        ) {
          setSlide(response.sliders)
        }
      })
      .catch(function (error) {})
  }

  useEffect(() => {
    loadData(1)
  }, [])
  //

  const theme = useTheme()
  const isSmall = useMediaQuery(theme.breakpoints.down("sm"))

  return (
    <>
      {/* slider */}
      <Swiper
        className="mySwiper page-home-slider"
        spaceBetween={0}
        centeredSlides={true}
        autoplay={{ delay: 2500, disableOnInteraction: false }}
        pagination={{ clickable: true }}
      >
        {slide.map((item, index) => {
          return (
            <SwiperSlide>
              <img src={item.img} />
            </SwiperSlide>
          )
        })}
      </Swiper>

      {/* news */}
      <div className="page-home-news">
        <Container maxWidth="lg">
          <Grid container>
            <Swiper
              className="mySwiper"
              spaceBetween={0}
              centeredSlides={true}
              autoplay={{ delay: 10000, disableOnInteraction: false }}
              pagination={{ clickable: true }}
            >
              <SwiperSlide>
                <Stack
                  direction={{ xs: "column", md: "row" }}
                  justifyContent="space-between"
                  alignItems="center"
                  spacing={{ xs: 1, md: 4 }}
                  className="news-box"
                >
                  <div className="title-des-box">
                    <h3 className="title">
                      نحوه تشخیص و رفتار با کودکان بیش فعال
                    </h3>
                    <p className="des">
                      خصوص به ویژه مشکلات روحی و روانی باعث ایجاد مشکلات بسیار
                      زیادی در آینده فرزند خود خواهند شد. یکی از بزرگترین
                      مسئولیت هایی که بر عهده انسان است تربیت فرزند و رسیدگی به
                      احتیاجات جسمی و روحی کودک می باشد . والدین در صورت سهل
                      انگاری در این
                    </p>
                  </div>

                  <div className="date">
                    <span>10</span>
                    <span>اسفند</span>
                    <span>1400</span>
                  </div>
                </Stack>
              </SwiperSlide>

              <SwiperSlide>
                <Stack
                  direction={{ xs: "column", md: "row" }}
                  justifyContent="space-between"
                  alignItems="center"
                  spacing={{ xs: 1, md: 4 }}
                  className="news-box"
                >
                  <div className="title-des-box">
                    <h3 className="title">
                      نحوه تشخیص و رفتار با کودکان بیش فعال
                    </h3>
                    <p className="des">
                      خصوص به ویژه مشکلات روحی و روانی باعث ایجاد مشکلات بسیار
                      زیادی در آینده فرزند خود خواهند شد. یکی از بزرگترین
                      مسئولیت هایی که بر عهده انسان است تربیت فرزند و رسیدگی به
                      احتیاجات جسمی و روحی کودک می باشد . والدین در صورت سهل
                      انگاری در این
                    </p>
                  </div>

                  <div className="date">
                    <span>10</span>
                    <span>اسفند</span>
                    <span>1400</span>
                  </div>
                </Stack>
              </SwiperSlide>
            </Swiper>
          </Grid>
        </Container>
      </div>

      {/* rosha-pages */}
      <div className="rosha-pages-bar">
        <Container maxWidth="lg">
          <Grid container spacing={isSmall ? 1 : 4}>
            <Grid item xs={12}>
              <div className="title">تا قله فاصله ای نیست</div>
              <div className="des">
                صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و کارها و
                همچنین استعدادیابی و استعدادپروری کودکان و نوجوانان، با تکیه بر
                دانش روز و تجارب اساتید و کســــــــــب و کار و رویش و شکوفایی
                استعداد، با نگاه بومی و رویکردی جامع در تلاش است خدماتی موثر و
                نقش آفرین در حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر
                با اهداف مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه
                تهران در دو حوزه تخصصی مشـاوره
              </div>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Link to="" className="link-box">
                <img src={serviceLogo2} />
                <div className="title">آزمون</div>
              </Link>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Link to="" className="link-box">
                <img src={serviceLogo2} />
                <div className="title">غربال گری</div>
              </Link>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Link to="" className="link-box">
                <img src={serviceLogo2} />
                <div className="title">استعدادیابی</div>
              </Link>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Link to="" className="link-box">
                <img src={serviceLogo2} />
                <div className="title">مشاوره</div>
              </Link>
            </Grid>
          </Grid>
        </Container>
      </div>

      {/* rosha */}
      <div className="rosha-bar">
        <Container maxWidth="lg">
          <Grid container>
            <Grid item sm={12} md={6} className="video-box">
              <video id="video" poster={videoImage}>
                <source src={video11} type="video/mp4" />
              </video>
              {!isPlay ? (
                <FontAwesomeIcon
                  icon={faPlay}
                  className="icon"
                  onClick={handleVideoPlay}
                />
              ) : null}
            </Grid>

            <Grid item sm={12} md={6} className="title-des-box">
              <div className="title">یک دقیقه با روشا</div>
              <div className="des">
                مناسب برای خود فکر... جای آنکه به انتخاب های رایج در جامعه
                فکــــــــــــــــــر کند، به انتخاب درســـت در زندگی اش کمک می
                کند. در واقع می شـــود گفت او »به شــناســایی درســـت
                اســـتعدادها، به افراد در داشـــتن انتخاب های گیری آینده
                تحصـــــــــــــيلي و شغلي افراد خواهد بود. به همین دلیل مطالعه
                و بهبود اين ويژگيهاي ذاتي راهگشــــــــــاي موفقيت و جهت
                اســـــتعدادها و گونههاي رفتاري متفاوتي اســـــت كه
                شــــــناســــــايي، انسان يكي از پيچيده ترين مخلوقات عالم هستي،
                داراي قابليتها ...
              </div>
              <div className="link">
                <a href="">ادامه مطلب</a>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>

      {/* service */}
      <div className="service-bar">
        <Container maxWidth="lg">
          <Grid container>
            <Grid item xs={12} className="title">
              خدمات توانمند سازی روشا
            </Grid>

            <Grid item xs={12}>
              <Swiper
                className="mySwiper"
                breakpoints={{
                  320: { slidesPerView: 2 },
                  640: { slidesPerView: 3 },
                  768: { slidesPerView: 4 },
                  992: { slidesPerView: 4 },
                  1142: { slidesPerView: 5 }
                }}
                spaceBetween={30}
                centeredSlides={false}
                autoplay={{ delay: 25000, disableOnInteraction: false }}
                navigation={true}
              >
                <SwiperSlide>
                  <img src={serviceLogo1} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo2} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo3} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo6} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo7} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo8} />
                </SwiperSlide>
                <SwiperSlide>
                  <img src={serviceLogo9} />
                </SwiperSlide>
              </Swiper>
            </Grid>
          </Grid>
        </Container>
      </div>

      {/* package */}
      <div className="package-bar">
        <Container maxWidth="lg">
          <Swiper
            className="mySwiper"
            slidesPerView={1}
            spaceBetween={30}
            centeredSlides={false}
            autoplay={{ delay: 25000, disableOnInteraction: false }}
            pagination={{ clickable: true }}
            navigation={true}
          >
            <SwiperSlide>
              <Grid container>
                <Grid item xs={12} sm={6} className="col-des">
                  <div>
                    <div className="title-pre">
                      پکیج استعدادیابی 5 تا 11 سالگی
                    </div>
                    <div className="title">تا قله فاصله ای نیست</div>
                    <div className="des">
                      صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و
                      کارها و همچنین استعدادیابی و استعدادپروری کودکان و
                      نوجوانان، با تکیه بر دانش روز و تجارب اساتید و
                      کســــــــــب و کار و رویش و شکوفایی استعداد، با نگاه بومی
                      و رویکردی جامع در تلاش است خدماتی موثر و نقش آفرین در
                      حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر با اهداف
                      مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران
                      در دو حوزه تخصصی مشـاوره
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={6} className="col-image">
                  <div className="slider-image-holder">
                    <img src={packageImage3} className="image" />
                  </div>
                </Grid>
              </Grid>
            </SwiperSlide>
            <SwiperSlide>
              <Grid container>
                <Grid item xs={12} sm={6} className="col-des">
                  <div>
                    <div className="title-pre">
                      پکیج استعدادیابی 12 تا 15 سالگی
                    </div>
                    <div className="title">تا قله فاصله ای نیست</div>
                    <div className="des">
                      صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و
                      کارها و همچنین استعدادیابی و استعدادپروری کودکان و
                      نوجوانان، با تکیه بر دانش روز و تجارب اساتید و
                      کســــــــــب و کار و رویش و شکوفایی استعداد، با نگاه بومی
                      و رویکردی جامع در تلاش است خدماتی موثر و نقش آفرین در
                      حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر با اهداف
                      مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران
                      در دو حوزه تخصصی مشـاوره
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={6} className="col-image">
                  <div className="slider-image-holder">
                    <img src={pachageImage2} className="image" />
                  </div>
                </Grid>
              </Grid>
            </SwiperSlide>

            <SwiperSlide>
              <Grid container>
                <Grid item xs={12} sm={6} className="col-des">
                  <div>
                    <div className="title-pre">پکیج 16 سال به بالا</div>
                    <div className="title">تا قله فاصله ای نیست</div>
                    <div className="des">
                      صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و
                      کارها و همچنین استعدادیابی و استعدادپروری کودکان و
                      نوجوانان، با تکیه بر دانش روز و تجارب اساتید و
                      کســــــــــب و کار و رویش و شکوفایی استعداد، با نگاه بومی
                      و رویکردی جامع در تلاش است خدماتی موثر و نقش آفرین در
                      حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر با اهداف
                      مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه تهران
                      در دو حوزه تخصصی مشـاوره
                    </div>
                  </div>
                </Grid>

                <Grid item xs={12} sm={6} className="col-image">
                  <div className="slider-image-holder">
                    <img src={packageImage1} className="image" />
                  </div>
                </Grid>
              </Grid>
            </SwiperSlide>
          </Swiper>
        </Container>
      </div>

      {/* gallery-bar */}
      <div className="gallery-bar">
        <Container maxWidth="lg">
          <Grid container spacing={isSmall ? 1 : 4}>
            <Grid item xs={12}>
              <div className="title">گالری تصاویر</div>
              <div className="des">
                صاحب نظران ارائه نماید . ایده ها، مشاوره و توسعه کسـب و کارها و
                همچنین استعدادیابی و استعدادپروری کودکان و نوجوانان، با تکیه بر
                دانش روز و تجارب اساتید و کســــــــــب و کار و رویش و شکوفایی
                استعداد، با نگاه بومی و رویکردی جامع در تلاش است خدماتی موثر و
                نقش آفرین در حوزه های شتابدهی مرکز توسعه کسب و کار روشا متناظر
                با اهداف مرکز نوآوری دانشکده روانشناسی و علوم تربیتی دانشگاه
                تهران در دو حوزه تخصصی مشـاوره
              </div>
            </Grid>

            <Grid item xs={12}>
              <PhotoAlbum layout="columns" photos={photos} />
            </Grid>
          </Grid>
        </Container>
      </div>
    </>
  )
}
export default HomePage
