import React from "react"
import Grid from "@mui/material/Grid"
import "./Rules.css"
import imageroll from "../../assets/images/rolls.png"
import image from "../../assets/images/PikPng 1.png"
import imgicon1 from "../../assets/images/آزمون آیکون سفید (1).png"
import imgicon2 from "../../assets/images/استعداد یابی آیکون سفید.png"
import imgicon3 from "../../assets/images/غربالگری.png"
import imgicon4 from "../../assets/images/مشاوره آیکون سفید.png"
import Header from "../../components/Header/Header"
import Container from "@mui/material/Container"
import { Stack } from "@mui/material"

function Rules() {
  //
  return (
    <div>
      <Header title="قوانین و مقررات" banner={imageroll} />

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="title-rolls">
              <h2>قوانین و مقررات</h2>
              <ul className="lists-roll">
                <li className="list-roll">
                  اطلاعات مورد نیاز را به دقت وارد کنید.
                </li>
                <li className="list-roll">
                  در صورت نیاز به مراجعه به متخصص با بخش پشتیبانی مرکز روشا تماس
                  بگیرید.
                </li>
                <li className="list-roll">
                  مرکز روشا از هر ایده و پیشنهادی در راستای بهبود فرایندها
                  استقبال می کند.
                </li>
                <li className="list-roll">
                  بعد از اتمام فرآیند عضویت، کلیه اطلاعات شخصی و یا خدماتی که به
                  شما ارائه شده است با رعایت اصول امانتداری ذخیره خواهد شد.
                </li>
                <li className="list-roll">
                  کلیه پرداخت ها فقط از طریق درگاه اینترنتی سامانه بوده و مرکز
                  روشا به هیچ پرداخت دیگری ترتیب اثر نخواهد داد.
                </li>
                <li className="list-roll">
                  هنگام ثبتنام برای پکیجهای استعدادیابی و دوره ها به بازه سنی
                  مورد نظر دقت فرمایید.
                </li>
              </ul>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className="section-background">
        <Container maxWidth="lg">
          <Grid container>
            <Grid item xs={12} sm={8}>
              <div className="title-rolls seconde">
                <h3 className="title">قوانین و مقررات بخش توانمندسازی</h3>
                <ul className="lists-roll lists-roll2">
                  <li className="list-roll">
                    والدین باید در صورت لزوم نهایت همکاری با مربیان و مشاورین
                    مرکز داشته باشند.
                  </li>
                  <li className="list-roll">
                    پس از ثبت‌نام تحت هیچ شرایطی شهریه پرداختی (حتی قبل از شروع
                    کلاسها) مسترد و یا رزرو ترم بعدی نگه داشته نخواهد شد.
                  </li>
                  <li className="list-roll">
                    کلیه پرداخت ها فقط از طریق درگاه اینترنتی سامانه بوده و مرکز
                    روشا به هیچ پرداخت دیگری ترتیب اثر نخواهد داد.
                  </li>
                  <li className="list-roll">
                    مسئولیت غیبت در کلاس ها و یا جلسه مشاوره به عهده نواندیشان و
                    والدین بوده و مرکز موظف به برگزاری جلسه جبرانی و یا توضیح
                    مجدد مطالب عنوان شده نمی باشد.
                  </li>
                  <li className="list-roll">
                    در صورتی که فرزند شما به دلیل موجهی امکان حضور در دوره و یا
                    مشاوره را نداشته باشد، می تواند با تایید مدیریت و موافقت
                    ایشان، در دوره های بعدی حضور پیدا کند.
                  </li>
                </ul>
              </div>
            </Grid>

            <Grid item xs={12} sm={4} className="img-rolls">
              <img src={image} />
            </Grid>
          </Grid>
        </Container>
      </div>

      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} className="panel-page-content-grid">
            <div className="title-rolls title-rolls3">
              <h3 className="title">
                قوانین و مقررات استعدادیابی (حضوری و غیرحضوری)
              </h3>
              <ul className="lists-roll">
                <li className="list-roll">
                  مرکز روشا از هر ایده و پیشنهادی در راستای بهبود فرایندها
                  استقبال می کند.
                </li>
                <li className="list-roll">
                  هر نواندیش و خانواده موظف است لوازم مورد نیاز خود را که از قبل
                  به اطلاع میرسد ، هر جلسه همراه داشته باشد.
                </li>
                <li className="list-roll">
                  لطفا پس از ثبت نام نهایی و ارسال دعوتنامه ، تاریخ و ساعت دقیق
                  برگزاری جلسه مشاوره خود را بخاطر بسپارید.
                </li>
                <li className="list-roll">
                  بعد از اتمام فرآیند عضویت، کلیه اطلاعات شخصی و یا خدماتی که به
                  شما ارائه شده است با رعایت اصول امانتداری ذخیره خواهد شد.
                </li>
                <li className="list-roll">
                  لازم به ذکر است که حتما 15 دقیقه قبل از شروع جلسه مشاوره در
                  مرکز حضور داشته باشید
                </li>
                <li className="list-roll">
                  نواندیشان و والدین از آوردن ميهمان به مرکز جداً خودداري
                  نمايند.
                </li>
                <li className="list-roll">
                  اطلاعات تماس خود را برای اطلاع رسانی های بعدی، به درستی در
                  سامانه قرار دهید.
                </li>
                <li className="list-roll">
                  لازم است افراد مورد آزمون حتما قبل از کلاس ها و یا مشاوره
                  استراحت کافی داشته و صبحانه یا عصرانه میل کرده باشند.
                </li>
                <li className="list-roll">
                  اطلاعات را هنگام ثبت در سایت، به درستی ثبت و ذخیره نمایید. این
                  امر برای صدور گواهی نهایی، الزامیست.
                </li>
                <li className="list-roll">
                  کلیه پرداخت ها فقط از طریق درگاه اینترنتی سامانه بوده و مرکز
                  روشا به هیچ پرداخت دیگری ترتیب اثر نخواهد داد.
                </li>
                <li className="list-roll">
                  مسئولیت غیبت در کلاس ها و یا جلسه مشاوره به عهده نواندیشان و
                  والدین بوده و مرکز موظف به برگزاری جلسه جبرانی و یا توضیح مجدد
                  مطالب عنوان شده نمی باشد.
                </li>
              </ul>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className="title-rolls4 section-background">
        <Container maxWidth="lg">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <div className="title-bar">قوانین و مقررات</div>
            </Grid>

            <Grid item xs={12} sm={6}>
              {/* <Grid container className="last-roll-holder" spacing={2}>
                <Grid item xs={10} sm={10} className="roll roll-left">
                  تمام کسانی که در کشور در حوزه مشاوره ، ارزیابی و یا
                  توانمندسازی کودک و نوجوان فعالیت مینمایند می توانند در طرح
                  روشا بعنوان همکار در سامانه تماس با ما پیام ارسال کنند، تا
                  فرایند بررسی انجام گردد.
                </Grid>
                <Grid item xs={2} sm={2}>
                  <div className="icon-holder">
                    <img src={imgicon1} />
                  </div>
                </Grid>
              </Grid> */}
              <Stack
                direction={{ xs: "column-reverse", md: "row" }}
                alignItems="center"
                justifyContent="space-between"
                spacing={2}
                sx={{
                  minHeight: "150px"
                }}
              >
                <div className="roll ml-16">
                  تمام کسانی که در کشور در حوزه مشاوره ، ارزیابی و یا
                  توانمندسازی کودک و نوجوان فعالیت مینمایند می توانند در طرح
                  روشا بعنوان همکار در سامانه تماس با ما پیام ارسال کنند، تا
                  فرایند بررسی انجام گردد.
                </div>
                <div className="icon-holder">
                  <img src={imgicon1} />
                </div>
              </Stack>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Stack
                direction={{ xs: "column-reverse", md: "row-reverse" }}
                alignItems="center"
                justifyContent="space-between"
                spacing={2}
                sx={{
                  minHeight: "150px"
                }}
              >
                <div className="roll ">
                  کلیه موسسات فرهنگی دیجیتال که دارای محصولات دیجیتال حوزه کودک
                  و نوجوان هستند می توانند یک نسخه از اثر خود را به دبیرخانه
                  ارسال نمایند تا پس از بررسی در فروشگاه دیجیتال روشا قرار گیرد
                  و یا به کاربران معرفی گردد.
                </div>
                <div className="icon-holder ml-16">
                  <img src={imgicon2} />
                </div>
              </Stack>
              {/* <Grid container className="last-roll-holder" spacing={2}>
                <Grid item xs={2} sm={2}>
                  <div className="icon-holder">
                    <img src={imgicon2} />
                  </div>
                </Grid>
                <Grid item xs={10} sm={10} className="roll">
                  کلیه موسسات فرهنگی دیجیتال که دارای محصولات دیجیتال حوزه کودک
                  و نوجوان هستند می توانند یک نسخه از اثر خود را به دبیرخانه
                  ارسال نمایند تا پس از بررسی در فروشگاه دیجیتال روشا قرار گیرد
                  و یا به کاربران معرفی گردد.
                </Grid>
              </Grid> */}
            </Grid>

            <Grid item xs={12} sm={6}>
              <Stack
                direction={{ xs: "column-reverse", md: "row" }}
                alignItems="center"
                justifyContent="space-between"
                spacing={2}
              >
                <div className="roll ml-16">
                  کلیه انتشارات و شرکت هایی که دارای کتاب و یا منابع آموزشی و
                  علمی حوزه کودکان و نوجوانان هستند می توانند یک نسخه از اثر خود
                  را به دبیرخانه روشا ارسال نمایند تا پس از بررسی در فروشگاه
                  دیجیتال روشا قرار گیرد.
                </div>
                <div className="icon-holder">
                  <img src={imgicon3} />
                </div>
              </Stack>
              {/* <Grid container className="last-roll-holder" spacing={2}>
                <Grid item xs={10} sm={10} className="roll roll-left">
                  کلیه انتشارات و شرکت هایی که دارای کتاب و یا منابع آموزشی و
                  علمی حوزه کودکان و نوجوانان هستند می توانند یک نسخه از اثر خود
                  را به دبیرخانه روشا ارسال نمایند تا پس از بررسی در فروشگاه
                  دیجیتال روشا قرار گیرد.
                </Grid>
                <Grid item xs={2} sm={2}>
                  <div className="icon-holder">
                    <img src={imgicon3} />
                  </div>
                </Grid>
              </Grid> */}
            </Grid>

            <Grid item xs={12} sm={6}>
              <Stack
                direction={{ xs: "column-reverse", md: "row-reverse" }}
                alignItems="center"
                justifyContent="space-between"
                spacing={2}
              >
                <div className="roll ">
                  در صورت بروز هرگونه مسئله اخلاقی از سوی مراجعه کنندگان در طول
                  هریک از خدمات روشا (مشاوره، مشاوره کوچینگ و غیره)، ضمن پیگیری
                  های قضایی به دلیل ضبط مکالمات، کلیه خدمات و سرویس ها از سمت
                  مجموعه روشا قطع شده و هیچ مبلغی مسترد نخواهد شد.
                </div>
                <div className="icon-holder ml-16">
                  <img src={imgicon4} />
                </div>
              </Stack>
              {/* <Grid container className="last-roll-holder" spacing={2}>
                <Grid item xs={2} sm={2}>
                  <div className="icon-holder">
                    <img src={imgicon4} />
                  </div>
                </Grid>
                <Grid item xs={10} sm={10} className="roll">
                  در صورت بروز هرگونه مسئله اخلاقی از سوی مراجعه کنندگان در طول
                  هریک از خدمات روشا (مشاوره، مشاوره کوچینگ و غیره)، ضمن پیگیری
                  های قضایی به دلیل ضبط مکالمات، کلیه خدمات و سرویس ها از سمت
                  مجموعه روشا قطع شده و هیچ مبلغی مسترد نخواهد شد.
                </Grid>
              </Grid> */}
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  )
}
export default Rules
