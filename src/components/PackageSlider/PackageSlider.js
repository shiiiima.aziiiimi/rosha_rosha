import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

import imgBG from "../../assets/images/atyuv.jpg";

import "./PackageSlider.css";
import Zamine from "../../assets/images/زمینه کارکتر.png";
import Char from "../../assets/images/کارکتر.png";

// import Swiper core and required modules
import SwiperCore, { Parallax, Pagination, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Parallax, Pagination, Navigation]);

export default function PackageSlider() {
  return (
    <div className="last-slider">
      <Swiper
        speed={600}
        parallax={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        className="mySwiper last-slider"
      >
        <div
          slot="container-start"
          className="parallax-bg"
          style={{ "background-image": "url(" + imgBG + ")" }}
          data-swiper-parallax="-23%"
        ></div>
        <SwiperSlide>
          <div className="pakage-inner">
            <div className="details-inner">
              <div className="images-holder">
                <div className="img1">
                  <img src={Char} />
                </div>
              </div>
              <div className="content-holder">
                {/* <span>تست </span> */}
                <h2>تا قله فاصله ای نیست!</h2>
                <p>
                  خیلی از ما توی مسیری از زندگی افتادیم که دائم در تلاشیم. بدون
                  اینکه کاملا از خود واقعیمون آگاه باشیم و بدونیم عمیقا دنبال چی
                  هستیم؟! شاید تا موفقیتی که همیشه دنبالشیم راهی نمونده باشه اما
                  نمیدونیم چجوری بهش برسیم. اینجا جاییه که می¬تونیم بهتون کمک
                  کنیم تا به ظرفیتهای پنهان خودتون پی ببرید و خودتونو کامل
                  بشناسید. پکیجهای استعدادیابی روشا شامل 10 آزمون و خرده آزمون و
                  با رویکرد ارزیابی چندوجهی است که نیمرخی از ویژگی¬های فردی، هوش
                  و استعدادتون به شما ارائه میده.{" "}
                </p>
              </div>
            </div>
          </div>
        </SwiperSlide>
        {/* <SwiperSlide>
          <div className="title" data-swiper-parallax="-300">Slide 2</div>
          <div className="subtitle" data-swiper-parallax="-200">Subtitle</div>
          <div className="text" data-swiper-parallax="-100">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus
              felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet
              magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
              ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros
              quis feugiat.</p>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="title" data-swiper-parallax="-300">Slide 3</div>
          <div className="subtitle" data-swiper-parallax="-200">Subtitle</div>
          <div className="text" data-swiper-parallax="-100">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus
              felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet
              magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
              ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros
              quis feugiat.</p>
          </div>
        </SwiperSlide> */}
      </Swiper>
    </div>
  );
}

// import React, { useRef, useState } from "react";
// // Import Swiper React components
// import { Swiper, SwiperSlide } from "swiper/react";

// // Import Swiper styles
// import "swiper/css";
// import "swiper/css/pagination"
// import "swiper/css/navigation"

// import "./styles.css";

// // import Swiper core and required modules
// import SwiperCore, {
//   Parallax,Pagination,Navigation
// } from 'swiper';

// // install Swiper modules
// SwiperCore.use([Parallax,Pagination,Navigation]);

// export default function App() {

//   return (
//     <>

//   <Swiper style={{'--swiper-navigation-color': '#fff','--swiper-pagination-color': '#fff'}} speed={600} parallax={true} pagination={{
//   "clickable": true
// }} navigation={true} className="mySwiper">
//   <div slot="container-start" className="parallax-bg" style={{'background-image': 'url(https://swiperjs.com/demos/images/nature-1.jpg)'}} data-swiper-parallax="-23%"></div>
//   <SwiperSlide>
//           <div className="title" data-swiper-parallax="-300">Slide 1</div>
//           <div className="subtitle" data-swiper-parallax="-200">Subtitle</div>
//           <div className="text" data-swiper-parallax="-100">
//             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus
//               felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet
//               magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
//               ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros
//               quis feugiat.</p>
//           </div>
//         </SwiperSlide><SwiperSlide>
//           <div className="title" data-swiper-parallax="-300">Slide 2</div>
//           <div className="subtitle" data-swiper-parallax="-200">Subtitle</div>
//           <div className="text" data-swiper-parallax="-100">
//             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus
//               felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet
//               magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
//               ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros
//               quis feugiat.</p>
//           </div>
//         </SwiperSlide><SwiperSlide>
//           <div className="title" data-swiper-parallax="-300">Slide 3</div>
//           <div className="subtitle" data-swiper-parallax="-200">Subtitle</div>
//           <div className="text" data-swiper-parallax="-100">
//             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus
//               felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet
//               magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
//               ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros
//               quis feugiat.</p>
//           </div>
//         </SwiperSlide>
//   </Swiper>
//     </>
//   )
// }
