

import React, { useState } from "react";
import { useLocation } from "react-router-dom"
import Grid from "@mui/material/Grid";
import Loading from "../Loading/Loading";
import axios from "axios";
import General from "../../utils/General";
import { TableBody } from "@mui/material";
// import "./FormContact.css";

//
function FormAcceleration1(props) {


  const [formLoading, setFormLoading] = useState(false);

  const [first_name, setFormfirst_name] = useState("");
  const [last_name, setFormlast_name] = useState("");
  const [email, setFormEmail] = useState("");
  const [phone, setFormPhone] = useState("");
  const [field, setFormField] = useState("");
  const [title, setFormTitle] = useState("");
  const [member_count, setFormMemberCount] = useState("");
  const [introduction, setFormIntroduction] = useState("");
  const [idea_detail, setFormIdeaDetail] = useState("");
  const [expectation, setExpectation] = useState("");
  const [formError, setFormError] = useState("");
  const onFormfirst_nameInputChange = (event) =>
    setFormfirst_name(event.target.value);
  const onFormlast_nameInputChange = (event) =>
    setFormlast_name(event.target.value);
  const onFormEmailInputChange = (event) => setFormEmail(event.target.value);
  const onFormPhoneInputChange = (event) => setFormPhone(event.target.value);
  const onFormFieldInputChange = (event) => setFormField(event.target.value);
  const onFormTitleInputChange = (event) => setFormTitle(event.target.value);
  const onFormMemberCountInputChange = (event) =>
    setFormMemberCount(event.target.value);
  const onFormIntroductionInputChange = (event) =>
    setFormIntroduction(event.target.value);
  const onFormIdeaTitleInputChange = (event) =>
    setFormIdeaDetail(event.target.value);
  const onFormExpectationInputChange = (event) =>
    setExpectation(event.target.value);
  const onFormError = (event) => setFormError(event.target.value);
  const body = {
    first_name: first_name,
    last_name:last_name,
    email: email,
    phone: phone,
    field:field,
    title: title,
    member_count: member_count,
    introduction: introduction,
    idea_detail: idea_detail,
    expectation:expectation
  }
 
  // comment
  const handleForm = (event) => {
    axios({
      method: "post",
      url: General.siteUrl+"/acceleration-submit/prepevent",
      data: body,
      headers: {
        ...General.authorizationToken,
        "content-type": "application/json",
      },


    }).then((res) => {
      console.log(res)
      console.log("data :" ,body)
    }).catch((err) => {
      console.log("err :" ,err)
    })
  };

  //
  return (
    <div className="form-contact">
      <div className="title-bar form-title-bar">رویداد مقدماتی </div>
      <div className="form-des">شما فقط یک ایده دارید و هیچ فعالیت کلیدی خاصی برای ورود به بازار انجام نداده اید. ما در کنار شما هستیم تا ایده خود را تجاری سازی کنید.</div>

      <div className="form-contact-box">
        <form onSubmit={handleForm} autoComplete="off">
          <Grid container spacing={2}>
            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormfirst_nameInputChange}
                value={first_name}
                type="text"
                placeholder="نام"
                autoComplete="false"
              />
            </Grid>

            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormlast_nameInputChange}
                value={last_name}
                type="text"
                placeholder="نام خانوادگی"
                autoComplete="false"
              />
            </Grid>

            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormEmailInputChange}
                value={email}
                type="email"
                placeholder="ایمیل"
                autoComplete="false"
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormPhoneInputChange}
                value={phone}
                type="text"
                placeholder="شماره تماس"
                autoComplete="false"
              />
            </Grid>

            <Grid item xs={12}>
              <textarea
                onChange={onFormFieldInputChange}
                placeholder="حوزه فعالیت( طرح های نوآورانه در حوزه روانشناسی،علوم شناختی،  کودک و نوجوان ، آموزش و علوم تربیتی )"
              >
                {field}
              </textarea>
            </Grid>
            <Grid item xs={12}>
              <input
                onChange={onFormTitleInputChange}
                value={title}
                type="text"
                placeholder=" عنوان استارتاپ"
                autoComplete="false"
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormMemberCountInputChange}
                value={member_count}
                type="text"
                placeholder="تعداد اعضای تیم"
                autoComplete="false"
              />
            </Grid>

            <Grid item sm={6} xs={12}>
              <input
                onChange={onFormIntroductionInputChange}
                value={introduction}
                type="text"
                placeholder=" نحوه آشنایی با ما"
                autoComplete="false"
              />
            </Grid>
            <Grid item xs={12}>
              <textarea
                onChange={onFormIdeaTitleInputChange}
                placeholder="لطفا در چند سطر کلیات طرح و ایده خود را توضیح دهید(نیاز چیست؟ شما چطور این نیاز را برطرف میکنید؟ مزیت های رقابتی شما چیست؟)"
              >
                {idea_detail}
              </textarea>
            </Grid>
            <Grid item xs={12}>
              <textarea
                onChange={onFormExpectationInputChange}
                placeholder="انتظار شما از حضور در شتابدهنده چیست؟"
              >
                {expectation}
              </textarea>
            </Grid>
            <Grid item xs={12}>
              {formLoading ? (
                <Loading />
              ) : (
                <button
                  onClick={handleForm}
                  type="button"
                  className="btn btn-primary"
                >
                  ارسال
                </button>
              )}
            </Grid>
          </Grid>
        </form>
      </div>
      {formError && (
        <div className="form-contact-alert">
          {/* {ERROR_MESSAGES[commentError] ?? commentError} */}
        </div>
      )}
    </div>
  );
}
export default FormAcceleration1;
