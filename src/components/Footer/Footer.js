import React from "react"
import Grid from "@mui/material/Grid"
import Container from "@mui/material/Container"
import "./footer.css"

import instgram from "../../assets/images/instagram_logo_icon_173070.png"
import telegram from "../../assets/images/1946547.png"
import video from "../../assets/images/img_489998.png"
import enamad from "../../assets/images/enamad.png"
import rasane from "../../assets/images/rasane.png"
import TelegramIcon from "@mui/icons-material/Telegram"
import InstagramIcon from "@mui/icons-material/Instagram"
import PlayCircleIcon from "@mui/icons-material/PlayCircle"

//
function Footer() {
  //
  return (
    <footer>
      <Grid className="footer-holder">
        <Container maxWidth="lg">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={8} className="about-des">
              <h3 className="title">درباره مرکز توسعه کسب و کار روشا</h3>
              <p className="des">
                مرکز رویش و شکوفایی استعداد روشا متناظر با اهداف مرکز نوآوری
                دانشکده روانشناسی و علوم تربیتی دانشگاه تهران در حوزه های تخصصی
                مشاوره، ارزیابی، استعدادیابی، استعدادپروری و شتابدهی و حمایت از
                استعدادهای کودک و نوجوان، با نگاه بومی و رویکردی جامع در تلاش
                است خدماتی موثر و نقش آفرین به خانواده ها، با تکیه بر دانش روز و
                تجارب اساتید و صاحب نظران ارائه نماید.
              </p>
            </Grid>

            <Grid item xs={12} sm={12} md={4} className="logo-social">
              <Grid container>
                <Grid item xs={12} className="logo">
                  <a className="link" href="">
                    <img src={enamad} />
                  </a>
                  <a className="link" href="">
                    <img src={rasane} />
                  </a>
                </Grid>

                <Grid item xs={12} className="social">
                  <a className="link" href="">
                    <PlayCircleIcon />
                  </a>
                  <a className="link" href="">
                    <InstagramIcon />
                  </a>
                  <a className="link" href="">
                    <TelegramIcon />
                  </a>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </Grid>

      <Grid className="copy-right">
        <Container maxWidth="lg">
          <Grid container>
            <Grid item xs={12} className="des">
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است
            </Grid>
          </Grid>
        </Container>
      </Grid>
    </footer>
  )
}
export default Footer
