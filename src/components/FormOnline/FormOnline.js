import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";

import "./FormOnline.css";
import axios from "axios";
import { Alert } from "@mui/material";
import General from "../../utils/General";
import ReserveApi from "../../services/ReserveApi";
import Select from "react-select";
import Loading from "../Loading/Loading";
import { Link } from "react-router-dom";
import Payment from "../../pages/Payment/Payment";

//
function FormOnline(props) {
  //
  const options = [
    {
      label: " مشاوره حضوری - مبلغ سرمایه‌گذاری: 215 هزار تومان",
      value: "talent_scout_presence",
    },
    {
      label: " مشاوره غیر حضوری - مبلغ سرمایه‌گذاری: 227 هزار تومان",
      value: "talent_scout_none_presence",
    },
    {
      label:
        " آزمونهای رایانه ای توجه و تمرکز IVA توسط متخصص (همراه با تفسیر) - مبلغ سرمایه‌گذاری: 347 هزار تومان",
      value: "iva",
    },
    {
      label:
        " آزمونهای رایانه ای توجه و تمرکز ریهاکام توسط متخصص (همراه با تفسیر)-مبلغ سرمایه‌گذاری: 347 هزار تومان",
      value: "ریهاکام",
    },
    {
      label:
        " آزمون حضوری ارزیابی هوش بینه توسط متخصص (همراه با تفسیر)- مبلغ سرمایه‌گذاری: 487 هزار تومان",
      value: "بیمه",
    },
    {
      label:
        "   آزمون حضوری ارزیابی هوش وکسلر توسط متخصص (همراه با تفسیر)- مبلغ سرمایه‌گذاری:487 هزار تومان",
      value: "وکسلر",
    },
    {
      label:
        "  بسته استعدادیابی ۵ تا ۱۱ سال توسط اساتید مجرب روان شناسی - مبلغ سرمایه‌گذاری:847 هزار تومان",
      value: "استعدادیابی 5 تا 11 سال",
    },
    {
      label:
        " بسته استعدادیابی ۱۲ تا ۱۵ سال توسط اساتید مجرب روان شناسی - مبلغ سرمایه‌گذاری: 974 هزار تومان",
      value: "استعدادیابی 12 تا 15 سال",
    },
    {
      label:
        " بسته استعدادیابی ۱۶ سال به بالا توسط اساتید مجرب روان شناسی- مبلغ سرمایه‌گذاری:1 میلیون و 147 هزار تومان",
      value: "استعدادیابی 16 سال به بالا",
    },
  ];

  const title = props.title;
  const des = props.des;
  const [formLoading, setFormLoading] = useState(false);
  const [service, setService] = useState("");
  const [age, setAge] = useState("");
  const [agencies, setAgencies] = useState("");
  const [dataagency, setDataAgency] = useState([]);
  const [comment, setComment] = useState("");
const[paymentId,setPaymentId]=useState(0)
  const [commentError, setCommentError] = useState("");
  const onServiceInputChange = (event) => {



    setService(event.value);
  }
  const onAgeInputChange = (event) => setAge(event.target.value);
  const onAgencieslInputChange = (event) => {
    // console.log("::--"+JSON.stringify(event));
    
    setAgencies(event.value);
  }
  const onCommentInputChange = (event) => setComment(event.target.value);
  const getData = () => {
    // get data
    ReserveApi.find()
      .then(function (response) {
        setDataAgency(response.agencies);
        // if (
        //   response !== null &&
        //   response.items !== null &&
        //   response.items.length > 0
        // ) {
        //   setMinutes(response.minutes);
        //   setTeachers(response.council_count);
        //   setVebinars(response.items_count);
        //   setData([...dataTmp, ...response.items]);
        //   if (response.items.next_page_url != null) setIsNextPage(true);
        // }

        // setDataLoading(false); // hide loading
       
      })
      .catch(function (error) {
        // setDataLoading(false); // hide loading
        console.log(error);
        setCommentError(error);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  // comment
  const handleComment = (event) => {
    ReserveApi.sendId({  type: service,
      age_range: age,
      agency_id: 1,
      info: comment,}).then
    (
        function (response)
      {
      console.log(response.payment_id)
        setPaymentId(response.payment_id) 
  
        }
    ).catch(function (error)
    {
      console.log("err", error)
      
    });
 
  };
  
  let optionAgencies = dataagency.map((item, index) => {
    return {
      label: item.title + `(${item.city + " " + item.state})`,
      value: item.id,
    };
  });
  
 
  return (
    <div className="form-contact">
      <div className="title-bar form-title-bar">{title}</div>
      <div className="form-des">{des}</div>

      <div className="form-contact-box">
        <form onSubmit={handleComment} autoComplete="off">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Select
                onChange={onServiceInputChange}
                value={options.id}
                className="select-input-form-box"
                options={options}
                placeholder="لطفا خدمت مورد نظر را انتخاب کنید "
              />
            </Grid>

            <Grid item xs={12}>
              <input
                onChange={onAgeInputChange}
                value={age}
                type="text"
                placeholder="لطفا سن نواندیش را وارد کنید "
                autoComplete="false"
              />
            </Grid>

            <Grid item xs={12}>
              <Select
                 onChange={onAgencieslInputChange}
                id={dataagency.id}
                className="select-input-form-box"
                value={optionAgencies.id}
                options={optionAgencies}
                placeholder="نمایندگی ها"
              ></Select>
            </Grid>

            <Grid item xs={12}>
              <textarea
                onChange={onCommentInputChange}
                placeholder="اگر یادداشتی دارید، بنویسید"
              >
                {comment}
              </textarea>
            </Grid>

            <Grid item xs={12}>
              {formLoading ? (
                <Loading />
              ) : (
                  <Link
                    // to={"Payment/:id"}
                
                  to={{ pathname: "payment/" + paymentId , state:  paymentId }}
                  >
                               <button
                  onClick={handleComment}
                  type="button"
                  className="btn btn-primary"
                >
            ورود به صفحه پرداخت
                </button>
                  </Link>
              )}
            </Grid>
          </Grid>
        </form>
      </div>
      {commentError && (
        <div className="form-contact-alert">
        ارسال نشد
        </div>
      )}
    </div>
  );
}
export default FormOnline;
