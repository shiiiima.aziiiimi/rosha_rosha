import { Box } from '@mui/system'
import React from 'react'
import { Link } from 'react-router-dom'

function Comment() {
    return (
        <>
            <Box className="accel-item-title accel-item-title-center" sx={{ mb: 2, px: 3 }}>
                لورم ایپسوم متن ساختگی
            </Box>
            <Box component="form">
                <Box sx={{ mb: 2 }}>
                    <input type="text" placeholder="نام" className="ro-input" />
                </Box>
                <Box sx={{ mb: 2 }}>
                    <input type="email" placeholder="ایمیل" className="ro-input" />
                </Box>
                <Box>
                    <textarea placeholder="پیام" className="ro-input" row='4'></textarea>
                </Box>
                <Box sx={{ textAlign: 'left', mt: 3 }}>
                    <Link to="" className="btn-signup" sx={{ px: 2 }}>ارسال</Link>
                </Box>
            </Box>
        </>
    )
}

export default Comment