import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import "./Navbar.css";
import { menuItems, panelMenuItems } from "../../utils/Data";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import IconButton from "@mui/material/IconButton";
import Drawer from "@mui/material/Drawer";

import Logo from "../../assets/images/4.png";
import {useAuth} from "../../Contex/AuthProvider";
import ServiceApi from "../../services/ServiceApi";

//
function Navbar()
{
    const { initAuth, user } = useAuth();
    const [drawerOpen, setDrawerOpen] = useState(false);
    const [menuServiceItems, setMenuServiceItems] = useState([]);

  const toggleDrawer = () => () => {
    setDrawerOpen(!drawerOpen);
  };

    // load data
    const loadData = () =>
    {
        // get data
        ServiceApi.findAll().then
        (
            function(response)
            {
                if (response !== null && response.services !== null && response.services.length > 0)
                {
                  setMenuServiceItems(response.services);
            
                }
            }
            ).catch
        (
            function (error)
            {
            }
            );
    };


    useEffect(() => {loadData(1);}, []);

    //
    return(
        <Container maxWidth="lg">
            <Grid container>
                <Grid item xs={12} className="nav-bar">
                    <div className="logo-nav">
                        <Link to="/" className="logo"><img src={Logo}/></Link>

            <IconButton className="btn-drawer" onClick={toggleDrawer(true)}>
              <FontAwesomeIcon icon={faEllipsisVertical} className="icon" />
            </IconButton>

                        <ul className="nav">
                            {
                                menuItems.map
                                (
                                    menuItem =>
                                    {
                                        return (
                                            <li>
                                                <Link to={menuItem.path} className="nav-link"><FontAwesomeIcon icon={menuItem.icon} className="icon" />{menuItem.title}{(menuItem.subMenuItems !== undefined && menuItem.subMenuItems.length > 0) ? <FontAwesomeIcon icon={faCaretDown} className="icon-dropdown" /> : null}</Link>
                                                {
                                                    menuItem.id === 2 ?
                                                        (menuServiceItems !== undefined && menuServiceItems.length > 0) ?
                                                            <ul>
                                                                {
                                                                    menuServiceItems.map
                                                                    (
                                                                        subMenuItem =>
                                                                        {
                                                                            return <li><Link to={"/service/" + subMenuItem.id} className="nav-link">{subMenuItem.title}</Link></li>
                                                                        }
                                                                    )
                                                                }
                                                            </ul>
                                                            :
                                                            null
                                                        :
                                                    (menuItem.subMenuItems !== undefined && menuItem.subMenuItems.length > 0) ?
                                                        <ul>
                                                            {
                                                                menuItem.subMenuItems.map
                                                                (
                                                                    subMenuItem =>
                                                                    {
                                                                        return <li><Link to={subMenuItem.path} className="nav-link">{subMenuItem.title}</Link></li>
                                                                    }
                                                                )
                                                            }
                                                        </ul>
                                                        :
                                                        null
                                                }
                                            </li>
                                        );
                                    }
                                )
                            }

              {/*{*/}
              {/*    navitems.map*/}
              {/*    (*/}
              {/*        item =>*/}
              {/*        {*/}
              {/*            if(item.title === "سرویس ها")*/}
              {/*            {*/}
              {/*                return (*/}
              {/*                    <li key={item.id} className="nav-item"*/}
              {/*                        onMouseEnter={() => setDropdown(true)}*/}
              {/*                        onMouseLeave={() => setDropdown(false)}>*/}
              {/*                        <i className="fa fa-angle-down"></i>*/}
              {/*                        <Link to="item.path" className="nav-link">{item.title}</Link>*/}
              {/*                        {dropdown && <DropDown />}*/}
              {/*                    </li>*/}
              {/*                )*/}
              {/*            }*/}
              {/*            else*/}
              {/*            {*/}
              {/*                return (*/}
              {/*                    <li key={item.id} className="nav-item">*/}
              {/*                        <i className={item.icon}></i>*/}
              {/*                        <Link to="item.path" className="nav-link">{item.title}</Link>*/}
              {/*                    </li>*/}
              {/*                )*/}
              {/*            }*/}
              {/*        }*/}
              {/*    )*/}
              {/*}*/}
            </ul>
          </div>

          {user.loggedIn === true ? (
            <Link className="btn-logout" to="/logout">
              خروج
            </Link>
          ) : (
            <div className="btn-signup">
              <Link to="/login">ورود</Link> |{" "}
              <Link to="/register">ثبت نام</Link>
            </div>
          )}
        </Grid>
      </Grid>

      <Drawer
        className="nav-drawer"
        anchor="right"
        open={drawerOpen}
        onClose={toggleDrawer(false)}
      >
        <ul className="nav">
          {menuItems.map((menuItem) => {
            return (
              <li>
                <Link to={menuItem.path} className="nav-link">
                  <FontAwesomeIcon icon={menuItem.icon} className="icon" />
                  {menuItem.title}
                </Link>
                {menuItem.subMenuItems !== undefined &&
                menuItem.subMenuItems.length > 0 ? (
                  <ul>
                    {menuItem.subMenuItems.map((subMenuItem) => {
                      return (
                        <li>
                          <Link to={subMenuItem.path} className="nav-link">
                            {subMenuItem.title}
                          </Link>
                        </li>
                      );
                    })}
                  </ul>
                ) : null}
              </li>
            );
          })}
          {user.loggedIn === true
            ? panelMenuItems.map((menuItem, i) => {
                return (
                  <li>
                    <Link to={menuItem.path} className="nav-link">
                      <FontAwesomeIcon icon={menuItem.icon} className="icon" />
                      {menuItem.title}
                    </Link>
                  </li>
                );
              })
            : null}
        </ul>
      </Drawer>
    </Container>
  );
}
export default Navbar;
