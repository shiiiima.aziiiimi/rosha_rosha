import React from "react";
import {Link} from "react-router-dom";


import {panelMenuItems} from "../../utils/Data";
import userImage from "../../assets/images/closeup-senior-lecturer-with-arms-crossed 1.png";
import menuIcon from "../../assets/images/1946547.png";


import "./PanelMenu.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useAuth} from "../../Contex/AuthProvider";

//
function PanelMenu()
{
    const { initAuth, user } = useAuth();

    //
    return(
        <aside className="panel-side-bar">
            <div className="user-bar">
                {user.image !== "" && user.image !== null ? <img className="user-image" src={user.image} /> : <img className="user-image" src={userImage} />}
                <div className="user-name">{user.nameFamily !== "" && user.nameFamily !== null ? user.nameFamily : "-"}</div>
                <div className="user-des">{user.phone !== "" && user.phone !== null ? user.phone : "-"}</div>
            </div>

            <ul className="link-bar">
                {
                    panelMenuItems.map
                    (
                        (menuItem, i) =>
                        {
                            return (
                                <li className={menuItem.full == true ? "link-box full" : "link-box"} key={i}>
                                    <Link to={menuItem.path} className="link">
                                        <img className="icon" src={menuItem.icon} />
                                        <span className="title">{menuItem.title}</span>
                                    </Link>
                                </li>
                            );
                        }
                    )
                }
            </ul>
        </aside>
    );
}
export default PanelMenu;
