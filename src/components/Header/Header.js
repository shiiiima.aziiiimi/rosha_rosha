import React from "react";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";

import "./Header.css";

//
function Header(prop)
{
    const title = prop.title;
    const subTitle = prop.subTitle;
    const banner = prop.banner;
    const image = prop.image;

    //
    return(
        <header className={(image !== undefined) ? "header-bar" : "header-bar no-image"} style={{backgroundImage: "url(" + banner + ")"}}>
            <Container maxWidth="lg" className="height-100per">
                <Grid container className="height-100per">
                    <Grid item xs={12} className="header-title-box">
                        {
                            (image !== undefined) ?
                                <div className="row-image-title">
                                    <div className="col-image">
                                        <img src={image} />
                                    </div>

                                    <div className="col-title">
                                        <h1 className="title">{title}</h1>
                                        <h2 className="sub-title">{subTitle}</h2>
                                    </div>
                                </div>
                                :
                                <h1 className="title">{title}</h1>
                        }
                    </Grid>
                </Grid>
            </Container>
        </header>
    )
}
export default Header;
