import Recat from "react";
import "./Empty.css";
import { Button, Grid } from "@mui/material";
import { Link } from "react-router-dom";
function EmptyPage({ description }) {
  return (
    <div className="empty-page">
      <Grid container>
        <Grid item xs={12} className="des-container">
          <p>{description}</p>
          <Link to="dashboard">
            {" "}
            <Button className="btn-send" variant="contained">
              برگشت به دشبورد
            </Button>
          </Link>
        </Grid>
      </Grid>
      <div className="container">
        <div className="head">
          <div className="hair">
            <div className="hair--forehead"></div>
          </div>
          <div className="mouth"></div>
          <div className="head--top">
            <div className="glasses">
              <div className="glasses--right">
                <div className="eye"></div>
                <div className="eyebrow"></div>
              </div>
              <div className="glasses--left">
                <div className="eye"></div>
                <div className="eyebrow"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default EmptyPage;
