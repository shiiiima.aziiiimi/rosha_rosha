import { Grid } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { Link } from 'react-router-dom'

function NewsLetter() {
    return (
        <Box className="news-letter" sx={{ mt: 7, px: 3, display: 'flex' }}>
            <Grid md={6} sx={{ mx: 'auto', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Box className="news-letter-box flex-1">
                    <Link to="" className="btn-signup submit-btn" sx={{ px: 2 }}>ثبت</Link>
                    <input type='email' placeholder="ایمیل" />
                </Box>
                <Box className="new-letters-text" sx={{ pr: { md: 2 } }}>
                    <Box className="img-text-holder"><b>ایمیل خبرنامه</b><img src='./assets/images/email-img.png' /></Box>
                    <Box>ارسال اخبار ومقالات</Box>
                </Box>
            </Grid>
        </Box>
    )
}

export default NewsLetter