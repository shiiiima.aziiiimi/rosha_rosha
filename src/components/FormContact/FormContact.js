import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Loading from "../../components/Loading/Loading";
import "./FormContact.css";
import axios from "axios";
import { Alert } from "@mui/material";
import General from "../../utils/General";

//
function FormContact(props) {
  //
  const title = props.title;
  const des = props.des;

  const [formLoading, setFormLoading] = useState(false);

  const [commentName, setCommentName] = useState("");
  const [commentFamily, setCommentFamily] = useState("");
  const [commentEmail, setCommentEmail] = useState("");
  const [commentMessage, setCommentMessage] = useState("");
  const [commentPhone, setCommentPhone] = useState("");
  const [commentError, setCommentError] = useState("");
  const onCommentNameInputChange = (event) =>
    setCommentName(event.target.value);
  const onCommentFamilyInputChange = (event) =>
    setCommentFamily(event.target.value);
  const onCommentEmailInputChange = (event) =>
    setCommentEmail(event.target.value);
  const onCommentMessageInputChange = (event) =>
    setCommentMessage(event.target.value);
  const onCommentPhoneInputChange = (event) =>
    setCommentPhone(event.target.value);
  var body = {
    email: commentEmail,
    first_name: commentName,
    last_name: commentFamily,
    note: commentMessage,
    phone: commentPhone,
  };
  // comment
  const handleComment = (event) => {
    axios({
      method: "post",
      url: General.siteUrl + "/contact",
      data: body,
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
    })
      .then((response) => {
        // check if received an OK response, throw error if not
        if (response.ok) {
          return response.json();
        }
        throw new Error(response.statusText);
      })
      .then((jsonData) => {
        console.log("Successful", jsonData);
      })
      .catch((errors) => {
        // If error was thrown then unpack here and handle in component/app state
        console.log("error", errors);
        setCommentError({ errors });
      });
  };

  //
  return (
    <div className="form-contact">
      <div className="title-bar form-title-bar">{title}</div>
      <div className="form-des">{des}</div>

      <div className="form-contact-box">
        <form onSubmit={handleComment} autoComplete="off">
          <Grid container spacing={2}>
            <Grid item sm={6} xs={12}>
              <input
                onChange={onCommentNameInputChange}
                value={commentName}
                type="text"
                placeholder="نام"
                autoComplete="false"
              />
            </Grid>

            <Grid item sm={6} xs={12}>
              <input
                onChange={onCommentFamilyInputChange}
                value={commentFamily}
                type="text"
                placeholder="نام خانوادگی"
                autoComplete="false"
              />
            </Grid>

            <Grid item xs={12}>
              <input
                onChange={onCommentEmailInputChange}
                value={commentEmail}
                type="email"
                placeholder="ایمیل"
                autoComplete="false"
              />
            </Grid>
            <Grid item xs={12}>
              <input
                onChange={onCommentPhoneInputChange}
                value={commentPhone}
                type="text"
                placeholder="تلفن"
                autoComplete="false"
              />
            </Grid>

            <Grid item xs={12}>
              <textarea
                onChange={onCommentMessageInputChange}
                placeholder="پیام"
              >
                {commentMessage}
              </textarea>
            </Grid>

            <Grid item xs={12}>
              {formLoading ? (
                <Loading />
              ) : (
                <button
                  onClick={handleComment}
                  type="button"
                  className="btn btn-primary"
                >
                  ارسال
                </button>
              )}
            </Grid>
          </Grid>
        </form>
      </div>
      {commentError && (
        <div className="form-contact-alert">
          {/* {ERROR_MESSAGES[commentError] ?? commentError} */}
        </div>
      )}
    </div>
  );
}
export default FormContact;
