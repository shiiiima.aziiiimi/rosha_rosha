export const navitems = [
    {
        id: 4,
        title: "روشا",
        path: "./home",
        cName: "fa nav-item",
        icon: "fa fa-bars"

    },
    {
        id: 3,
        title: "مجله و اخبار",
        path: "./home",
        cName: "nav-item",
        icon: "fa fa-book"

    },
    {
        id: 2,
        title: "سرویس ها",
        path: "./home",
        cName: "nav-item",
        icon: "fa fa-seedling"


    },
    {
        id: 1,
        title: "خانه",
        path: "./home",
        cName: "nav-item",
        icon: "fa fa-home"

    },



]
export const serviceDropdown = [
    {
        id: 1,
        title: "خانه",
        path: "./home",
        cName: "nav-item"

    },
    {
        id: 2,
        title: "خانه",
        path: "./home",
        cName: "nav-item"

    },
    {
        id: 3,
        title: "خانه",
        path: "./home",
        cName: "nav-item"

    },
    {
        id: 4,
        title: "خانه",
        path: "./home",
        cName: "nav-item"

    },
    {
        id: 5,
        title: "خانه",
        path: "./home",
        cName: "nav-item",

    },

]

export const SliderData = [
    {
        image: "/public"
    },
    {
        image: "https://tse1.mm.bing.net/th?id=OIP.OF59vsDmwxPP1tw7b_8clQHaE8&pid=Api&P=0&w=268&h=180"
    },
    {
        image: "https://images.search.yahoo.com/images/view;_ylt=AwrJ4NY2_W9hHlUA5h02nIlQ;_ylu=c2VjA3NyBHNsawNpbWcEb2lkAzdmMzdjNmY0NmY4ZGRkMThhZjZkZmQ2YjdkNjgzNmUyBGdwb3MDOQRpdANiaW5n?back=https%3A%2F%2Fimages.search.yahoo.com%2Fyhs%2Fsearch%3Fp%3Dgoogle%26ei%3DUTF-8%26type%3Dq3000_A18FS_set_bcrq%26fr%3Dyhs-ima-st_mig%26hsimp%3Dyhs-st_mig%26hspart%3Dima%26tab%3Dorganic%26ri%3D9&w=2200&h=1502&imgurl=s1.ibtimes.com%2Fsites%2Fwww.ibtimes.com%2Ffiles%2F2014%2F04%2F16%2Fgoogle.jpg&rurl=http%3A%2F%2Fibtimes.com%2Fgoogle-inc-googl-q1-2014-earnings-report-misses-estimates-earnings-rise-profit-margins-1572791&size=766.6KB&p=google&oid=7f37c6f46f8ddd18af6dfd6b7d6836e2&fr2=&fr=yhs-ima-st_mig&tt=Google+Inc.+%28GOOGL%29+Q1+2014+Earnings+Report+Misses+...&b=0&ni=264&no=9&ts=&tab=organic&sigr=B7e9wxRmCA_u&sigb=3FQvzngPktSA&sigi=VKVkUg64EbSu&sigt=6jFNNiSJ9MtD&.crumb=5k5sbbODmih&fr=yhs-ima-st_mig&hsimp=yhs-st_mig&hspart=ima&type=q3000_A18FS_set_bcrq"
    }
]


export const menuItems =
    [
        {
            id: 1,
            title: "خانه",
            path: "./home",
            icon: "fa fa-home"
        },
        {
            id: 2,
            title: "سرویس ها",
            path: "./home",
            icon: "fa fa-seedling",
            subMenuItems:
                [
                    {
                        id: 21,
                        title: "سرویس 1",
                        path: "./home",
                    },
                    {
                        id: 22,
                        title: "سرویس 2",
                        path: "./home",
                    },
                    {
                        id: 23,
                        title: "سرویس 3",
                        path: "./home",
                    },
                ]
        },
        {
            id: 3,
            title: "مجله و اخبار",
            path: "./home",
            icon: "fa fa-book",
            subMenuItems:
                [
                    {
                        id: 31,
                        title: "مجله 1",
                        path: "./home",
                    },
                    {
                        id: 32,
                        title: "مجله 2",
                        path: "./home",
                    },
                    {
                        id: 33,
                        title: "مجله 3",
                        path: "./home",
                    },
                ]
        },
        {
            id: 4,
            title: "روشا",
            path: "./home",
            icon: "fa fa-bars"
        },
    ];

export const HandIconSvg = () => {
    return (
        <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M31.9999 10.6665C32.9999 10.6665 33.8333 10.9998 34.3333 11.6665C34.9999 12.3332 35.3333 13.1665 35.3333 13.9998L21.9999 18.9998L10.3333 15.6665V0.666504H13.4999L25.6666 5.1665C26.4999 5.49984 26.9999 6.1665 26.9999 6.99984C26.9999 7.49984 26.8333 7.99984 26.4999 8.33317C26.1666 8.6665 25.6666 8.99984 24.9999 8.99984H20.3333L17.4999 7.83317L16.9999 9.33317L20.3333 10.6665H31.9999ZM0.333252 0.666504H6.99992V18.9998H0.333252V0.666504Z" fill="#FEFEFE" />
        </svg>
    )
}

export const BookIconSvg = () => {
    return (
        <svg width="28" height="24" viewBox="0 0 28 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M14 2.12035C12.2762 0.638097 9.77725 0.417597 7.50225 0.645097C4.85275 0.912847 2.17875 1.8211 0.51275 2.57885C0.359886 2.64837 0.230257 2.76041 0.139337 2.9016C0.0484167 3.04279 4.65e-05 3.20717 0 3.3751L0 22.6251C4.05335e-05 22.7715 0.036815 22.9156 0.106955 23.0441C0.177095 23.1726 0.278358 23.2814 0.401469 23.3607C0.52458 23.4399 0.665603 23.487 0.811621 23.4976C0.957639 23.5082 1.10398 23.482 1.23725 23.4214C2.78075 22.7213 5.2675 21.8796 7.67725 21.6364C10.143 21.3878 12.2098 21.7886 13.3175 23.1711C13.3995 23.2733 13.5034 23.3558 13.6215 23.4124C13.7396 23.4691 13.869 23.4985 14 23.4985C14.131 23.4985 14.2604 23.4691 14.3785 23.4124C14.4966 23.3558 14.6005 23.2733 14.6825 23.1711C15.7903 21.7886 17.857 21.3878 20.321 21.6364C22.7325 21.8796 25.221 22.7213 26.7628 23.4214C26.896 23.482 27.0424 23.5082 27.1884 23.4976C27.3344 23.487 27.4754 23.4399 27.5985 23.3607C27.7216 23.2814 27.8229 23.1726 27.893 23.0441C27.9632 22.9156 28 22.7715 28 22.6251V3.3751C28 3.20717 27.9516 3.04279 27.8607 2.9016C27.7697 2.76041 27.6401 2.64837 27.4872 2.57885C25.8212 1.8211 23.1473 0.912847 20.4978 0.645097C18.2227 0.415847 15.7238 0.638097 14 2.12035Z" fill="#6AA3AE" />
        </svg>
    )
}

export const CartIconSvg = () => {
    return (
        <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.75 0.500032C0.551088 0.500032 0.360322 0.579049 0.21967 0.719702C0.0790176 0.860354 0 1.05112 0 1.25003C0 1.44894 0.0790176 1.63971 0.21967 1.78036C0.360322 1.92101 0.551088 2.00003 0.75 2.00003H2.415L4.383 9.87503C4.55025 10.5425 5.148 11 5.83575 11H15.1882C15.8655 11 16.4408 10.55 16.6185 9.89753L18.5625 2.75003H16.992L15.1875 9.50003H5.835L3.86775 1.62503C3.78649 1.30195 3.59916 1.01549 3.33576 0.811517C3.07236 0.607544 2.74813 0.49786 2.415 0.500032H0.75ZM14.25 11C13.0162 11 12 12.0163 12 13.25C12 14.4838 13.0162 15.5 14.25 15.5C15.4838 15.5 16.5 14.4838 16.5 13.25C16.5 12.0163 15.4838 11 14.25 11ZM7.5 11C6.26625 11 5.25 12.0163 5.25 13.25C5.25 14.4838 6.26625 15.5 7.5 15.5C8.73375 15.5 9.75 14.4838 9.75 13.25C9.75 12.0163 8.73375 11 7.5 11ZM9.75 0.500032V4.25003H7.5L10.5 7.25003L13.5 4.25003H11.25V0.500032H9.75ZM7.5 12.5C7.923 12.5 8.25 12.827 8.25 13.25C8.25 13.673 7.923 14 7.5 14C7.077 14 6.75 13.673 6.75 13.25C6.75 12.827 7.077 12.5 7.5 12.5ZM14.25 12.5C14.673 12.5 15 12.827 15 13.25C15 13.673 14.673 14 14.25 14C13.827 14 13.5 13.673 13.5 13.25C13.5 12.827 13.827 12.5 14.25 12.5Z" fill="#F8F8F8" />
        </svg>
    )
}