// import React, { useState, useEffect } from "react";
// import Header from "../../components/Header/Header";
// import Container from "@mui/material/Container";
// import Grid from "@mui/material/Grid";
// import { Link } from "react-router-dom";
// import { Box, Button, Typography, TextField } from "@mui/material";
// import ReactApexChart from "react-apexcharts";
// import { HandIconSvg, BookIconSvg } from "../../components/Icon/Icon";
// import { Swiper, SwiperSlide } from "swiper/react";
// import Comment from "../../components/Comment/Comment";
// import NewsLetter from "../../components/NewsLetter/NewsLetter";

// import axios from "axios";
// import General from "../../utils/General";

// import "./Result.css"
// function ResultQarbalgary() {

//     const [barChartData, setBarChartData] = useState({
//         series: [
//           {
//             name: "Inflation",
//             data: [2.3, 3.1, 4.0, 10.1, 4.0, 3.6, 3.2, 2.3, 1.4, 0.8, 0.5, 0.2],
//           },
//         ],
//         options: {
//           chart: {
//             height: 500,
//             type: "bar",
//           },
    
//           plotOptions: {
//             bar: {
//               borderRadius: 10,
//               dataLabels: {
//                 position: "top", // top, center, bottom
//               },
//             },
//           },
//           colors: ["#4F8794"],
//           dataLabels: {
//             enabled: true,
//             formatter: function (val) {
//               return val + "%";
//             },
//             offsetY: -20,
//             style: {
//               fontSize: "12px",
//               colors: ["#252525"],
//             },
//           },
    
//           xaxis: {
//             categories: [
//               "Jan",
//               "Feb",
//               "Mar",
//               "Apr",
//               "May",
//               "Jun",
//               "Jul",
//               "Aug",
//               "Sep",
//               "Oct",
//               "Nov",
//               "Dec",
//             ],
//             position: "top",
//             axisBorder: {
//               show: false,
//             },
//             axisTicks: {
//               show: false,
//             },
//             crosshairs: {
//               fill: {
//                 type: "gradient",
//                 gradient: {
//                   colorFrom: "#4F8794",
//                   colorTo: "#4F8794",
//                   stops: [0, 100],
//                   opacityFrom: 0.4,
//                   opacityTo: 0.5,
//                 },
//               },
//             },
//             tooltip: {
//               enabled: true,
//             },
//           },
//           yaxis: {
//             axisBorder: {
//               show: false,
//             },
//             axisTicks: {
//               show: false,
//             },
//             labels: {
//               show: false,
//               formatter: function (val) {
//                 return val + "%";
//               },
//             },
//           },
//           title: {
//             text: "Monthly Inflation in Argentina, 2002",
//             floating: true,
//             offsetY: 480,
//             align: "center",
//             style: {
//               color: "#252525",
//             },
//           },
//         },
//       });
    
//       const [radarChartData, setRadarChartData] = useState({
//         series: [
//           {
//             name: "Series 1",
//             data: [80, 50, 30, 40, 100, 20],
//           },
//           {
//             name: "Series 2",
//             data: [20, 30, 40, 80, 20, 80],
//           },
//           {
//             name: "Series 3",
//             data: [44, 76, 78, 13, 43, 10],
//           },
//         ],
//         options: {
//           chart: {
//             height: 350,
//             type: "radar",
//             dropShadow: {
//               enabled: true,
//               blur: 1,
//               left: 1,
//               top: 1,
//             },
//             toolbar: {
//               show: true,
//               tools: {
//                 download: false,
//               },
//             },
//           },
    
//           stroke: {
//             width: 2,
//           },
//           fill: {
//             opacity: 0.1,
//           },
//           markers: {
//             size: 0,
//           },
//           xaxis: {
//             categories: ["2011", "2012", "2013", "2014", "2015", "2016"],
//           },
//         },
//       });
     
//       return (
//         <>
//           <div className="img-holder header-smaller-text">
//             <img src="./assets/images/res-exam-header.png" className="img-roll" />
//           </div>
//           <Box className="header-img-text-holder">
//             <img src="./assets/images/res-exam-img.png" />
//             <Box>
//               <Box className="head-roll">نتیجه آزمون هالند</Box>
//               <Box className="head-subtitle">علیرضا قربانی</Box>
//             </Box>
//           </Box>
//           <Box className="istj-container" sx={{ mt: 5, px: 5 }}>
//             <Box className="istj-background par-text">
//               <Typography component="p">
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//               <img src="./assets/images/exam-man.png" />
//             </Box>
//           </Box>
//           <Box className="par-text" sx={{ mt: 5, px: 5 }}>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Box className="par-text" sx={{ mt: 7, px: 5 }}>
//             <Box className="par-header" sx={{ mb: 5 }}>
//               <Box>لورم ایپسوم متن ساختگی</Box>
//             </Box>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Grid container className="text-img-container" sx={{ mt: 7 }}>
//             <Grid item md={7} className="text-i">
//               <Typography component="p" className="par-text" sx={{ pl: 5 }}>
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//             </Grid>
//             <Grid item md={5} className="t-img">
//               <img src="./assets/images/resualt-exam-img2.png" />
//             </Grid>
//           </Grid>
//           <Box className="par-text" sx={{ mt: 7, px: 5 }}>
//             <Box className="par-header" sx={{ mb: 5 }}>
//               <Box>لورم ایپسوم متن ساختگی</Box>
//             </Box>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Grid container className="text-img-container" sx={{ mt: 7 }}>
//             <Grid item md={7} className="text-i">
//               <Typography component="p" className="par-text" sx={{ pl: 5 }}>
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//             </Grid>
//             <Grid item md={5} className="t-img">
//               <img src="./assets/images/resualt-exam-img2.png" />
//             </Grid>
//           </Grid>
//           <Box className="par-text" sx={{ mt: 7, px: 5 }}>
//             <Box className="par-header" sx={{ mb: 5 }}>
//               <Box>لورم ایپسوم متن ساختگی</Box>
//             </Box>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Grid container className="text-img-container" sx={{ mt: 7 }}>
//             <Grid item md={7} className="text-i">
//               <Typography component="p" className="par-text" sx={{ pl: 5 }}>
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//             </Grid>
//             <Grid item md={5} className="t-img">
//               <img src="./assets/images/resualt-exam-img2.png" />
//             </Grid>
//           </Grid>
//           <Box className="par-text" sx={{ mt: 7, px: 5 }}>
//             <Box className="accel-item-title">لورم ایپسوم متن ساختگی</Box>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Grid md={6} sx={{ m: "auto", mt: 7 }}>
//             <ReactApexChart
//               options={barChartData.options}
//               series={barChartData.series}
//               type="bar"
//               height={500}
//             />
//           </Grid>
//           <Box className="par-text" sx={{ mt: 7, px: 5 }}>
//             <Typography component="p">
//               لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//               استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//               ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
//               کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی
//               در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می
//               طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
//               الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
//               صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و
//               شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای
//               اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده
//               قرار گیرد.
//             </Typography>
//           </Box>
//           <Box className="-container">
//             <Grid container className="box-has-img-text-2" sx={{ mt: 12, px: 5 }}>
//               <Grid item md="auto">
//                 <img src="./assets/images/exam-result-img-3.png" />
//               </Grid>
//               <Grid item md={6}>
//                 <Box className="accel-item-title" sx={{ mb: 2 }}>
//                   لورم ایپسوم متن ساختگی
//                 </Box>
//                 <Typography component="p" className="par-text">
//                   لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                   استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
//                   در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
//                   نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
//                   کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
//                   جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
//                   طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
//                   فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
//                   موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
//                   نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
//                   دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
//                 </Typography>
//               </Grid>
//             </Grid>
//             <Grid container className="box-has-img-text-2" sx={{ mt: 12, px: 5 }}>
//               <Grid item md="auto">
//                 <img src="./assets/images/exam-result-img-3.png" />
//               </Grid>
//               <Grid item md={6}>
//                 <Box className="accel-item-title" sx={{ mb: 2 }}>
//                   لورم ایپسوم متن ساختگی
//                 </Box>
//                 <Typography component="p" className="par-text">
//                   لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                   استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
//                   در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
//                   نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
//                   کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
//                   جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
//                   طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
//                   فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
//                   موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
//                   نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
//                   دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
//                 </Typography>
//               </Grid>
//             </Grid>
//             <Grid container className="box-has-img-text-2" sx={{ mt: 12, px: 5 }}>
//               <Grid item md="auto">
//                 <img src="./assets/images/exam-result-img-3.png" />
//               </Grid>
//               <Grid item md={6}>
//                 <Box className="accel-item-title" sx={{ mb: 2 }}>
//                   لورم ایپسوم متن ساختگی
//                 </Box>
//                 <Typography component="p" className="par-text">
//                   لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                   استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
//                   در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
//                   نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
//                   کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
//                   جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
//                   طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
//                   فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
//                   موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
//                   نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
//                   دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
//                 </Typography>
//               </Grid>
//             </Grid>
//           </Box>
//           <Grid container className="" sx={{ mt: 12, px: 5 }}>
//             <Grid item md={6}>
//               <Box className="accel-item-title" sx={{ mb: 2 }}>
//                 لورم ایپسوم متن ساختگی
//               </Box>
//               <Typography component="p" className="par-text">
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//             </Grid>
//             <Grid item md={6}>
//               <ReactApexChart
//                 options={radarChartData.options}
//                 series={radarChartData.series}
//                 type="radar"
//                 height={350}
//               />
//             </Grid>
//           </Grid>
//           <Box className="ball-background" sx={{ mt: 12, px: 5, pt: 7, pb: 7 }}>
//             <Box>
//               <Box
//                 className="accel-item-title accel-item-title-center"
//                 sx={{ mb: 2 }}
//               >
//                 لورم ایپسوم متن ساختگی
//               </Box>
//               <Typography component="p" className="par-text">
//                 لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
//                 استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
//                 ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
//                 و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
//                 زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
//                 متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
//                 رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
//                 کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
//                 راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
//                 حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
//                 طراحی اساسا مورد استفاده قرار گیرد.
//               </Typography>
//               <Box
//                 sx={{
//                   mt: 4,
//                   display: "flex",
//                   flexWrap: "wrap",
//                   justifyContent: "space-between",
//                 }}
//               >
//                 <ul className="list-1-container">
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                 </ul>
//                 <ul className="list-1-container">
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                 </ul>
//                 <ul className="list-1-container">
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                   <li>لورم ایپسوم متن ساختگی </li>
//                 </ul>
//               </Box>
//             </Box>
//             <Grid container sx={{ mt: 4, justifyContent: "space-between" }}>
//               <Grid item md={7} sx={{ pl: { md: 3 } }}>
//                 <Box className="card-box">
//                   <Box className="card-header">
//                     <HandIconSvg />
//                     <span>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</span>
//                   </Box>
//                   <Box className="card-body">
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       1. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       2. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       3. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       4. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       5. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       6. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       7. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       8. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       9. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                     <Typography component="p" sx={{ mb: 2 }}>
//                       10. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
//                     </Typography>
//                   </Box>
//                 </Box>
//               </Grid>
//               <Grid item md={5}>
//                 <Box className="card-box-1">
//                   <Box className="card-header">
//                     <BookIconSvg />
//                     <span>لورم ایپسوم</span>
//                   </Box>
//                   <Box className="card-body">
//                     <Grid container className="book-img-btn">
//                       <Grid item xs={6}>
//                         <img
//                           src="./assets/images/laptop-result-page.png"
//                           className="book-img"
//                         />
//                       </Grid>
//                       <Grid item xs={6} sx={{ pr: { md: 3 } }}>
//                         <Link to="" className="btn-signup w-100">
//                           خرید کتاب
//                         </Link>
//                       </Grid>
//                     </Grid>
//                     <Box sx={{ mt: 3 }}>
//                       <Typography component="h6" className="book-title">
//                         نام کتاب
//                       </Typography>
//                       <Typography component="p" className="par-text">
//                         افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
//                         طراحان خلاقی، و فر، و شرایط سخت تایپ به پایان رسد و زمان م
//                         طراحی اساسا مورد استفاده قرار گیرد.
//                       </Typography>
//                     </Box>
//                   </Box>
//                 </Box>
//               </Grid>
//             </Grid>
//             <Box sx={{ mt: 7 }}>
//               <Box
//                 className="accel-item-title accel-item-title-center"
//                 sx={{ mb: 2 }}
//               >
//                 لورم ایپسوم متن ساختگی
//               </Box>
//               <Swiper
//                 slidesPerView={4}
//                 spaceBetween={30}
//                 loop={true}
//                 navigation={true}
//                 breakpoints={{
//                   1: {
//                     slidesPerView: 1,
//                   },
//                   640: {},
//                   768: {
//                     slidesPerView: 3,
//                   },
//                   1024: {
//                     slidesPerView: 4,
//                   },
//                 }}
//                 className="mySwiper exam-recom"
//               >
//                 <SwiperSlide>
//                   <Box className="recom-exam-img">
//                     <img src="./assets/images/recom-exam-img.png" />
//                   </Box>
//                   <Box className="recom-exam-content" sx={{ p: 2 }}>
//                     <Typography component="h6" className="book-title">
//                       نام آزمون
//                     </Typography>
//                     <Typography component="p" className="par-text" sx={{ mt: 2 }}>
//                       افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
//                       طراحان خلاقی، و فر، و شرایط سخت تایپ به پایان رسد و زمان م
//                       طراحی اساسا مورد استفاده قرار گیرد.
//                     </Typography>
//                   </Box>
//                 </SwiperSlide>
//                 <SwiperSlide>
//                   <Box className="recom-exam-img">
//                     <img src="./assets/images/recom-exam-img.png" />
//                   </Box>
//                   <Box className="recom-exam-content" sx={{ p: 2 }}>
//                     <Typography component="h6" className="book-title">
//                       نام آزمون
//                     </Typography>
//                     <Typography component="p" className="par-text" sx={{ mt: 2 }}>
//                       افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
//                       طراحان خلاقی، و فر، و شرایط سخت تایپ به پایان رسد و زمان م
//                       طراحی اساسا مورد استفاده قرار گیرد.
//                     </Typography>
//                   </Box>
//                 </SwiperSlide>
//                 <SwiperSlide>
//                   <Box className="recom-exam-img">
//                     <img src="./assets/images/recom-exam-img.png" />
//                   </Box>
//                   <Box className="recom-exam-content" sx={{ p: 2 }}>
//                     <Typography component="h6" className="book-title">
//                       نام آزمون
//                     </Typography>
//                     <Typography component="p" className="par-text" sx={{ mt: 2 }}>
//                       افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
//                       طراحان خلاقی، و فر، و شرایط سخت تایپ به پایان رسد و زمان م
//                       طراحی اساسا مورد استفاده قرار گیرد.
//                     </Typography>
//                   </Box>
//                 </SwiperSlide>
//                 <SwiperSlide>
//                   <Box className="recom-exam-img">
//                     <img src="./assets/images/recom-exam-img.png" />
//                   </Box>
//                   <Box className="recom-exam-content" sx={{ p: 2 }}>
//                     <Typography component="h6" className="book-title">
//                       نام آزمون
//                     </Typography>
//                     <Typography component="p" className="par-text" sx={{ mt: 2 }}>
//                       افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
//                       طراحان خلاقی، و فر، و شرایط سخت تایپ به پایان رسد و زمان م
//                       طراحی اساسا مورد استفاده قرار گیرد.
//                     </Typography>
//                   </Box>
//                 </SwiperSlide>
//               </Swiper>
//             </Box>
//           </Box>
//           <Grid md={9} className="" sx={{ mt: 7, mx: "auto" }}>
//             <Comment />
//           </Grid>
//           <NewsLetter />
//         </>
//       );

    
// }
// export default ResultQarbalgary;






import React, { useState, useEffect } from "react";
import Header from "../../components/Header/Header";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { Box, Button, Typography, TextField } from "@mui/material";
import ReactApexChart from "react-apexcharts";
import { HandIconSvg, BookIconSvg } from "../../components/Icon/Icon";
import { Swiper, SwiperSlide } from "swiper/react";
import Comment from "../../components/Comment/Comment";
import NewsLetter from "../../components/NewsLetter/NewsLetter";

import axios from "axios";
import General from "../../utils/General";

import "./Result.css"
function ResultQarbalgary() {
  return (
    <div>he</div>
  )
}
  export default ResultQarbalgary;