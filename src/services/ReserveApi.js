import React from "react";
import axios from "axios";
import General from "../utils/General";

export default {
  // public

  // user panel
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/talent-scout/presence",
        method: "GET",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        params: params,
      });
  console.log("params :" , params)
      return response.data;
    } catch (error) {
      throw error;
    }
  },
  findId: async function ( payment_id ) {

    try {
      const response = await axios({
        url: General.siteUrl + "/payment-single/" + payment_id  ,
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/json",
          "accept":"application/json"
        },
      


      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
  sendId: async function ( params  ) {

    try {
      const response = await axios({
        url: General.siteUrl + "/talent-scout/presence/reserve" ,
        method: "Post",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/json",
          "accept":"application/json"
        },
      
params:params

      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
  send: async function ( params ) {

    try {
      const response = await axios({
        url: General.siteUrl + "/payment-single-post/2374"  ,
        method: "Post",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/json",
          "accept":"application/json"
        },
      
params:params

      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },


};

