import axios from "axios";
import General from "../utils/General";
const queryParams = new URLSearchParams(window.location.search);
const id = queryParams.get("id");
export default {
  // public
  findById: async function (params, id) {
    try {
      const response = await axios({
        url: General.siteUrl + "/service-page/" + id,
        method: "GET",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        params: params,
      });
   
      return response.data;
    } catch (error) {
      throw error;
    }
  },
// menu items
  
findAll: async function (params) {
  try {
    const response = await axios({
      url: General.siteUrl + "/home" ,
      method: "GET",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      params: params,
    });

    return response.data;
  } catch (error) {
    throw error;
  }
  },


  // user panel
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-services/",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });
      console.log("api:", response.data);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
