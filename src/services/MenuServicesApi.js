import axios from "axios";
import General from "../utils/General";

export default {
  // public

  // user panel
  find: async function (id) {
    try {
      const response = await axios({
        url: General.siteUrl + "/service-page/" + { id },
        method: "GET",
        headers: { "content-type": "application/x-www-form-urlencoded" },
      });
      console.log(id);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
