import axios from "axios";
import General from "../utils/General";

export default {
  // public
  findById: async function (id) {
    try {
      const response = await axios({
        url: General.siteUrl + "/exams/" + id,
        method: "GET",
        //headers: {...General.authorizationToken, "content-type": "application/x-www-form-urlencoded"},
        // params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
  findExam: async function (id) {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-exams/" + id,
        method: "GET",
        //headers: {...General.authorizationToken, "content-type": "application/x-www-form-urlencoded"},
        // params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  // user panel
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-exams",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });
      console.log( "find id",response)
      return response.data;
    } catch (error) {
      throw error;
    }
  
  },
 
  // exams
  findAll: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/exams-all",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  packageExams: async function (params) {
    try {
      const response = await axios({
        url:
          General.siteUrl +
          "/user-package-exams/" +
          params.packageId +
          "/" +
          params.userPackageId,
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
