// import axios from "axios";
// import General from "../utils/General";

// export default {
//   // public

//   // user panel
//   find: async function (params) {
//     const userData = JSON.parse(localStorage.getItem("user"));

//     try {
//       const response = await axios({
//         url: General.siteUrl + "/exam-execute/" + params.userId,
//         method: "GET",
//         headers: {
//           Authorization: `Bearer ${userData.token}`,
//           "content-type": "application/x-www-form-urlencoded",
//         },
//         params: params,
//       });
//       console.log(response);
//       return response.data;
//     } catch (error) {
//       throw error;
//     }
//   },
// };

import axios from "axios";
import General from "../utils/General";

export default {
  // public

  // user panel
  find: async function (userExamId) {
    const userData = JSON.parse(localStorage.getItem("user"));

    try {
      const response = await axios({
        url: General.siteUrl + "/exam-execute/" + userExamId,
        method: "GET",
        headers: {
          Authorization: `Bearer ${userData.token}`,
          "content-type": "application/x-www-form-urlencoded",
        },
    
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
