import axios from "axios";
import General from "../utils/General";

export default {
  login: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/login",
        method: "POST",
        // headers: {
        //   "content-type": "application/x-www-form-urlencoded",
        //   "content-type": "application/json",
        //   "accept": "application/json",
        // },
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
                    // "accept": "application/json",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  //
  register: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/register",
        method: "POST",
        headers: {
          // "content-type": "application/x-www-form-urlencoded",
          "content-type": "application/json",
          "accept": "application/json",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  //
  userInfo: async function () {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-edit",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  //
  update: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/updateInfo",
        method: "POST",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  //
  updatePassword: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/updatePassword",
        method: "POST",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },

  //
  userIndex: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-index",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/json",
          Accept: "application/json",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
