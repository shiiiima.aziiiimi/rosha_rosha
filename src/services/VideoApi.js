import axios from "axios";
import General from "../utils/General";

export default
{
  // public

  // user panel
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/user-videos",
        method: "GET",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
