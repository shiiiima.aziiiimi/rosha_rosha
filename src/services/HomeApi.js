import axios from "axios";
import General from "../utils/General";

export default {
  // public

  // user panel
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/home",
        method: "GET",
        headers: { "content-type": "application/json" ,"accept":"application/json"},
        params: params,
      });
      console.log("home:",response.data);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
