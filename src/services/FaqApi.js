import axios from "axios";
import General from "../utils/General";

export default
{
  // public
  find: async function (params) {
    try {
      const response = await axios({
        url: General.siteUrl + "/faq",
        method: "GET",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        params: params,
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
