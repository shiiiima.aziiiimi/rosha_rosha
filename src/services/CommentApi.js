import axios from "axios";
import General from "../utils/General";

export default {
  // public

  // user panel
  save: async function (id) {
    try {
      const response = await axios({
        url: General.siteUrl + "/add-to-favourite/" + id + "/exam",
        method: "POST",
        headers: {
          ...General.authorizationToken,
          "content-type": "application/x-www-form-urlencoded",
        },
      });

      return response.data;
    } catch (error) {
      throw error;
    }
  },
};
